<%@ include file="/WEB-INF/views/global/include.jsp" %>
<%@ page session="false" contentType="text/html;charset=utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>錯誤訊息 | CMS 2013</title>
</head>
<body class="login">
<div id="container_metro"></div>
<div id="container">

    <div id="wrapper">

        <div id="content">

            <div id="content_body">
            <form>
            <div class="fbr"></div>
            <label class="error">對不起，你尚未被授權執行此項作業。</label>
                                                <div class="fbr"></div>
                                                <label><a href="javascript:history.go(-1);">回上一頁</a></label>
                                                <div class="fbr"></div>
                                                </form>
            </div>
            <!-- /#content_body -->

            <!-- /#metro_channel -->

        </div>
        <!-- /#content -->

        <dl id="sidebar">
            <a href="/travel/" id="logo">Travel MIS</a>
        </dl>
        <!-- /#sidebar -->

        </div>
        <!-- /#wrapper -->

</div>
<!-- /#container -->
</body>
</html>