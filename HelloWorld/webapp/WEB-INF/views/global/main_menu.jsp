<dl id="metro_channel" class="metro">
	<dt>
		<a href="${pageContext.request.contextPath}/category"
			<c:if test="${curMenu == 'category'}">class="on"</c:if>><img
			src="/img/category.png">主題</a>
	</dt>
	<dt>
		<a href="${pageContext.request.contextPath}/edit"
			<c:if test="${curMenu == 'edit'}">class="on"</c:if>><img
			src="/img/edit.png">商品</a>
	</dt>
	<dt>
		<a href="${pageContext.request.contextPath}/customer"
			<c:if test="${curMenu == 'customer'}">class="on"</c:if>><img
			src="/img/security.png">顧客</a>
	</dt>
	<dt>
		<a href="${pageContext.request.contextPath}/security/my"
			<c:if test="${curMenu == 'security'}">class="on"</c:if>><img
			src="/img/security.png">權限</a>
	</dt>
	<dt>
		<a href="${pageContext.request.contextPath}/log"
			<c:if test="${curMenu == 'log'}">class="on"</c:if>><img
			src="/img/log.png">Log</a>
	</dt>
</dl>
<!-- /#metro_channel -->