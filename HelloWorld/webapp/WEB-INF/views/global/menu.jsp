<%@ page contentType="text/html;charset=utf-8"%>
<ul id="menu" class="menu-main">
	<li class="gn-trigger">
		<a class="icon icon-menu" style="padding-top: 32px;"><span>Menu</span></a>
		<nav class="menu-wrapper">
			<div class="scroller">
				<ul class="menu">
					<li class="item-list">
						<a class="glyph-icon flaticon-padlock">商品</a>
						<ul class="submenu">
							<li><a class="glyph-icon flaticon-working" href="/HelloWorld/goods/edit">商品管理</a></li>
							<li><a class="glyph-icon flaticon-tag1" href="/HelloWorld/goods/tag">商品標籤管理</a></li>
							<li><a class="glyph-icon flaticon-small27" href="/HelloWorld/goods/type">商品類型</a></li>
							<li><a class="glyph-icon flaticon-earth1" href="/HelloWorld/goods/area">地區管理</a></li>
						</ul>
					</li>
					<li><a class="glyph-icon flaticon-sections" href="/HelloWorld/category">主題</a></li>
					<li><a class="glyph-icon flaticon-money" href="/HelloWorld/customer">顧客</a></li>
					<li class="item-list"">
						<a class="glyph-icon flaticon-small28">權限</a>
						<ul class="submenu">
							<li><a class="glyph-icon flaticon-user3" href="/HelloWorld/security/user">使用者</a></li>
							<li><a class="glyph-icon flaticon-license" href="/HelloWorld/security/category">主題授權</a></li>
<!-- 							<li><a class="glyph-icon flaticon-add70" href="/HelloWorld/security/role">權限角色</a></li>-->
							<li><a class="glyph-icon flaticon-github2" href="/HelloWorld/security/my">我</a></li>
						</ul>
					</li>
					<li class="item-list">
						<a class="glyph-icon flaticon-record1">記錄</a>
						<ul class="submenu">
							<li><a class="glyph-icon flaticon-sections" href="/HelloWorld/log/data">資料修改記錄</a></li>
<!-- 							<li><a class="glyph-icon flaticon-grating" href="/HelloWorld/log/login_log">登入記錄</a></li>
							<li><a class="glyph-icon flaticon-newspapre" href="/HelloWorld/log/tablemapping">功能修改記錄</a></li>-->
						</ul>
					</li>
				</ul>
			</div><!-- /scroller -->
		</nav>
	</li>
	<li><a class="codrops-icon" href="${pageContext.request.contextPath}${baseURL}"><span>${menuName}</span></a></li>
	<li style="width:151px"><a class="codrops-icon" href=""><span>${metroName}</span></a></li>
	<li style="width:auto;"><span style="font-weight: bold;padding-left:10px;">hi, ${currentUserName} 您好</span><a class="glyph-icon flaticon-extract" style="display: inline-block;" href="/travel/j_spring_security_logout"><span>登出</span></a></li>
</ul>
