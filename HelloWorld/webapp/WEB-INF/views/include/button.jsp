<div id="btn">
	<c:if test="${not empty message}">
 	<dl>
 		<dt>
 			<label><span>&nbsp;${message}</span></label>
 			<div class="fbr"></div>		
 		</dt>
 	</dl>
 	</c:if>
  <dl>
  	<dt><a href="javascript:void(0);" class="send" id="save_btn">儲存</a></dt>
    <dt><a href="javascript:void(0);" class="send" id="reset_btn" title="按了之後，資料會恢復到未修改前的內容.">重寫</a></dt>
    <c:if test="${command != 'insert'}">
    <dt><a href="javascript:void(0);" class="send" id="delete_btn">刪除</a></dt>
    <dt><a href="javascript:void(0);" class="send" id="new_btn" title="要新增下一筆時，按此按鈕.">新增</a></dt>
		<c:if test="${curMenu != 'security'}">
    <dt><a href="javascript:void(0);" class="send" id="save_as_btn" title="另存成新的一筆資料.">另存</a></dt>
		</c:if>		    
    </c:if>		    
    <%@ include file="/WEB-INF/views/include/copyright.jsp" %>
  </dl>
</div>