<%@ include file="/WEB-INF/views/global/include.jsp"%>
<%@ page session="true" contentType="text/html;charset=utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>登入 | shen 2018</title>
<meta charset="UTF-8" />
<link rel="stylesheet" type="text/css" href="/css/reset.css">
<link rel="stylesheet" type="text/css" href="/css/structure.css">

<script src="/jquery_ui/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/js/localization/messages_zh_TW.js" type="text/javascript"></script>
</head>
<body class="login">
<form class="box login" action="${pageContext.request.contextPath}/j_spring_security_check" method="post" name="frmLogin" id="frmLogin" >
        <input name="go_fun" id="go_fun" value="/security/user"" type="hidden">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <fieldset class="boxBody">
          <label>帳號
          <input type="text" placeholder="請輸入帳號" name="username" id="username" value="shen123" type="text" value="${sessionScope['SPRING_SECURITY_LAST_USERNAME']}" class="required"></label>
          <label><a href="#" class="rLink" tabindex="5"></a>密碼
          <input type="password" placeholder="請輸入密碼" name="password" id="password"  value="shen123" type="password" class="required"></label>

          <label>驗證碼 <img id="keyImage" src="${pageContext.request.contextPath}/keyimg" /> <a href="javascript:void(0);" onClick="changeKeyImage()">換一張圖</a></label>
          <input name="keycode" id="keycode" type="text" size="50" class="required">
        </fieldset>
        <footer>
          <input type="submit" class="btnLogin" value="Login" tabindex="4">
        </footer>
</form>
<script language="javascript">
        $(function() {
                $('#j_username').focus();
                $("#frmLogin").validate();
        });


        function changeKeyImage() {
                $("#keyImage").attr('src', '/travel/keyimg?t=' + Math.random());
                $('#keycode').focus();
        };
</script>
</body>
</html>