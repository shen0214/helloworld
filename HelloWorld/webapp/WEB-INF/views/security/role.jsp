<%@ include file="/WEB-INF/views/global/include.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page session="false" contentType="text/html;charset=utf-8"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<c:set var="curMenu" value="security" />
<c:set var="menuName" value="權限" />
<c:set var="metro" value="user" />
<c:set var="metroName" value="使用者" />
<title>${menuName} | ${webName}</title>
<%@ include file="/WEB-INF/views/global/css_javascript.jsp"%>
</head>
<body>
<div class="container">
	<%@ include file="/WEB-INF/views/global/menu.jsp"%>
	<div id="content">
<div id="nav_left">
        <div class="d_toggle"></div>
        <ul id="navigation">
			<dl style="margin-top:70px">
     			<c:set var="selected" value=""/>  
     			<c:forEach var="role" items="${securityRoles}">
     			<c:set var="selected" value=""/>
     			<c:if test="${role.roleName == securityRole.roleName}">
     				<c:set var="selected" value=" class=selected"/>  
     			</c:if>
				<dd style="width:250px;margin-top: 10px;"><a href="${pageContext.request.contextPath}${baseURL}/${role.roleName}/form"${selected}>${role.roleComment} (${role.roleName})</a></dd>
			</c:forEach>
			</dl>	
		</ul>
</div>
<script type="text/javascript">
$('#nav_left').menu_left({show:true});
$(window).on('resize',function(){
        if($(window).width()<1024)
                $('#nav_left').menu_left({show:false });
        else
                $('#nav_left').menu_left({show:true });
});
</script>
		<!-- /#navigation -->
		<div id="content_body">
			<%@ include file="/WEB-INF/views/global/form_message_list.jsp" %>
				<form:form method="post" commandName="${formId}" action="${pageContext.request.contextPath}${baseURL}/${command}">			    	     
				  <dl class="form_item">
					<dd><span>權限角色</span></dd>
				  	<c:choose> 
				  		<c:when test="${command == 'insert'}">
				   	<form:input path="roleName" maxlength="20" cssClass="required" />
						<form:errors path="roleName" cssClass="error" />
							</c:when>
							<c:otherwise>
								<dd><span class="txt">${securityRole.roleName}</span></dd>
								<form:hidden path="roleName" />
							</c:otherwise>
						</c:choose>
				  </dl>
				  <dl class="form_item">
					<dd><span>角色說明</span></dd>
				   	<dd><form:input path="roleComment" maxlength="20" cssClass="required" />
						<form:errors path="roleComment" cssClass="error" /></dd>
				  </dl>
				  
				</form:form>	  
				
				<div  class="btn_area">
					<a href="javascript:void(0);" class="send" id="save_btn">儲存</a>&nbsp;&nbsp;&nbsp;
					<a href="javascript:void(0);" class="send" id="reset_btn">重寫</a>&nbsp;&nbsp;&nbsp;
					<a href="javascript:void(0);" class="send" id="delete_btn">刪除</a>&nbsp;&nbsp;&nbsp;
					<a href="javascript:void(0);" class="send" id="new_btn">新增</a>&nbsp;&nbsp;&nbsp;
				</div>
				
				<div class="split_line"></div>					  
				<fieldset><legend><a href="javascript:void(0);" onClick="$('#table_log').load('${pageContext.request.contextPath}/log/view/security_role/${securityRole.roleName}');">點擊觀看資料修改記錄</a></legend>
					<div id="table_log">
					</div>
				</fieldset>
				
			
			
				
		</div>
	</div>
			 
</div><!-- /container -->
<script src="/js/classie.js"></script>
<script src="/js/gnmenu.js"></script>
<script>
	new gnMenu( document.getElementById( 'menu' ) );
</script>
<script>
$(function () {
	$("#navigation").treeview({
		persist: "cookie",
		cookieId: "role",
		cookiePath: "/",
		collapsed: true,
		unique: false
	});

	$("#save_btn").click(function() { 
  	$('#${formId}').submit();
  });
  $("#reset_btn").click(function() { 
  	$('#${formId}').get(0).reset();
  	$('#roleName').focus();
  });
  <c:if test="${command != 'insert'}">
  $("#delete_btn").click(function() { 
  	if ( window.confirm( "刪除時，系統將無法再復原此筆記錄，確定要執行嗎 ?" )  == true ) {
  		window.location.href = '${pageContext.request.contextPath}${baseURL}/delete/${securityRole.roleName}';
  	}
  });
  $("#new_btn").click(function() { 
  	window.location.href = '${pageContext.request.contextPath}${baseURL}/form';
  });
  </c:if>
  $("#${formId}").validate();
  $('#roleName').focus();
});
</script>
</body>
</html>
