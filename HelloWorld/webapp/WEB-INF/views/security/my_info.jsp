<%@ include file="/WEB-INF/views/global/include.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page session="false" contentType="text/html;charset=utf-8"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<c:set var="curMenu" value="security" />
<c:set var="menuName" value="權限" />
<c:set var="metro" value="info" />
<c:set var="metroName" value="我" />
<title>${menuName} | ${webName}</title>
<%@ include file="/WEB-INF/views/global/css_javascript.jsp"%>
</head>
<body>
<div class="container">
	<%@ include file="/WEB-INF/views/global/menu.jsp"%>
	<div id="content">
		<!-- /#navigation -->
		<div id="content_body">
			<form:form method="post" commandName="${formId}" action="${pageContext.request.contextPath}/security/my/info/update">
				<form:hidden path="id" />								   
				<dl class="form_item">
					<dd><span>email</span></dd>
					<dd><form:input path="email" maxlength="50" cssClass="required" />
						<form:errors path="email" cssClass="error" /></dd></dl>		

				<dl class="form_item">
					<dd><span>密碼</span></dd>
				  	<dd><input type="password" id="password" name="password" maxlength="20" value="${user.password}" cssClass="required"></dd></dl>

				<dl class="form_item">
					<dd><span>姓名</span></dd>
				   	<dd><form:input path="name" maxlength="20" cssClass="required" />
						<form:errors path="name" cssClass="error" /></dd></dl>
				  
				 <fieldset><legend>個人化</legend>
					商品狀態：<select name="myGoodsStatus" id="myGoodsStatus" class="inline">
						<c:forEach var="statusItem" items="${goodsStatus}">
							<option value="${statusItem.code}"<c:if test="${statusItem.code == user.myGoodsStatus}"> selected</c:if>>${statusItem.name}</option>
						</c:forEach>
							<option value="-1"<c:if test="${-1 == user.myGoodsStatus}"> selected</c:if>>全部</option>
					</select>
					<div class="fbr"></div>
					
<!-- 					預設頻道：<select name="myDefaultChannel" id="myDefaultChannel" class="inline">
						<c:forEach var="channelItem" items="${channelList}">
							<option value="${channelItem.id}"<c:if test="${channelItem.id == user.myDefaultChannel}"> selected</c:if>>${channelItem.name}</option>
						</c:forEach>
					</select>
					<div class="fbr"></div>-->
					<label>（變更資料後，要重新登入後，才會生效 !!!）</label>
				</fieldset>
				<div class="fbr"></div>
			</form:form>
			
			<div  class="btn_area_horizontal">
				<c:if test="${not empty message}">
				<div id="message_box_horizontal"><span>${message}</span></div>
				</c:if>
				<a href="javascript:void(0);" class="send" id="save_btn">儲存</a>&nbsp;&nbsp;&nbsp;
				<a href="javascript:void(0);" class="send" id="reset_btn" title="按了之後，資料會恢復到未修改前的內容.">重寫</a>&nbsp;&nbsp;&nbsp;
				<c:if test="${command != 'insert'}">
				<a href="javascript:void(0);" class="send" id="delete_btn">刪除</a>&nbsp;&nbsp;&nbsp;
				<a href="javascript:void(0);" class="send" id="new_btn" title="要新增下一筆時，按此按鈕.">新增</a>
				</c:if>
				<%@ include file="/WEB-INF/views/include/copyright.jsp"%>
			</div>
						

		  <div class="split_line"></div>					  
			<fieldset><legend><a href="javascript:void(0);" onClick="$('#table_log').load('${pageContext.request.contextPath}/log/view/user/${user.id}');">點擊觀看資料修改記錄</a></legend>
				<div id="table_log">
				</div>
			</fieldset>				
			
			
			
			
			
			
			
			
			
			
			
			
				
		</div>
	</div>
			 
</div><!-- /container -->
<script src="/js/classie.js"></script>
<script src="/js/gnmenu.js"></script>
<script>
	new gnMenu( document.getElementById( 'menu' ) );
</script>
<script>
$(function () {
	$("#save_btn").click(function() { 
  	$('#${formId}').submit();
  });
  $("#reset_btn").click(function() { 
  	$('#${formId}').get(0).reset();
  	$('#email').focus();
  });
  
  $("#${formId}").validate();  
  $('#email').focus();  
});
</script>
</body>
</html>
