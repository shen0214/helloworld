<%@ include file="/WEB-INF/views/global/include.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page session="false" contentType="text/html;charset=utf-8"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<c:set var="curMenu" value="security" />
<c:set var="menuName" value="權限" />
<c:set var="metro" value="user" />
<c:set var="metroName" value="使用者" />
<title>${menuName} | ${webName}</title>
<%@ include file="/WEB-INF/views/global/css_javascript.jsp"%>
</head>
<body>
<div class="container">
	<%@ include file="/WEB-INF/views/global/menu.jsp"%>
	<div id="content">
		<c:import url="${baseURL}/form/pagelist" />
		<!-- /#navigation -->
		<div id="content_body">
			<form:form method="post" modelAttribute="${formId}"
				action="${pageContext.request.contextPath}${baseURL}/${command}">
				<form:hidden path="userId" />
				<dl class="form_item">
					<dd><span class="red_star">email： </span></dd>
					<dd><form:input path="email" maxlength="50" size="40" cssClass="required inline" />
						<form:errors path="email" cssClass="error" /> </dd>
				</dl>
				<dl class="form_item">
					<dd><span class="red_star">密碼： </span></dd>
					<dd><form:input path="password" maxlength="20" size="20" cssClass="required inline" />
						<form:errors path="password" cssClass="error" /> </dd>
				</dl>
<!--			<c:if test="${command != 'insert'}">&nbsp;&nbsp;&nbsp;[<a href="${pageContext.request.contextPath}${baseURL}/${user.userId}/assign">指定主題</a>]</c:if>-->
				<dl class="form_item">
					<dd><span class="red_star">姓名： </span></dd>
					<dd><form:input path="name" maxlength="20" size="20" cssClass="required inline" />
						<form:errors path="name" cssClass="error" /> </dd>
					</dd>
				</dl>
				<style>
					fieldset{
						margin:20px 50px;
						width:70%;
					}
				</style>
					<fieldset>
						<legend class="red_star">權限角色</legend>
						<c:forEach var="tempRole" items="${currentUser.roles}">
							<c:set var="checked" value="" />
							<c:forEach var="role" items="${user.roles}">
								<c:if test="${role.roleName == tempRole.roleName}">
									<c:set var="checked" value=" checked" />
								</c:if>
							</c:forEach>
							<label><input type="checkbox" name="roleId"
								id="roleId" value="${tempRole.roleId}" cssClass="inline"
								${checked} /> ${tempRole.roleComment}</label>
						</c:forEach>
					</fieldset>
				<fieldset>
					<legend>個人化</legend>
					商品狀態：<mytags:goodsStatus name="myGoodsStatus" id="myGoodsStatus" selected="${user.myGoodsStatus}" clss="inline" info="商品狀態"/>
				</fieldset>
				</dl>
				<fieldset>
					<legend class="red_star">部門</legend>
					<form:radiobuttons path="deptId" items="${userDepts}"
						itemValue="code" itemLabel="name" cssClass="inline" />
					<form:errors path="deptId" cssClass="error" />
				</fieldset>

				<fieldset>
					<legend class="red_star">管理層級</legend>
					<label><form:radiobutton path="categoryLimit" value="1" />地區管理</label>
					<label><form:radiobutton path="categoryLimit" value="0" />權限控管（高權限）</label>
				</fieldset>

				<fieldset>
					<legend class="red_star">是否啟用</legend>
					<form:radiobuttons path="enabled" items="${enabledItems}" itemValue="code" itemLabel="name" />
					<form:errors path="enabled" cssClass="error" />
				</fieldset>
			</form:form>
			

			<div  class="btn_area_horizontal">
				<c:if test="${not empty message}"><div id="message_box_horizontal"><span>${message}</span></div></c:if>
				<a href="javascript:void(0);" class="send" id="save_btn">儲存</a>&nbsp;&nbsp;&nbsp;
				<a href="javascript:void(0);" class="send" id="reset_btn"
				title="按了之後，資料會恢復到未修改前的內容.">重寫</a>&nbsp;&nbsp;&nbsp;
				<c:if test="${command != 'insert'}">
				<a href="javascript:void(0);" class="send" id="delete_btn">刪除</a>&nbsp;&nbsp;&nbsp;
				<a href="javascript:void(0);" class="send" id="new_btn" title="要新增下一筆時，按此按鈕.">新增</a>
				</c:if>
				<%@ include file="/WEB-INF/views/include/copyright.jsp"%>
			</div>
						

			<div class="split_line"></div>
			<fieldset>
				<legend>最近修改記錄</legend>
				<div id="table_log"></div>
			</fieldset>
				
			
			
			
				
		</div>
	</div>
			 
</div><!-- /container -->
<script src="/js/classie.js"></script>
<script src="/js/gnmenu.js"></script>
<script>
	new gnMenu( document.getElementById( 'menu' ) );
</script>
<script>
$(function () {
	$("#save_btn").click(function() {
		alert("a");
  	$('#${formId}').submit();
  });
  $("#reset_btn").click(function() { 
  	$('#${formId}').get(0).reset();
  	$('#email').focus();
  });
  <c:if test="${command != 'insert'}">
  $("#delete_btn").click(function() { 
  	if ( window.confirm( "刪除時，系統將無法再復原此筆記錄，確定要執行嗎 ?" )  == true ) {
  		window.location.href = '${pageContext.request.contextPath}${baseURL}/delete/${user.userId}';
  	}
  });
  $("#new_btn").click(function() { 
  	window.location.href = '${pageContext.request.contextPath}${baseURL}/form';
  });
  </c:if>
  
  $("#${formId}").validate();  
  $('#email').focus();  
});
$('#table_log').load('${pageContext.request.contextPath}/log/view/user<c:if test="${command != 'insert'}">/${user.userId}</c:if>');

</script>
</body>
</html>
