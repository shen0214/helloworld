<%@ include file="/WEB-INF/views/global/include.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page session="false" contentType="text/html;charset=utf-8"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<c:set var="curMenu" value="security" />
<c:set var="menuName" value="權限" />
<c:set var="metro" value="category" />
<c:set var="metroName" value="類別授權" />
<title>${menuName} | ${webName}</title>
<%@ include file="/WEB-INF/views/global/css_javascript.jsp"%>
</head>
<body>
<div class="container">
	<%@ include file="/WEB-INF/views/global/menu.jsp"%>
	<div id="content">
		<div id="navigation">
			<c:import url="${baseURL}/form/pagelist" />
		</div>
		<!-- /#navigation -->
		<div id="content_body">
			<fieldset><legend>請指定 [<a href="${pageContext.request.contextPath}/security/user/${user.id}/form">${user.name}</a>] 可以操作的類別：</legend>      	
      	<div id="table_list">
				  <table id="data_list">
						<tr class="cell_title">			
							<th>
								<input type="checkbox" name="check_all" id="check_all" value="1"> 全選
							</th>
							<th>
								中文名稱
							</th>
							<th>
								英文簡稱
							</th>							
						</tr>
						<c:forEach var="category" items="${listCategory}" varStatus="rowCounter">
							<c:choose>
								<c:when test="${(rowCounter.count % 2) == 0}">
				          <c:set var="rowStyle" scope="page" value="#FFFFFF"/>
				        </c:when>
				        <c:otherwise>
				          <c:set var="rowStyle" scope="page" value="#EEF7FF"/>
				        </c:otherwise>
				    	</c:choose>
						<tr bgcolor="${rowStyle}">					
							<td>
								<input type="checkbox" name="check_row" id="check_row" value="${category.id}"<c:if test="${category.userId != 0}"> checked</c:if>>
								&nbsp;<span id="boxMsg_${category.id}"></span>
							</td>							
							<td>
								<c:if test="${not empty category.upperCategoryName}">
								${category.upperCategoryName} > 
								</c:if>
								${category.name}
							</td>	
							<td>
								${category.code}
							</td>	
						</tr>
						</c:forEach>
					</table>
					</fieldset>
      	</div>
			
			
			
			
			
			
			
			
			
			
			
				
		</div>
	</div>
			 
</div><!-- /container -->
<script src="/js/classie.js"></script>
<script src="/js/gnmenu.js"></script>
<script>
	new gnMenu( document.getElementById( 'menu' ) );
</script>
<script>
$(function () {
	$("#save_btn").click(function() { 
  	$('#${formId}').submit();
  });
  $("#reset_btn").click(function() { 
  	$('#${formId}').get(0).reset();
  	$('#email').focus();
  });
  <c:if test="${command != 'insert'}">
  $("#delete_btn").click(function() { 
  	if ( window.confirm( "刪除時，系統將無法再復原此筆記錄，確定要執行嗎 ?" )  == true ) {
  		window.location.href = '${pageContext.request.contextPath}${baseURL}/delete/${user.id}';
  	}
  });
  $("#new_btn").click(function() { 
  	window.location.href = '${pageContext.request.contextPath}${baseURL}/form';
  });
  </c:if>
  
  $("#${formId}").validate();  
  $('#email').focus();  
});
</script>
</body>
</html>
