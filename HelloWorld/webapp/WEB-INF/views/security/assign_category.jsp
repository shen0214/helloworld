<%@ include file="/WEB-INF/views/global/include.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page session="false" contentType="text/html;charset=utf-8"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<c:set var="curMenu" value="security" />
<c:set var="menuName" value="權限" />
<c:set var="metro" value="category" />
<c:set var="metroName" value="類別授權" />
<title>${menuName} | ${webName}</title>
<%@ include file="/WEB-INF/views/global/css_javascript.jsp"%>
</head>
<body>
<div class="container">
	<%@ include file="/WEB-INF/views/global/menu.jsp"%>
	<div id="content">
		<c:import url="/security/user/assign_category/pagelist" />
		<!-- /#navigation -->
		<div id="content_body">
			<fieldset>
				<legend>請指定 [<a href="${pageContext.request.contextPath}/security/user/${user.id}/form">${user.name}</a>] 可以操作的類別：</legend>      	
				<div id="table_list">
				  <table id="data_list">
						<tr class="cell_title">			
							<th>
								<input type="checkbox" name="check_all" id="check_all" value="1"> 全選
							</th>
							<th>
								中文名稱
							</th>
							<th>
								英文簡稱
							</th>							
						</tr>
						<c:forEach var="category" items="${listCategory}" varStatus="rowCounter">
							<c:choose>
								<c:when test="${(rowCounter.count % 2) == 0}">
				          <c:set var="rowStyle" scope="page" value="#FFFFFF"/>
				        </c:when>
				        <c:otherwise>
				          <c:set var="rowStyle" scope="page" value="#EEF7FF"/>
				        </c:otherwise>
				    	</c:choose>
						<tr bgcolor="${rowStyle}">					
							<td>
								<input type="checkbox" name="check_row" id="check_row" value="${category.id}"<c:if test="${category.userId != 0}"> checked</c:if>>
								&nbsp;<span id="boxMsg_${category.id}"></span>
							</td>							
							<td>
								<c:if test="${not empty category.upperCategoryName}">
								${category.upperCategoryName} > 
								</c:if>
								${category.name}
							</td>	
							<td>
								${category.code}
							</td>	
						</tr>
						</c:forEach>
					</table>
					</fieldset>
      	</div>
			
			
			
				
		</div>
	</div>
			 
</div><!-- /container -->
<script src="/js/classie.js"></script>
<script src="/js/gnmenu.js"></script>
<script>
	new gnMenu( document.getElementById( 'menu' ) );
</script>
<script>
var insertURL = "${pageContext.request.contextPath}${baseURL}/user/${user.id}/insert/";
var deleteURL = "${pageContext.request.contextPath}${baseURL}/user/${user.id}/delete/";
	
$(function () {
	$("#check_all").click(function() {
		if($("#check_all").prop("checked")) {
			if ( !window.confirm( "確定要將此使用者加到所有的類別嗎 ?" ) ) {
				$("#check_all").prop("checked", false);
				return;
			}
     	$("input[name='check_row']").each(function() {
     		var curBox = $(this);
      	if (!curBox.prop("checked")) {
      		curBox.prop("checked", true);
	      	doAction(insertURL, curBox.val());
      	}
     	});
   	}
   	else {
   		if ( !window.confirm( "確定要取消此使用者所有類別的授權嗎 ?" ) ) {
   			$("#check_all").prop("checked", true);
   			return;
   		}      	
     	$("input[name='check_row']").each(function() {
     		var curBox = $(this);
     		if (curBox.prop("checked")) {
      		curBox.prop("checked", false);
	      	doAction(deleteURL, curBox.val());
      	}
     	});           
   	}
	});
	
	// 單一使用者加入與移出.
	$("input[name='check_row']").click(function() {
		// 為了防止 event 被穿透到 TR 所以必須修改 checkbox 的狀態.
		$(this).prop("checked",!$(this).prop("checked"));		
	});
	
	function doAction(runURL, categoryId) {
		$.ajax( {
	 		url: runURL + categoryId
	 	} )
	 	.done(function( data ) {
	 		$("#boxMsg_" + categoryId).html(data);
	 	});
	};
	
	$("#data_list tr").click(function() {
   	var input = $(this).find("input");
   	if ($(input).attr("name") != "check_row") return;
		if( $(input).prop("checked") ) {
			doAction(deleteURL, $(input).val());
		}	
		else {
			doAction(insertURL, $(input).val());
		}
		input.prop("checked", !input.prop("checked"));
   });
});
</script>
</body>
</html>
