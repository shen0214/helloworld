<%@ include file="/WEB-INF/views/global/include.jsp"%>
<%@ include file="/WEB-INF/views/global/css_javascript.jsp" %>
<link rel="stylesheet" type="text/css" href="/css/main.css" />

<div id="nav_left">
        <div class="d_toggle"></div>
	<ul id="navigation">
	<c:set var="currentURL" value="${pageContext.request.contextPath}/security/user/${clickToPage}/pagelist" />
	<c:set var="assign_type" value="" />
	<c:set var="assign_action" value="form" />
	<c:if test="${clickToPage == 'assign_category'}">
		<c:set var="assign_type" value="/category" />
		<c:set var="assign_action" value="assign" />
	</c:if>
	<c:forEach var="user" items="${users}" varStatus="rowCounter">
		<dl class="form_item">
			<dd style="text-align:left;margin:0 15px;">${user.dept.name}</dd>
			<dd style="width:100px;text-align:left;">
			<a href="${pageContext.request.contextPath}/security${assign_type}/user/${user.userId}/${assign_action}">${user.name}</a></dd>
		</dl>
	</c:forEach>
	</ul>
</div>