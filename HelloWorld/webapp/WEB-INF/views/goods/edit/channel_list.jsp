<%@ include file="/WEB-INF/views/global/include.jsp"%>
<%@ page session="true" contentType="text/html;charset=utf-8"%>
<fieldset style="width:100%"><legend>請挑選目的地類別</legend>
<c:if test="${not empty listChannel}">
<div>
<!--	<div style="float:left; overflow:scroll; width:250px; height:450px;">
		<ul>			
			<c:forEach var="tempChannel" items="${listChannel}">
		  <li>
		  	<a href="javascript:void(0);" onClick="loadCategory(${tempChannel.id})">${tempChannel.name}</a>
		  	<div class="space_h5"></div>
		  </li>
			</c:forEach>
		</ul>
	</div>-->
	<div style="float:left; overflow:scroll; width:250px; height:450px;" id="category_list" name="category_list">
	</div>
	<div style="float:left; overflow:scroll; width:250px; height:450px;" id="sub_category_list" name="sub_category_list">
	</div>
</div>
</c:if>
</fieldset>
<script>
$(document).ready(function(){
	loadCategory(${channelMain.id});
	loadSubCategory(${upperCategoryId});
});

function loadCategory(channelId) {
	$("#category_list").load("${pageContext.request.contextPath}${baseURL}/list/category/${command}");
	$("#sub_category_list").html("");
}

function loadSubCategory(upperCategory) {
	if (upperCategory == 0) {
		$("#sub_category_list").html("");
		return;
	}
	$("#sub_category_list").load("${pageContext.request.contextPath}${baseURL}/list/" + upperCategory + "/subcategory/${command}");
}
</script>