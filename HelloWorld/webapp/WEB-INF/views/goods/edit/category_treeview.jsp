<div id="nav_left">
	<div class="d_toggle"></div>
	<ul id="navigation">
		<a href="${pageContext.request.contextPath}${baseURL}">商品管理 (未上架數量)</a>
		<c:if test="${not empty channelCategoryTree}">		
		<c:set var="categoryTree" value="${channelCategoryTree[0]}" />
		
		<c:if test="${not empty categoryTree}">
		<ul>			
		  <!-- 取出該 category tree 最上層的類別，即 upper category id 為 0 的類別. -->
			<c:forEach var="tempCategory" items="${categoryTree}">
			<li>
				<c:set var="subCategoryTree" value="${channelCategoryTree[tempCategory.id + 0]}" />
				<c:if test="${not empty subCategoryTree}">
				<a href="${pageContext.request.contextPath}${baseURL}/category/${tempCategory.id}/list">${tempCategory.name} (${tempCategory.unEditGoods})</a>
				<!-- ${tempCategory.name} (${tempCategory.unEditGoods}) -->
				</c:if>
					
				<c:if test="${empty subCategoryTree}">						
					<a href="${pageContext.request.contextPath}${baseURL}/category/${tempCategory.id}/list">${tempCategory.name} (${tempCategory.unEditGoods})</a>
				&nbsp;<a href="${pageContext.request.contextPath}${baseURL}/goods/category/${tempCategory.id}/view" class="plus">[+]</a>
				</c:if>
				
				<c:if test="${not empty subCategoryTree}">
				<c:set var="currentCategory" value="${tempCategory}" scope="request" />
				<c:set var="loopCount" value="0" scope="request" />
				<jsp:include page="category_treeview_block.jsp"/>
				</c:if>
			</li>
			</c:forEach>
		</ul>
		</c:if>
		</c:if>
	</ul>
</div>	
<script type="text/javascript">
$('#nav_left').menu_left({show:true});	
$(window).on('resize',function(){
        if($(window).width()<1024)
                $('#nav_left').menu_left({show:false });
        else
                $('#nav_left').menu_left({show:true });
});
</script>
