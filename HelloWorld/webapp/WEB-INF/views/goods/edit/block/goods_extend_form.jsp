<%@ include file="/WEB-INF/views/global/include.jsp"%>
<%@ page session="true" contentType="text/html;charset=utf-8"%>
<%@ include file="/WEB-INF/views/global/constant.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>${metroName} | ${menuName} | <%= SystemName %></title>
<%@ include file="/WEB-INF/views/global/css_javascript.jsp" %>
</head>
<body>
<form:form method="post" commandName="${formId}" action="${pageContext.request.contextPath}${baseURL}/${goodsExtend.goodsId}/${command}">	
<fieldset style="width:95%"><legend><label>連結其它網頁</label>
	<label><span>${message}</span></label>
	</legend>
	<div>
		<div style="float:left;overflow:hidden;width:280px;">			
			<c:if test="${not empty listExtend}">
			<c:forEach var="record" items="${listExtend}">
			<ul>
				<li><a href="${pageContext.request.contextPath}${baseURL}/${record.goodsId}/${record.id}/form">${record.title}</a></li>
			</ul>
			</c:forEach>
			</c:if>	
			<c:if test="${empty listExtend}">目前無資料。</c:if>
		</div>
		<div style="float:right;">
			<form:hidden path="id" /> 
			標題：<label>
			   	<form:input path="title" size="30" cssClass="required inline" /> 
					<form:errors path="title" cssClass="error inline" />
			</label>
			<div class="fbr"></div>
			
			URL：<label>
			   	<form:input path="url" size="30" cssClass="required inline" /> 
					<form:errors path="url" cssClass="error" />
			</label>
			<div class="fbr"></div>
				
			權重：<label><span>
			   	<form:input path="weight" size="3" cssClass="required inline" /> 
					<form:errors path="weight" cssClass="error" />
					</span>
					&nbsp;&nbsp;&nbsp;<span id="extend_message" class="error"></span>
			</label>
			<div class="fbr"></div>
			
			另開視窗：
			  	<form:radiobutton path="openWindow" value="1" cssClass="inline" /> 是
				<form:radiobutton path="openWindow" value="0" cssClass="inline" /> 否
			
			<div class="fbr"></div>
			<div id="btn">
				<dl style="display: block;text-align: right; margin-right: 30px;">
				<dt style="display: inline"><a href="javascript:void(0);" class="send" id="save_btn">儲存</a></dt>
				<dt style="display: inline"><a href="javascript:void(0);" class="send" id="reset_btn" title="按了之後，資料會恢復到未修改前的內容.">重寫</a></dt>
				<c:if test="${command != 'insert'}">
				<dt style="display: inline"><a href="javascript:void(0);" class="send" id="delete_btn">刪除</a></dt>
				<dt style="display: inline"><a href="javascript:void(0);" class="send" id="new_btn" title="要新增下一筆時，按此按鈕.">新增</a></dt>
				</c:if>		
				</dl>
			</div>
		</div>
	</div>	
</fieldset>
</form:form>
<script language="javascript">
$(document).ready(function(){
	$("#save_btn").click(function() { 
  	$('#${formId}').submit();
  });
  
  $("#reset_btn").click(function() { 
  	$('#${formId}').get(0).reset();
  	$('#title').focus();
  });
  
  <c:if test="${command != 'insert'}">
  $("#delete_btn").click(function() { 
  	if ( window.confirm( "刪除時，系統將無法再復原此筆記錄，確定要執行嗎 ?" )  == true ) {
  		window.location.href = '${pageContext.request.contextPath}${baseURL}/${goodsExtend.goodsId}/delete/${goodsExtend.id}';
  	}
  });
  $("#new_btn").click(function() { 
  	window.location.href = '${pageContext.request.contextPath}${baseURL}/${goodsExtend.goodsId}/form';
  });
  </c:if>
  $("#${formId}").validate();
  $('#title').focus(); 
});
</script>
</body>
</html>
