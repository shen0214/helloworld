<%@ include file="/WEB-INF/views/global/include.jsp"%>
<%@ page session="true" contentType="text/html;charset=utf-8"%>
<form:form method="post" commandName="${formId}"
	action="${pageContext.request.contextPath}${baseURL}/category/${category.id}/${command}">
	<label style="margin-left:30px;font-weight:bolder;line-height: 35px;">
		<c:forEach var="upperCategory" items="${upperCategoryList}">
				 ${upperCategory.name} ＞
		</c:forEach> <a	href="${pageContext.request.contextPath}/goods/edit/category/${category.id}/list">${category.name}</a>
		<c:if test="${not empty goodsInfo.headline}">
			＞ ${goodsInfo.headline}
		</c:if>
	</label>
	<form:hidden path="goodsId" />
	<form:hidden path="providerId" />
	<form:hidden path="rid" />
	<form:hidden path="insertTime" />
	<form:hidden path="topImageId" />
	<form:hidden path="topImageWeight" />
	<form:hidden path="upperCategoryId" />

	<form:hidden path="weight" value="99" />

	<dl class="goods_form_item">
		<dd><span>狀態</span></dd>
		<dd><form:select path="status" items="${statusList}" itemValue="code" itemLabel="name" cssClass="inline" /> <form:errors path="status" cssClass="error" />&nbsp;&nbsp;&nbsp;&nbsp;
			<span>商品類型</span>
<form:select path="goodsType" items="${typeList}" itemValue="code" itemLabel="name" cssClass="inline" /> <form:errors path="goodsType" cssClass="error" /> &nbsp;&nbsp;&nbsp;&nbsp;
		</dd>
	</dl>

	<dl class="goods_form_item">
		<dd><span class="red_star">*</span>來源 </dd>
		<dd><form:input path="sourceName" size="60" cssClass="required inline" /> <form:errors path="sourceName" cssClass="error" /></dd>
	</dl>
	<dl class="goods_form_item">
		<dd><span class="red_star">*</span>起</dd>
		<dd><form:input path="startTime" size="19" cssClass="required inline" />
		<input type="button" name="startTime_trigger" id="startTime_trigger" value="..."> <form:errors path="startTime" cssClass="error" />
			<script	type="text/javascript">
				new Calendar.setup({
					inputField : "startTime",
					dateFormat : "%Y/%m/%d %H:%M:00",
					trigger : "startTime_trigger",
					bottomBar : true,
					minuteStep : 1,
					showTime : 24
				});
			</script>&nbsp;&nbsp;&nbsp;&nbsp;
		<span class="red_star">*</span>訖<form:input	path="endTime" size="19" cssClass="required inline" />
		<input type="button" name="endTime_trigger" id="endTime_trigger" value="..."> <form:errors path="endTime" cssClass="error" />
			<script type="text/javascript">
				new Calendar.setup({
					inputField : "endTime",
					dateFormat : "%Y/%m/%d %H:%M:00",
					trigger : "endTime_trigger",
					bottomBar : true,
					minuteStep : 1,
					showTime : 24
				});
			</script>
		</dd>
	</dl>			
	<dl class="goods_form_item">
		<dd><span class="red_star">*</span>報名截止時間</dd>
		<dd><form:input path="closeTime" size="19" cssClass="required inline" />
		<input type="button" name="closeTime_trigger" id="closeTime_trigger" value="..."> <form:errors path="closeTime" cssClass="error" />
			<script type="text/javascript">
				new Calendar.setup({
					inputField : "closeTime",
					dateFormat : "%Y/%m/%d %H:%M:00",
					trigger : "closeTime_trigger",
					bottomBar : true,
					minuteStep : 1,
					showTime : 24
				});
			</script>
		</dd>
	</dl>
	<style>
		#startTime, #endTime, #closeTime{
			width:180px;
		}
	</style>	
	<dl class="goods_form_item">
		<dd><span>費用</span></dd>
		<dd><form:input path="price" size="19"	style="width:145px" /> &nbsp;&nbsp;&nbsp;&nbsp;
			<span>線上付款URL</span><form:input path="onPayUrl" size="19" style="width:250px" />
		</dd>
	</dl>			
	<dl class="goods_form_item">
		<dd><span class="red_star">標題</span></dd>
		<dd><form:input	path="headline" size="60" cssClass="required inline" /> <form:errors path="headline" cssClass="error" /></dd>
	</dl>			
	<dl class="goods_form_item">
		<dd><span>副標</span></dd>
		<dd><form:input	path="subHeadline" size="60" cssClass="inline" /> <form:errors path="subHeadline" cssClass="error" /></dd>
	</dl>
	<dl class="goods_form_item">
		<dd><span>摘要</span></dd>
		<dd><form:textarea path="summary" rows="3" cols="60" /> <form:errors path="summary" cssClass="error" /></dd>
	</dl>
	<dl class="goods_form_item">
		<dd><span class="red_star">*</span>行程導覽</dd>
		<dd>&nbsp; <c:if test="${command != 'insert'}">
			<a href="javascript:void(0);" id="insertOrangeTitle">內容</a> ]
	 		<a href="javascript:void(0);" id="insertSmallTitle">航班</a> ｜
			[ <a href="javascript:void(0);" id="insertImage">插入圖</a> ｜
			<a href="javascript:void(0);" id="insertVideo">插入影音</a> ｜
	 		<a href="javascript:void(0);" id="insertLink">插入連結</a> ｜
	 		<a href="javascript:void(0);" id="insertP">p</a> ｜
	 		<a href="javascript:void(0);" id="insertBr">br</a> ｜
	 		<a href="javascript:void(0);" id="insertBold">粗體</a> ｜
	 		</c:if> <br/>
			<form:textarea path="body" rows="15" cols="60" cssClass="required" /><form:errors path="body" cssClass="error" /></dd>
	</dl>

	<dl class="goods_form_item">
		<dd><span>商品標籤</span></dd>
		<dd><c:forEach var="goodsTag" items="${goodsTagList}">
			<input type="checkbox" value="${goodsTag.code}" class="tripTag" name="goodsTag" />${goodsTag.name}</c:forEach>
		</dd>
	</dl>

	<dl class="goods_form_item">
		<dd><span>地區</span></dd>
		<dd><c:forEach var="area" items="${areaList}">
			<input type="checkbox" value="${area.code}" class="tripArea" name="tripArea" />${area.name}</c:forEach>
		</dd>
	</dl>

	<dl class="goods_form_item">
		<dd><span>主辦單位</span></dd>
		<dd><form:textarea path="organizer" rows="2" cols="60" /></dd>
	</dl>
	<dl class="goods_form_item">
		<dd><span>協辦單位</span></dd>
		<dd><form:textarea path="coOrganizer" rows="2" cols="60" /></dd>
	</dl>
	<dl class="goods_form_item">
		<dd><span>贊助單位</span></dd>
		<dd><form:textarea path="sponsor" rows="2" cols="60" /></dd>
	</dl>
	<c:if test="${command != 'insert'}">
	<dl class="goods_form_item">
		<dd><span></span></dd>
		<dd><label><a href="javascript:void(0);" onClick="$('#image_btn').click();"">圖：<b><span id="pictureNumber">${goodsInfo.pictureNumber}</span></b> 張</a>，
			<a href="javascript:void(0);" onClick="$('#video_btn').click();">影音：<b><span id="videoNumber">${goodsInfo.videoNumber}</span></b> 則</a>。
		</label></dd>
	</dl>
	</c:if>
<!--		<label><span>新增時間：<fmt:formatDate
					pattern="yyyy/MM/dd HH:mm:ss"
					value="${goodsInfo.insertTime}" /></span></label> &nbsp;&nbsp; <label><span>最近修改：<fmt:formatDate
					pattern="yyyy/MM/dd HH:mm:ss"
					value="${goodsInfo.modifiedTime}" />
				(${goodsInfo.modifiedUserName})
		</span> </label>
-->
<!--	<div class="fbr"></div> Tag <label> <form:input path="tag"
			size="60" cssClass="inline" /> <form:errors path="tag"
			cssClass="error" />
	</label>
	<div class="fbr"></div>-->

</form:form>
<div class="space_h5"></div>

<!--<script>
		$(function() {
			$("#info").click(function() {
				$.colorbox({
					iframe : true,
					href : "/dropdown/dropdown.html",
					innerWidth : 350,
					innerHeight : 280,
					opacity : 0.25
				}); // end of colorbox.		
			}); // end of click.		
		});
</script>
<fieldset>
	<legend>
		<label>GPS &nbsp;<i id="info">&lt;i&gt;</i></label>
	</legend>
</fieldset>-->

<script">
	function init() {
		var areaCode = "${goodsInfo.areaCode}";
		var tripAreaStr = Number(areaCode).toString(2);
		var tripAreaLen = tripAreaStr.length;
		for (i = tripAreaLen - 1; i >= 0; i--) {
			if ((Math.pow(2, i) & areaCode)!=0)
				$(".tripArea[value='" + i + "']").attr("checked", "checked");
		}
		
		var tagCode ="${goodsInfo.goodsTag}";
		var tripTagStr=Number(tagCode).toString(2);
		var tripTagLen = tripTagStr.length;
		for (i = tripTagLen - 1; i >= 0; i--) {
			if ((Math.pow(2, i) & tagCode)!=0)
				$(".tripTag[value='" + i + "']").attr("checked", "checked");
		}

/*		$.each($("#goodsTag").children(), function(i, o) {
			var n1 = Math.pow(2, $(o).val());
			var n2 = ${goodsInfo.goodsTag};
			if (n1 == n2)
				$(o).attr("selected", "selected");
		});
*/
	}
	init();
	var actId = "";
	$(function() {
		$("#insertImage").click( function() {
			$.colorbox({
				href : "${pageContext.request.contextPath}/goods/edit/image/category/${goodsInfo.categoryId}/${goodsInfo.goodsId}/select",
				innerWidth : 300,
				innerHeight : 400,
				opacity : 0.25,
				onOpen : function() {
					$('html').css({
						overflow : 'hidden'
					});
				},
				onClosed : function() {
					$('html').css({
						overflow : 'auto'
					});
				}
			}); // end of colorbox.		
		});

		$("#insertVideo").click(function() {
			$.colorbox({
				href : "${pageContext.request.contextPath}${baseURL}/video/channel/${channelMain.id}/article/${goodsInfo.goodsId}/select",
				innerWidth : 300,
				innerHeight : 400,
				opacity : 0.25,
				onOpen : function() {
					$('html').css({
						overflow : 'hidden'
					});
				},
				onClosed : function() {
					$('html').css({
						overflow : 'auto'
					});
				}
			}); // end of colorbox.		
		});

		var selText = "";
		$("#insertLink").click(function() {
			if (!!$('#body').selection()) {
				selText = $('#body').selection();
				$.colorbox({
					href : "#insert_link",
					inline : true,
					innerWidth : 380,
					innerHeight : 200,
					opacity : 0.25
				}); // end of colorbox.		
			} else {
				alert("請先選擇要連結的文字");
			}
		});
		$("#insertAct").colorbox({
			iframe : true,
			href : "/backend/vote/addActive/${goodsInfo.goodsId}",
			innerWidth : 780,
			innerHeight : 500,
			opacity : 0.25,
			onOpen : function() {
				actId = "";
			},
			onClosed : function() {
				if (!!actId)
					$('#body').selection('insert', {
						text : '<!--@ACTIVE_' + actId + '@-->',
						mode : 'after'
					});
			}

		});

		$("#insertP").click(function() {
			$('#body').selection('insert', {
				text : '<p>',
				mode : 'after'
			});
		});

		$("#insertBr").click(function() {
			$('#body').selection('insert', {
				text : '<br>',
				mode : 'after'
			});
		});

		$("#insertBold").click(function() {
			if (!!$('#body').selection()) {
				$('#body').selection('insert', {
					text : '<strong>',
					mode : 'before'
				}).selection('insert', {
					text : '</strong>',
					mode : 'after'
				});
			} else {
				alert("請先選擇要粗體的文字");
			}
		});

		$("#insertSmallTitle").click(function() {
			if (!!$('#body').selection()) {
				$('#body').selection('insert', {
					text : '<strong><font color="0074ad">',
					mode : 'before'
				}).selection('insert', {
					text : '</font></strong>',
					mode : 'after'
				});
			} else {
				alert("請先選擇要作為小標題的文字");
			}
		});

		$("#insertOrangeTitle").click(function() {
			if (!!$('#body').selection()) {
				$('#body').selection('insert', {
					text : '<strong><font color="#FF8000">',
					mode : 'before'
				}).selection('insert', {
					text : '</font></strong>',
					mode : 'after'
				});
			} else {
				alert("請先選擇要作為小標題的文字");
			}
		});

		$("#addLink").click(
						function() {
							var linkText = $("#link_text").val();
							var replaceText = '<a href="' + linkText + '" target="_blank">'
									+ selText + '</a>';
							if (!!linkText) {
								$('#body').selection('replace', {
									text : replaceText
								});
								$.colorbox.close();
							} else {
								alert("請輸入URL");
							}

						});
	});
</script>

<div style='display: none'>
	<div id='insert_link'
		style='padding: 10px; background: #fff; display: inline;'>
		<form>
			<fieldset>
				<legend>
					<label>輸入網址後，插入至內文中.</label>
				</legend>
				<input type="text" id="link_text" size="30" style="width: 280px">
				<input type="button" value="確定" id="addLink">
			</fieldset>
		</form>
	</div>
</div>

