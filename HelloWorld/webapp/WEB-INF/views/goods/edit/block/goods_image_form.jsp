<%@ include file="/WEB-INF/views/global/include.jsp"%>
<%@ page session="true" contentType="text/html;charset=utf-8"%>
<%@ include file="/WEB-INF/views/global/constant.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>圖 | <%= SystemName %></title>
<%@ include file="/WEB-INF/views/global/css_javascript.jsp" %>
</head>
<body>
<form:form method="post" commandName="${formId}" action="${pageContext.request.contextPath}${baseURL}/goods/${goodsImage.imageId}/${goodsImage.goodsId}/${command}" enctype="multipart/form-data">	
<fieldset style="width:95%"><legend><label>圖/照片： 共 ${fn:length(listImage)} 張</label>
	<label><span class="error">${message}</span></label>
	</legend>
	<div>
		<div style="float:left; width:300px;height:500px;overflow:auto;">			
			<c:if test="${not empty listImage}">
			<c:forEach var="record" items="${listImage}">
			<ul>
				<li><c:if test="${record.toChoose == 1}"><c:set var="chooseCheck" value="checked" /></c:if>
				<input type="checkbox" name="toChoose" id="toChoose" value="1" ${chooseCheck} onClick="chooseImage(${record.imageId})">
				${record.imageId} - <a href="${pageContext.request.contextPath}${baseURL}/${record.goodsId}/${record.imageId}/form">${record.producer}</a></li>
				<li><a href="${pageContext.request.contextPath}${baseURL}/${record.goodsId}/${record.imageId}/form">
<c:if test="${record.imgW != 0}">
<c:url value="${pgwServer}/gw/photo.php" var="gwURL">
   <c:param name="u" value="/photo/${record.filePath}"/> 	
   <c:param name="s" value="Y"/> 
   <c:param name="x" value="${record.imgX}" />	
   <c:param name="y" value="${record.imgY}" />
   <c:param name="sw" value="${record.imgW}" />	
   <c:param name="sh" value="${record.imgH}" />
   <c:param name="w" value="330" />	
   <c:param name="h" value="225" />
   <c:param name="f" value="Y" />
</c:url>
</c:if>
<c:if test="${record.imgW == 0}">
<c:url value="${pgwServer}/gw/photo.php" var="gwURL">
   <c:param name="u" value="/photo/${record.filePath}"/> 	
   <c:param name="x" value="0" />	
   <c:param name="y" value="0" />
   <c:param name="w" value="330" />	
   <c:param name="h" value="225" />
   <c:param name="f" value="Y" />
</c:url>
</c:if>
					<img src="/photo/travel/${record.goodsId}/origin/${record.filePath}" style="max-width:200px;" border="0">
					</a></li>
			</ul>
			</c:forEach>
			</c:if>	
			<c:if test="${empty listImage}">目前無資料。</c:if>
		</div>
		<div style="float:right;">
			<form:hidden path="imageId" /> 
			<form:hidden path="copyright" /> 

	選擇上傳圖檔：<label>
   	<input type="file" name="uploadFile" id="uploadFile" value="" size="20" cssClass="inline" />
	</label>	
	<div class="space_h5"></div>
						
	攝影：<label>
   	<form:input path="producer" size="40" cssClass="inline" /> 
		<form:errors path="producer" cssClass="error inline" />
	</label>
	<div class="space_h5"></div>
	
	權重：<label>
   	<form:input path="weight" size="3" cssClass="required inline" style="width:150px" /> 
		<form:errors path="weight" cssClass="error inline" />
	</label>
	
	流水號：<label>
   	${goodsImage.imageId}
<c:if test="${goodsImage.imgW != 0}">
<c:url value="${pgwServer}/gw/photo.php" var="gwURL">
   <c:param name="u" value="/photo/${goodsImage.filePath}"/> 	
   <c:param name="s" value="Y"/> 
   <c:param name="x" value="${goodsImage.imgX}" />	
   <c:param name="y" value="${goodsImage.imgY}" />
   <c:param name="sw" value="${goodsImage.imgW}" />	
   <c:param name="sh" value="${goodsImage.imgH}" />
   <c:param name="w" value="48" />	
   <c:param name="h" value="36" />
   <c:param name="f" value="Y" />
</c:url>
</c:if>
<c:if test="${goodsImage.imgW == 0}">
<c:url value="${pgwServer}/gw/photo.php" var="gwURL">
   <c:param name="u" value="/photo/${goodsImage.filePath}"/> 	
   <c:param name="x" value="0" />	
   <c:param name="y" value="0" />
   <c:param name="w" value="48" />	
   <c:param name="h" value="36" />
   <c:param name="f" value="Y" />
</c:url>
</c:if>
<c:if test="${command != 'insert'}">
<img src="/photo/travel/${goodsImage.goodsId}/origin/${goodsImage.imageId}.jpg" style="max-width:100px;" border="0">
</c:if>
	</label>
	<div class="space_h5"></div>
	
	<c:if test="${channel.imageChoose == 0}">
	<form:hidden path="toChoose" /> 
	</c:if>
	<c:if test="${channel.imageChoose == 1}">
	選用：
   	<form:radiobutton path="toChoose" value="1" /><span>是</span>
  	<form:radiobutton path="toChoose" value="0" /><span>否</span>	
	
	<div class="space_h5"></div>
	</c:if>
		
	標題：<label>
   	<form:input path="title" size="30" style="width:300px" cssClass="inline" /> 
		<form:errors path="title" cssClass="error inline" />
	</label>
	<div class="space_h5"></div>
	
	圖說：<label>
		<form:textarea path="description" style="width:300px" rows="3" cols="30" /> 
		<form:errors path="description" cssClass="error inline" />
	</label>
		
			
			
			
			
			
	<div id="btn" style="width:350px;">
	<dl style="display:block;text-align: right; margin-right: 33px;">
		<dt style="display:inline;"><label><span id="image_message" class="red_star"></span></label></dt>
		<dt style="display:inline;"><a href="javascript:void(0);" class="send" id="save_btn">儲存</a></dt>
		<dt style="display:inline;"><a href="javascript:void(0);" class="send" id="reset_btn">重寫</a></dt>
		<c:if test="${command != 'insert'}">
		<dt style="display:inline;"><a href="javascript:void(0);" class="send" id="delete_btn">刪除</a></dt>
		<dt style="display:inline;"><a href="javascript:void(0);" class="send" id="new_btn">新增</a></dt>
		</c:if>  
	</dl>
	</div>
			
			
			
		</div>
		
		
		
		
		
	</div>	
</fieldset>
</form:form>

<script language="javascript">
$(document).ready(function(){
	$("#save_btn").click(function() { 
		$("#image_message").html("圖檔上傳中，請稍候 ...<img src='/images/colorbox/images/loading.gif'>") 
  	$('#${formId}').submit();
  	$("#image_message").html("");
  });
  
  $("#reset_btn").click(function() { 
  	$('#${formId}').get(0).reset();
  	$('#producer').focus();
  });
  
  <c:if test="${command != 'insert'}">
  $("#delete_btn").click(function() { 
  	if ( window.confirm( "刪除時，系統將無法再復原此筆記錄，確定要執行嗎 ?" )  == true ) {
  		window.location.href = '${pageContext.request.contextPath}${baseURL}/${goodsImage.goodsId}/delete/${goodsImage.imageId}';
  	}
  });
  $("#new_btn").click(function() { 
  	window.location.href = '${pageContext.request.contextPath}${baseURL}/${goodsImage.goodsId}/form';
  });
  </c:if>
  $("#${formId}").validate();
  $('#producer').focus(); 
});

function chooseImage(imageId) {
	var url = "${pageContext.request.contextPath}${baseURL}/${goodsImage.goodsId}/choose/" + imageId;
	$.get(url,
			function(msg) {
				if (msg == "OK") {
					$(".error").html("變更圖的選用完成。");				
				}
				else {
					$(".error").html("變更圖的選用失敗。");
					alert(msg);
				}
	  	}
		)
		.fail(function(xhr, textStatus, errorThrown) {
			$(".error").html("變更圖的選用失敗。");
			alert(xhr.responseText);
	  });
}

</script>
</body>
</html>
