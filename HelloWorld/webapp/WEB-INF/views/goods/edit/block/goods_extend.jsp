<%@ include file="/WEB-INF/views/global/include.jsp"%>
<%@ page session="true" contentType="text/html;charset=utf-8"%>
<fieldset><legend><label>延伸閱讀：</label></legend>
	<c:if test="${not empty listExtend}">
	<ul>
	<c:forEach var="record" items="${listExtend}">
	<li><a href="${record.url}" target="_extend">${record.title}</a></li>
	</c:forEach>
	</ul>	
	</c:if>
	
	<c:if test="${empty listExtend}">目前無資料。</c:if>
</fieldset>
