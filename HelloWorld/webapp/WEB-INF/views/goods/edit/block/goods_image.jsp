<%@ include file="/WEB-INF/views/global/include.jsp"%>
<%@ page session="true" contentType="text/html;charset=utf-8"%>
<c:set var="imageURL" value="" />
<c:set var="urlProtocol" value="http://" />
<c:if test="${pageContext.request.serverPort != 80}">
<c:set var="urlProtocol" value="https://" />
</c:if>

<c:set var="disableCheckbox" value="" />
<c:if test="${channel.imageChoose == 0}">
	<c:set var="disableCheckbox" value="disabled" />
</c:if>
<form>
<fieldset style="width:95%"><legend><label>圖/照片：</label></legend>
	<c:if test="${not empty listImage}">
	<c:forEach var="record" items="${listImage}">
		<fmt:formatDate var="modifyTime" pattern="yyyyMMddHHmmss" value="${record.modifiedTime}" />
		<c:url value="${pgwServer}/photo/${record.filePath}" var="imageURL">
		   <c:param name="seq" value="${modifyTime}"/> 
		</c:url>
	
	<fieldset><legend>
		<c:set var="chooseCheck" value="" />
		<c:if test="${record.toChoose == 1}">
			<c:set var="chooseCheck" value="checked" />
		</c:if>
		<label><input type="checkbox" name="toChoose" id="toChoose" value="1" ${chooseCheck} ${disableCheckbox} onClick="chooseImage(${record.imageId})"> <span>選用</span></label>
		 | (圖 - ${record.imageId})
		</legend>
		<c:if test="${not empty record.title}">
		標題：${record.title}
		<div class="fbr"></div>		
		</c:if>
		攝影：${record.producer}
		<br>
		<c:set var="defaultWith" value="320" />
		<c:set var="defaultHeight" value="225" />
		<c:if test="${category.articleImageWidth != 0}">
			<c:set var="defaultWith" value="${category.articleImageWidth}" />
		</c:if>
		<c:if test="${category.articleImageHeight != 0}">
			<c:set var="defaultHeight" value="${category.articleImageHeight}" />
		</c:if>
		<c:if test="${record.imgW != 0}">
		<c:url value="${pgwServer}/gw/photo.php" var="gwURL">
		   <c:param name="u" value="/photo/${record.filePath}"/> 	
		   <c:param name="s" value="Y"/> 
		   <c:param name="x" value="${record.imgX}" />	
		   <c:param name="y" value="${record.imgY}" />
		   <c:param name="sw" value="${record.imgW}" />	
		   <c:param name="sh" value="${record.imgH}" />
		   <c:param name="w" value="${defaultWith}" />	
		   <c:param name="h" value="${defaultHeight}" />
		   <c:param name="f" value="Y" />
		</c:url>
		</c:if>
		<c:if test="${record.imgW == 0}">
		<c:url value="${pgwServer}/gw/photo.php" var="gwURL">
		   <c:param name="u" value="/photo/${record.filePath}"/> 	
		   <c:param name="x" value="0" />	
		   <c:param name="y" value="0" />
		   <c:param name="w" value="${defaultWith}" />	
		   <c:param name="h" value="${defaultHeight}" />
		   <c:param name="f" value="Y" />
		</c:url>
		</c:if>
		<label><span><a href="/photo/travel/${record.goodsId}/origin/${record.filePath}" target="imageView"><img src="/photo/travel/${record.goodsId}/origin/${record.filePath}" style="max-width:400px;" border="0"></a></span></span></label>
		<br>	
		
		
		<fieldset><legend>圖說：</legend>
		${record.description}	
		</fieldset>	
	</fieldset>
	<div class="fbr"></div>
	</c:forEach>
	</c:if>
	<c:if test="${empty listImage}">目前無圖.</c:if>
	</fieldset>
</form>
<div class="space_h5"></div>
<div class="split_line"></div>
<div class="space_h5"></div>
<script>
$("#pictureNumber").html(${pictureNumber});

function chooseImage(imageId) {
	var url = "${pageContext.request.contextPath}${baseURL}/${channelGoods.goodsId}/choose/" + imageId;
	$.get(url,
			function(msg) {
				if (msg == "OK") {
					$("#goods_message").html("變更圖的選用完成。");				
				}
				else {
					$("#goods_message").html("變更圖的選用失敗。");
					alert(msg);
				}
	  	}
		)
		.fail(function(xhr, textStatus, errorThrown) {
			$("#goods_message").html("變更圖的選用失敗。");
			alert(xhr.responseText);
	  });
}
</script>
