<%@ include file="/WEB-INF/views/global/include.jsp"%>
<%@ page session="true" contentType="text/html;charset=utf-8"%>
<c:set var="currentURL" value="${pageContext.request.contextPath}/feeder/article/menu"/>  
<div id="table_list">
	<form method="post" name="frmQuery" id="frmQuery" action="javascript:void(0);">
	 		<select name="col" id="col" class="inline">
				<c:forEach var="field" items="${pageParam.queryFields}">
				<option value="${field.key}"<c:if test="${field.key == pageParam.col}"> selected</c:if>>${field.value}</option>
				</c:forEach>
			</select>
			<input type="text" name="kw" id="kw" size="4" value="${pageParam.kw}"  class="inline" />
			<input type="hidden" name="order" id="order" value="${pageParam.orderBy}" />
			<input type="button" name="cmdQuy" value="查詢"  class="inline" onClick="queryData()">
			<c:if test="${not empty pageParam.kw && not empty pageParam.col}">
			<br>
			<a href="javascript:void(0);" onClick="reloadTableList('${currentURL}')">顯示全部</a>
			</c:if>			
	</form>
	<script>
	function queryData() {
		data = $("#frmQuery").serialize();
		$("#navigation").load("${currentURL}", data, function() {
		  $("table tr").hover(function() {
		  		$(this).css('background','#ffffaa');
		  		$(this).css('cursor','pointer');
		  }, function() {
		      $(this).css('background','');
		      $(this).css('cursor','auto');
		  });
		});
	};
	
	function reloadTableList(theUrl) {
		$("#navigation").load(theUrl, function() {
		  $("table tr").hover(function() {
		  		$(this).css('background','#ffffaa');
		  		$(this).css('cursor','pointer');
		  }, function() {
		      $(this).css('background','');
		      $(this).css('cursor','auto');
		  });
		});
	}
	</script>
	<table>
		<tr>			
			<c:set var="orderParam" value="kw=${pageParam.kwCode}&col=${pageParam.col}"/>			
			<th>
				稿別 <a href='javascript:reloadTableList("${currentURL}?order=type&${orderParam}")'><img src="/img/stock_down.gif" border="0" /></a>
			</th>
			<th>
				標題 <a href='javascript:reloadTableList("${currentURL}?order=headline&${orderParam}")'><img src="/img/stock_down.gif" border="0" /></a> 
				進稿時間 <a href='javascript:reloadTableList("${currentURL}?order=insertTime&${orderParam}")'><img src="/img/stock_down.gif" border="0" /></a>
			</th>
		</tr>
		<c:forEach var="record" items="${pageView.records}" varStatus="rowCounter">
			<c:choose>
				<c:when test="${(rowCounter.count % 2) == 0}">
          <c:set var="rowStyle" scope="page" value="#FFFFFF"/>
        </c:when>
        <c:otherwise>
          <c:set var="rowStyle" scope="page" value="#EEF7FF"/>
        </c:otherwise>
    	</c:choose>
		<tr bgcolor="${rowStyle}" onClick="loadArticle(${record.id})">					
			<td>
				${record.typeName}
			</td>									
			<td>
				${record.headline}
				<br>
				[<fmt:formatDate pattern="yyyy/MM/dd HH:mm:ss" value="${record.insertTime}" />]
			</td>
		</tr>
		</c:forEach>
	</table>
		
		<c:choose>
			<c:when test="${pageView.totalRecord == 0}">
	   		目前沒有記錄可供瀏覽。
	  	</c:when>
			<c:when test="${pageView.totalPage == 1}">
		 		目前共有 ${pageView.totalRecord} 筆。
			</c:when>
			<c:otherwise>
				<c:set var="paramString" value="order=${pageParam.orderBy}&${orderParam}" />	  
				共 <b>${pageView.totalRecord}</b> 筆，<select name='pageselect' onChange='reloadTableList(this.options[this.selectedIndex].value)'>
	  			<c:forEach var="j" begin="${pageView.pageIndex.startIndex}" end="${pageView.pageIndex.endIndex}">
						<option value='${currentURL}?pno=${j-1}&${paramString}'<c:if test="${(j-1) == pageView.currentPage}"> selected</c:if>>${j}</option>
					</c:forEach>
		 		</select> / <b>${pageView.totalPage}</b> 頁。
				<br>
				<c:choose>
					<c:when test="${pageView.currentPage > 0}">
	   				<a href='javascript:reloadTableList("${currentURL}?pno=0&${paramString}")'><font color=blue>第一頁</font></a> |
	   				<a href='javascript:reloadTableList("${currentURL}?pno=${pageView.currentPage - 1}&${paramString}")'><font color=blue>上一頁</font></a> |
	  			</c:when>
	  			<c:otherwise>
						第一頁 | 上一頁 |
		      </c:otherwise>
	      </c:choose>
	      <c:choose>
					<c:when test="${pageView.currentPage < (pageView.totalPage-1)}">
	   			<a href='javascript:reloadTableList("${currentURL}?pno=${pageView.currentPage + 1}&${paramString}")'><font color=blue>下一頁</font></a> |
	   			<a href='javascript:reloadTableList("${currentURL}?pno=${pageView.totalPage-1}&${paramString}")'><font color=blue>最末頁</font></a>
		     	</c:when>
		     	<c:otherwise>
	   				下一頁 | 最末頁
	  			</c:otherwise>
	  		</c:choose>
			</c:otherwise>
	  </c:choose>
</div>