<%@ include file="/WEB-INF/views/global/include.jsp"%>
<%@ page session="true" contentType="text/html;charset=utf-8"%>
<c:set var="urlProtocol" value="http://" />
<c:if test="${pageContext.request.serverPort != 80}">
<c:set var="urlProtocol" value="https://" />
</c:if>
<form>
<fieldset><legend><label>點選圖片後，插入至內文中.</label></legend>
	<c:if test="${not empty listImage}">
	<c:forEach var="record" items="${listImage}">
	<fieldset><legend>圖 - ${record.imageId}</legend>		
		攝影：${record.producer}
		<br>
	
		<c:if test="${record.imgW != 0}">
		<c:url value="${pgwServer}/gw/photo.php" var="gwURL">
		   <c:param name="u" value="/photo/${record.filePath}"/> 	
		   <c:param name="s" value="Y"/> 
		   <c:param name="x" value="${record.imgX}" />	
		   <c:param name="y" value="${record.imgY}" />
		   <c:param name="sw" value="${record.imgW}" />	
		   <c:param name="sh" value="${record.imgH}" />
		   <c:param name="w" value="330" />	
		   <c:param name="h" value="225" />
		   <c:param name="f" value="Y" />
		</c:url>
		</c:if>
		<c:if test="${record.imgW == 0}">
		<c:url value="${pgwServer}/gw/photo.php" var="gwURL">
		   <c:param name="u" value="/photo/${record.filePath}"/> 	
		   <c:param name="x" value="0" />	
		   <c:param name="y" value="0" />
		   <c:param name="w" value="330" />	
		   <c:param name="h" value="225" />
		   <c:param name="f" value="Y" />
		</c:url>
		</c:if>
		
		<label><span><a href="javascript:void(0);" onClick="insertImageTag(${record.imageId})"><img src="${gwURL}" style="max-width:120px;" border="0"></a></span></span></label>
		<br>	
		<label><input type="radio" id="pos_${record.imageId}" name="pos_${record.imageId}" value="LEFT"> 靠左</label>
		<label><input type="radio" id="pos_${record.imageId}" name="pos_${record.imageId}" value="CENTER" checked> 置中</label>
		<label><input type="radio" id="pos_${record.imageId}" name="pos_${record.imageId}" value="RIGHT"> 靠右</label>
	</fieldset>
	<div class="fbr"></div>
	</c:forEach>
	</c:if>
	<c:if test="${empty listImage}">目前無圖.</c:if>
	</fieldset>
</form>
<div class="space_h5"></div>
<div class="split_line"></div>
<div class="space_h5"></div>
<script>
function insertImageTag(imageId) {	
	var position = "CENTER";
	position = $('input[name=pos_' + imageId + ']:checked').val();
	parent.$('#body').insertAtCaret('<!--@IMAGE_'+ imageId + '_' + position + '@-->');
	$.colorbox.close();
};
</script>
