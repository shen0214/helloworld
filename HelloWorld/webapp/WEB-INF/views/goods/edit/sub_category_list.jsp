<%@ include file="/WEB-INF/views/global/include.jsp"%>
<%@ include file="/WEB-INF/views/global/constant.jsp" %>
<%@ page session="true" contentType="text/html;charset=utf-8"%>
<ul>${upperCategory.name}
	<c:if test="${not empty subCategoryList}">
	  <c:forEach var="tempCategory" items="${subCategoryList}">
			<li>
			<a href="javascript:void(0);" onClick="parent.updateData('${command}', ${channelMain.id}, ${tempCategory.id})">${tempCategory.name}</a>
			</li>
		</c:forEach>
	</c:if>		
	<c:if test="${empty subCategoryList}">
	沒有次類別 !!!
	</c:if>	
</ul>