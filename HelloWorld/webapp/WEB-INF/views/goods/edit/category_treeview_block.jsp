<%@ include file="/WEB-INF/views/global/include.jsp"%>
<%@ page session="true" contentType="text/html;charset=utf-8"%>
<c:set var="subCategoryTree" value="${channelCategoryTree[currentCategory.id + 0]}" />
<c:if test="${not empty subCategoryTree}">
<ul>			
  <c:forEach var="tempCategory" items="${subCategoryTree}">
		<c:set var="currentCategory" value="${tempCategory}" scope="request" />
		<li>
			<a href="${pageContext.request.contextPath}${baseURL}/category/${tempCategory.id}/list">${tempCategory.name} (${tempCategory.unEditGoods})</a>
			&nbsp;
			<a href="${pageContext.request.contextPath}${baseURL}/goods/category/${tempCategory.id}/view" class="plus">[+]</a>
			<% // 危險：遞迴呼叫，可能會無限迴圈, 設定 loopCount 最多執行十層. %>
			<c:set var="loopCount" value="${loopCount+1}" scope="request" />
			<c:if test="${loopCount < 10}">
				<jsp:include page="category_treeview_block.jsp"/>
			</c:if>
		</li>
	</c:forEach>
</ul>
</c:if>