<%@ include file="/WEB-INF/views/global/include.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page session="false" contentType="text/html;charset=utf-8"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<c:set var="curMenu" value="edit" />      
<c:set var="menuName" value="商品" />
<c:set var="metro" value="index" />
<c:set var="metroName" value="商品編輯" />
<title>${menuName} | ${webName}</title>
<%@ include file="/WEB-INF/views/global/css_javascript.jsp"%>
</head>
<body>
<div class="container">
	<%@ include file="/WEB-INF/views/global/menu.jsp"%>
	<div id="content">
	<%@ include file="category_treeview.jsp" %> 
		<!-- /#navigation -->
		<div id="content_body">
	<div id="message_box"><span id="goods_message">${message}</span></label></div>
		<div id="goods_form"></div>    		
     		<div id="goods_extend"></div>  		
     		<div id="goods_video"></div>     		
 
		<fieldset><legend>修改記錄</legend>
			<div id="table_log"></div>
		</fieldset>
		
	</div>

	<c:set var="editExtendURL" value="${pageContext.request.contextPath}${baseURL}/extend/${goodsInfo.goodsId}" />    
	<c:set var="editImageURL" value="${pageContext.request.contextPath}${baseURL}/image/category/${goodsInfo.categoryId}/goods/${goodsInfo.goodsId}" />
	<c:set var="editGoodsURL" value="${pageContext.request.contextPath}${baseURL}/goods/category/${goodsInfo.categoryId}" />

	<div id="btn" style="width:10%;float:right;position:fixed;top:200px;right:50px;">
		<dl>
			<dt><a href="javascript:void(0);" class="send" id="save_btn">儲存</a></dt>
			<dt><a href="javascript:void(0);" class="send" id="reset_btn">重寫</a></dt>
			<c:if test="${command != 'insert'}">	  
			<dt><a href="${editGoodsURL}/${goodsInfo.goodsId}/preview" class="send" id="preview_btn" target="_preview">預覽</a></dt>	
			<dt><a href="${pageContext.request.contextPath}${baseURL}/image/${goodsInfo.goodsId}/form" class="send" id="image_btn">圖</a></dt>
			<dt><a href="${editVideoURL}/form" class="send" id="video_btn">影音</a></dt>		  	
			<dt><a href="${editExtendURL}/form" class="send" id="extend_btn">延伸閱讀</a></dt>
			<dt><a href="javascript:void(0);" class="send" id="new_btn">新增</a></dt>
			<dt><a href="javascript:void(0);" class="send" id="delete_btn">刪除</a></dt>	
			</c:if>
			<%@ include file="/WEB-INF/views/include/copyright.jsp" %>
		</dl>
		
	</div>
			 
</div><!-- /container -->
<script src="/js/classie.js"></script>
<script src="/js/gnmenu.js"></script>
<script>
	new gnMenu( document.getElementById( 'menu' ) );
</script>
<script>
$(function () {	
	$("#navigation").treeview({
		persist: "location",
		collapsed: true,
		unique: true,		
		persist: "cookie",
		cookieOptions:{			
			path : '/'
		}
	});	
	
  $("#reset_btn").click(function() { 
  	$('#${formId}').get(0).reset();
  	$('#weight').focus();
  });
 
  <c:if test="${command != 'insert'}">
  
  $("#image_btn").colorbox({
 		iframe:true, innerWidth:800, innerHeight:580,
 		opacity:0.25,onOpen:function(){
 			$('html').css({
        overflow: 'hidden'
    	});
 		},
 		onClosed:function(){
// 			$("#goods_image").load("${editImageURL}/show");
 			$('html').css({
        overflow: 'auto'
    	});
 		}
 	});
  
  $("#extend_btn").colorbox({
 		iframe:true, innerWidth:700, innerHeight:330,
 		opacity:0.25,
 		onOpen:function(){
 			$('html').css({
        overflow: 'hidden'
    	});
 		},
 		onClosed:function(){
 			$("#goods_extend").load("${editExtendURL}/show");
 			$('html').css({
        overflow: 'auto'
    	});
 		}
 	});
	
	$("#save_btn").click(function() {
		if ($("#${formId}").valid() != true) {
			$("#goods_message").html("資料有問題，無法儲存 !");
			return;
		}
		$("#goods_message").html("<center>資料更新中 ...<img src='/images/colorbox/images/loading.gif'></center>") 
		var data = $("#${formId}").serialize();
		$.post("${editGoodsURL}/update", data, function(msg) {
			if (msg == "OK") {
				$("#goods_form").load("${editGoodsURL}/${goodsInfo.goodsId}/form", function(){	  		
					$("#${formId}").validate();
	  			$('#weight').focus();  
	  			$("#goods_message").html("更新完成.");
//	  			$("#goods_image").load("${editImageURL}/show");
				});
			}
			else {
				$("#goods_message").html(msg);
			}
	  });
  });
  
  $("#delete_btn").click(function() { 
  	if ( window.confirm( "按下確定後將無法再復原此筆資料，確定要執行嗎 ?" )  == true ) {
  		window.location.href = '${editGoodsURL}/delete/${goodsInfo.goodsId}';
  	}
  });
  
  $("#new_btn").click(function() { 
  	if ( window.confirm( "確定要開啟新的編輯畫面嗎 ?" )  == true ) {
  		window.location.href = '${editGoodsURL}/view';
  	}
  });
  
  
  loadArticle();
  </c:if>
  
  <c:if test="${command == 'insert'}"> 
  $("#save_btn").click(function() { 
  	if ($("#${formId}").valid() != true) return;
  	$("#${formId}").submit();
  });
  
  $("#goods_form").load("${editGoodsURL}/form",
  	function() {
  		$("#${formId}").validate();
  		$('#weight').focus();
  	}
  );
  </c:if>
});

function loadArticle() {
	$("#goods_message").html("<center>商品載入中 ...<img src='/images/colorbox/images/loading.gif'></center>")
 	
	$("#goods_form").load("${editGoodsURL}/${goodsInfo.goodsId}/form",
  	function() {
  		$("#${formId}").validate();
  		$('#weight').focus();  
  		$("#goods_message").html("商品載入完成.");
  	}
  );
  
  $("#goods_extend").load("${editExtendURL}/show");
//  $("#goods_image").load("${editImageURL}/show");
};

function cutImage(theImgUrl, cutImageUrl) {
	var cutURL = "/crop.jsp?photo=" + encodeURIComponent(theImgUrl) + "&cuturl=" + encodeURIComponent(cutImageUrl);
	var sidebarZindex = $('#sidebar').zIndex();
	$.colorbox({
	 iframe: "true",
	 innerWidth: "90%", 
	 innerHeight:"90%",
	 href: cutURL,
	 onOpen:function(){
	 		$('#sidebar').zIndex(1);
 			$('html').css({
        overflow: 'hidden'
    	});
 		},
 		onClosed:function(){
 			$('#sidebar').zIndex(sidebarZindex);
 			$('html').css({
        overflow: 'auto'
    	});
//    	$("#goods_image").load("${editImageURL}/show");
 		}
	});
};
$('#table_log').load('${pageContext.request.contextPath}/log/view/goods_info/${goodsInfo.categoryId}_${goodsInfo.goodsId}');
</script>
		
		
	</body>
</html>
