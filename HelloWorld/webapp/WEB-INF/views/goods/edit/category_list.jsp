<%@ include file="/WEB-INF/views/global/include.jsp"%>
<%@ page session="true" contentType="text/html;charset=utf-8"%>
<ul>${channelMain.name}
	<c:if test="${not empty channelCategory}">
	  <c:forEach var="tempCategory" items="${channelCategory}">
			<li>
				<c:if test="${tempCategory.subCategoryNumber > 0}">
				<a href="javascript:void(0);" onClick="loadSubCategory(${tempCategory.id})">${tempCategory.name}</a>
				</c:if>
				<c:if test="${tempCategory.subCategoryNumber <= 0}">
				<a href="javascript:void(0);" onClick="parent.updateData('${command}', ${channelMain.id}, ${tempCategory.id})">${tempCategory.name}</a>
				</c:if>
			</li>
		</c:forEach>
	</c:if>	
	<c:if test="${empty channelCategory}">
	尚未建置類別資料 !!!
	</c:if>	
</ul>
