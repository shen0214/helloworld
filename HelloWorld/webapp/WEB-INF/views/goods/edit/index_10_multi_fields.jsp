<%@ include file="/WEB-INF/views/global/include.jsp"%>
<%@ page session="true" contentType="text/html;charset=utf-8"%>
<%@ include file="/WEB-INF/views/global/constant.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<c:set var="curMenu" value="edit" />
<c:set var="menuName" value="編輯" />
<c:set var="metro" value="index" />
<c:set var="metroName" value="頻道" />
<title>${metroName} | ${menuName} | <%= SystemName %></title>
<%@ include file="/WEB-INF/views/global/css_javascript.jsp" %>
</head>
<body>
<div id="container_metro"></div>
<div id="container">
  <div id="wrapper"> 
    <div id="content">             
     	<%@ include file="category_treeview.jsp" %>             
     	<!-- /#navigation -->
     	<div id="content_body">
<fieldset><legend>[<b>${channelMain.name} <c:if test="${not empty category}"> - ${category.name}</c:if></b>] 的稿件.</legend>
<div id="data_log">
<c:set var="categoryURL" value="" />
<c:if test="${not empty category}"><c:set var="categoryURL" value="/category/${category.id}" /></c:if>
<c:set var="currentURL" value="${pageContext.request.contextPath}${baseURL}/channel/${channelMain.id}${categoryURL}/list"/>  
<c:set var="orderParam" value="kw=${pageParam.kwCode}&col=${pageParam.col}&status=${pageParam.status}&duration=${pageParam.duration}"/>		

<div id="table_list">	
	<%@ include file="/WEB-INF/views/goods/include/page_link.jsp" %>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;目前按 <b>${pageParam.orderByTitle}</b> 排序.
	<form method="post" name="frmQuery" id="frmQuery" action="${currentURL}">			  	
	<table>
		<tr><td></td></tr>
		<tr>
			<th colspan="5">			
				排序：<select name="order" id="order" onChange="$('#frmQuery').submit()" class="inline">
							<c:forEach var="field" items="${pageParam.orderMap}">
							<option value="${field.key}"<c:if test="${field.key == pageParam.orderBy}"> selected</c:if>>${field.value}</option>
							</c:forEach>
						</select>
				&nbsp;
				查詢：<select name="col" id="col" class="inline">
							<c:forEach var="field" items="${pageParam.queryFields}">
							<option value="${field.key}"<c:if test="${field.key == pageParam.col}"> selected</c:if>>${field.value}</option>
							</c:forEach>
						</select>
						<input type="text" name="kw" id="kw" size="6" value="${pageParam.kw}"  class="inline" />
						<input type="hidden" name="order" id="order" value="${pageParam.orderBy}" />
						
						<input type="submit" name="cmdQuy" value="Go"  class="inline">
						<c:if test="${not empty pageParam.kw && not empty pageParam.col}">
						<input type="button" name="cmdShowAll" value="全部" onClick='reloadTableList("${currentURL}")'  class="inline">
						</c:if>
				&nbsp;
				狀態：<select name="status" id="status" class="inline" onChange="$('#frmQuery').submit()">
					<c:forEach var="statusItem" items="${articleStatus}">
							<option value="${statusItem.code}"<c:if test="${statusItem.code == pageParam.status}"> selected</c:if>>${statusItem.name}</option>
					</c:forEach>
							<option value="-1"<c:if test="${-1 == pageParam.status}"> selected</c:if>>全部</option>
				</select>
				
				&nbsp;
				<select name="duration" id="duration" class="inline" onChange="$('#frmQuery').submit()">
					<c:forEach var="durationItem" items="${articleDuration}">
							<option value="${durationItem.code}"<c:if test="${durationItem.code == pageParam.duration}"> selected</c:if>>${durationItem.name}</option>
					</c:forEach>
					<option value="-1"<c:if test="${-1 == pageParam.duration}"> selected</c:if>>全部</option>					
				</select>
			</th>
		</tr>
		<tr><td></td></tr>
		
	<c:if test="${not empty pageView.records}">	
		<c:forEach var="record" items="${pageView.records}" varStatus="rowCounter">
			<c:choose>
				<c:when test="${(rowCounter.count % 2) == 0}">
          <c:set var="rowStyle" scope="page" value="#F8F8F8"/>
        </c:when>
        <c:otherwise>
          <c:set var="rowStyle" scope="page" value="#EEF7FF"/>
        </c:otherwise>
    	</c:choose>
		<tr bgcolor="${rowStyle}">
			<td rowspan="5">
				<label><input type="checkbox" name="article_chk" id="article_chk" value="${record.goodsId}"> ${record.tripId}
				</label>
				<input type="hidden" name="goodsId" id="goodsId" value="${record.goodsId}">
				<input type="hidden" name="category_${record.goodsId}" id="category_${record.goodsId}" value="${record.categoryId}">
			</td>	
			<td colspan="3">
				<input type="text" id="headline_${record.goodsId}" name="headline_${record.goodsId}" value="${record.headline}">			</td>	
			<td rowspan="5">
				<a href="${pageContext.request.contextPath}${baseURL}/article/channel/${channelMain.id}/category/${record.categoryId}/${record.goodsId}/view">編輯</a>
			</td>					
		</tr>
		<tr bgcolor="${rowStyle}">
			<td>
				進稿時間：<fmt:formatDate pattern="yyyy/MM/dd HH:mm:ss" value="${record.insertTime}" />				
			</td>	
			<td>
				見報日：<fmt:formatDate pattern="yyyy/MM/dd" value="${record.presentDate}" />
			</td>	
			<td>
				類別：${record.categoryName}
			</td>
		</tr>
		<tr bgcolor="${rowStyle}">
			<td>
				${record.articleTypeName} 《${record.sourceName}》
			</td>	
			<td>
				圖：${record.pictureNumber} 張，影音：${record.videoNumber} 則。
			</td>
			<td>
				狀態：<select name="stats_${record.goodsId}" id="status_${record.goodsId}" class="inline">
					<c:forEach var="statusItem" items="${articleStatus}">
							<option value="${statusItem.code}"<c:if test="${statusItem.code == record.status}"> selected</c:if>>${statusItem.name}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr bgcolor="${rowStyle}">
			<td>
				上線：<input type="text" id="onlineTime_${record.goodsId}" name="onlineTime_${record.goodsId}" value="<fmt:formatDate pattern="yyyy/MM/dd HH:mm:00" value="${record.onlineTime}" />" size="20" readonly class="inline">				
					<script type="text/javascript">
				    new Calendar.setup({
				        inputField: "onlineTime_${record.goodsId}",
				        dateFormat: "%Y/%m/%d %k:%M:00",
				        trigger: "onlineTime_${record.goodsId}",
				        bottomBar: true,
				        weekNumbers: true,
				        showTime: 24,
				        onSelect: function() {this.hide();},
				        onChange: function() {
	                this.setHours(new Date().getHours());
	                this.setMinutes(new Date().getMinutes());
            		}
				    });
					</script>
			</td>
			<td>
				下線：<input type="text" id="offlineTime_${record.goodsId}" name="offlineTime_${record.goodsId}" value="<fmt:formatDate pattern="yyyy/MM/dd HH:mm:00" value="${record.offlineTime}" />" size="20" readonly class="inline" >
					<script type="text/javascript">
				    new Calendar.setup({
				        inputField: "offlineTime_${record.goodsId}",
				        dateFormat: "%Y/%m/%d %k:%M:00",
				        trigger: "offlineTime_${record.goodsId}",
				        bottomBar: true,
				        weekNumbers: true,
				        showTime: 24,
				        onSelect: function() {this.hide();},
				        onChange: function() {
	                this.setHours(new Date().getHours());
	                this.setMinutes(new Date().getMinutes());
            		}
				    });
					</script>	
			</td>
			<td>
				權重：<input type="text" id="weight_${record.goodsId}" name="weight_${record.goodsId}" size="4" value="${record.weight}" class="inline">
			</td>
		</tr>
		<tr><td></td></tr>
		</c:forEach>			
	</c:if>
	</table>
	<input type="hidden" name="target_category" id="target_category" value="">
	<input type="hidden" name="target_channel" id="target_channel" value="">
	</form>
	<%@ include file="/WEB-INF/views/goods/include/page_link.jsp" %>
</div>
</div>
</fieldset>
  		</div>
      <!-- /#content_body -->
      <%@ include file="/WEB-INF/views/global/main_menu.jsp" %>
      <!-- /#metro_channel -->
      <%@ include file="/WEB-INF/views/include/metro.jsp" %>
      <!-- /#metro_sort -->        
    </div> 
    <!-- /#content -->
    <%@ include file="/WEB-INF/views/include/sidebar.jsp" %>     
    <!-- /#sidebar -->
    <div id="btn">
    	<dl>
    		<dt><div id="message_box">
					<label><span id="article_message"></span></label>
					<div class="fbr"></div>
				</div></dt>
    	</dl>
		  <dl>		  					
				<dt><a href="javascript:void(0);" class="send" id="save_btn">儲存</a></dt>
		    <dt><a href="javascript:void(0);" class="send" id="move_btn">移稿</a></dt>
		    <dt><a href="javascript:void(0);" class="send" id="copy_btn">複製</a></dt>
		    <dt><a href="javascript:void(0);" class="send" id="reset_btn">重寫</a></dt>
		    <c:if test="${not empty category}">
		    <dt><a href="javascript:void(0);" class="send" id="publish_category_btn">類別發行</a></dt>
		    <dt><a href="javascript:void(0);" class="send" id="publish_article_btn">單則發行</a></dt>
		    </c:if>
		    <%@ include file="/WEB-INF/views/include/copyright.jsp" %>
		  </dl>
		</div>
    <!-- /#btn -->
	</div> 
	<!-- /#wrapper -->   
</div> 
<!-- /#container -->
<script language=javascript>
$(document).ready(function(){
	$("#navigation").treeview({
		persist: "location",
		collapsed: true,
		unique: true
	});
		
	$("table tr").unbind('mouseenter mouseleave');
	
	$("#publish_category_btn").click(function() {
		$.colorbox({html:"<center>類別發行中 ...<img src='/images/colorbox/images/loading.gif'></center>"}); 
		$.get("${pageContext.request.contextPath}/publish/category/channel/${channelMain.id}${categoryURL}",
			function(msg) {
				$.colorbox.close();
				if (msg == "OK") {
					alert("發行完成。");
					reloadTableList("${currentURL}?${orderParam}&pno=${pageView.currentPage}");
				}
				else {
					$("#article_message").html("發行失敗 !");
					alert(msg);
				}
	  	}
		)
		.fail(function(xhr, textStatus, errorThrown) {
			$.colorbox.close();
			$("#article_message").html("發行失敗 !");
			alert(xhr.responseText);
	  });
  });
  
	
	$("#save_btn").click(function() {
		updateData("update", 0, 0);
  });
  
  $("#reset_btn").click(function() {
		$('#frmQuery').get(0).reset();
  });
  
  $("#move_btn").click(function() {
  	var isChecked = false;
  	$('input:checkbox:checked[name="article_chk"]').each(function(i) { isChecked = true; });
  	if ( !isChecked) {
  		alert("請選擇要移稿的新聞 !")
  		return;
  	}
		
		$.colorbox({
  		href:"${pageContext.request.contextPath}${baseURL}/list/${channelMain.id}/channel/move"
  	});
  });
  
  $("#copy_btn").click(function() {
  	var isChecked = false;
  	$('input:checkbox:checked[name="article_chk"]').each(function(i) { isChecked = true; });
  	if ( !isChecked) {
  		alert("請選擇要複製的新聞 !")
  		return;
  	}
		
		$.colorbox({
  		href:"${pageContext.request.contextPath}${baseURL}/list/${channelMain.id}/channel/copy"
  	});
  });
});	

function reloadTableList(theUrl) {
	window.location.href = theUrl;
}

function updateData(command, targetChannel, targetCategory) {
	$("#target_channel").val(targetChannel);
	$("#target_category").val(targetCategory);
	$("#article_message").html("<center>資料更新中 ...<img src='/images/colorbox/images/loading.gif'></center>") 
	var data = $("#frmQuery").serialize();
	$.post("${pageContext.request.contextPath}${baseURL}/channel/${channelMain.id}${categoryURL}/" + command, data, 
		function(msg) {
			if (msg == "OK") {
				$("#article_message").html("更新完成。");
				reloadTableList("${currentURL}?${orderParam}&pno=${pageView.currentPage}");
			}
			else {
				$("#article_message").html("更新失敗 !");
				alert(msg);
			}
  	}
	)
	.fail(function(xhr, textStatus, errorThrown) {
		$("#article_message").html("更新失敗 !");
		alert(xhr.responseText);
  });
}
</script>
</body>
</html>
