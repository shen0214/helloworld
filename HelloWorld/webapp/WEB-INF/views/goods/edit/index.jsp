<%@ include file="/WEB-INF/views/global/include.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page session="false" contentType="text/html;charset=utf-8"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<c:set var="curMenu" value="edit" />
<c:set var="menuName" value="商品" />
<c:set var="metro" value="index" />
<c:set var="metroName" value="商品管理" />
<c:set var="categoryId" value="0" />
<c:if test="${not empty category}">
<c:set var="categoryId" value="${category.id}" />
</c:if>
<title>${menuName} | ${webName}</title>
<%@ include file="/WEB-INF/views/global/css_javascript.jsp"%>
</head>
<body>
<div class="container">
	<%@ include file="/WEB-INF/views/global/menu.jsp"%>
	<div id="content">
	<%@ include file="category_treeview.jsp" %>  
	<!-- /#navigation -->

	<div id="content_body">
	<c:if test="${categoryLimit != 'true'}">
		<legend><b> <c:if test="${not empty category}">[ ${category.name}]</c:if></b></legend>
		<div id="data_log">
		<c:set var="categoryURL" value="" />
		<c:if test="${not empty category}"><c:set var="categoryURL" value="/category/${category.id}" /></c:if>
		<c:set var="currentURL" value="${pageContext.request.contextPath}${baseURL}${categoryURL}/list"/>  
		<c:set var="orderParam" value="kw=${pageParam.kwCode}&col=${pageParam.col}&status=${pageParam.status}"/>

		<c:if test="${not empty pageView.records}">	
		<div id="table_list">	
		<%@ include file="../include/page_link.jsp" %>
		依商品建置時間排序.
			<form method="post" name="frmQuery" id="frmQuery" action="${currentURL}">			  	
			<table>
				<tr>
				<th colspan="10">
				<c:forEach var="field" items="${pageParam.queryFields}"><input type="hidden" value="${field.key}" name="col"/> ${field.value}</c:forEach>
					<input type="text" name="kw" id="kw" size="8" value="${pageParam.kw}"  class="inline" />
					<input type="hidden" name="order" id="order" value="${pageParam.orderBy}" /><input type="submit" name="cmdQuy" value="查詢"  class="inline">
					&nbsp;	狀態：<select name="status" id="status" class="inline" onChange="$('#frmQuery').submit()">
					<c:forEach var="statusItem" items="${goodsStatus}">
						<option value="${statusItem.code}"<c:if test="${statusItem.code == pageParam.status}"> selected</c:if>>${statusItem.name}</option>
					</c:forEach>
						<option value="-1"<c:if test="${-1 == pageParam.status}"> selected</c:if>>全部</option>
					</select>
				</th>
				</tr>
				<tr><td></td></tr>


				<tr>
				<th nowrap></th>
				<th nowrap>標題</th>
				<th nowrap>狀態</th>
				<th nowrap>來源</th>
				<th nowrap>圖</th>
				<th nowrap>新增時間</th>
				<th nowrap>&nbsp;</th>
				<th style="font-size:15px">
					<a href="javascript:void(0)" id="pick_all" name="pick_all" title="全選" class="glyph-icon flaticon-accepted"></a>
					<a href="javascript:void(0)" id="cancel_all" name="cancel_all" title="全取消" class="glyph-icon flaticon-x3"></a></th>
				</tr>

				<c:forEach var="record" items="${pageView.records}" varStatus="rowCounter">
				<c:choose>
				<c:when test="${(rowCounter.count % 2) == 0}">
				<c:set var="rowStyle" scope="page" value="#F8F8F8"/>
				</c:when>
				<c:otherwise>
				<c:set var="rowStyle" scope="page" value="#EEF7FF"/>
				</c:otherwise>
				</c:choose>
				<tr bgcolor="${rowStyle}">
					<td>
						<a style="margin:0;" class="glyph-icon flaticon-pencil5" href="${pageContext.request.contextPath}${baseURL}/goods/category/${record.categoryId}/${record.goodsId}/view"></a>
					</td>	
					<td>
						<input type="text" id="headline_${record.goodsId}" name="headline_${record.goodsId}" value="${record.headline}" class="inline" size="25">
						<input type="hidden" id="headline2_${record.goodsId}" name="headline2_${record.goodsId}" value="${record.headline}">
					</td>	
					<td>
						<select name="status_${record.goodsId}" id="status_${record.goodsId}" class="inline">
						<c:forEach var="statusItem" items="${goodsStatus}">
							<option value="${statusItem.code}"<c:if test="${statusItem.code == record.status}"> selected</c:if>>${statusItem.name}</option>
						</c:forEach>
						</select>
						<input type="hidden" id="status2_${record.goodsId}" name="status2_${record.goodsId}" value="${record.status}">
					</td>
					<td nowrap>
					${fn:substring(record.sourceName, 0, 3)}
					</td>
					<td nowrap>
					${record.pictureNumber},${record.videoNumber}
					</td>
					<td nowrap>
						<fmt:formatDate pattern="MM/dd HH:mm" value="${record.insertTime}" />
					</td>
					<td nowrap>
						<a class="icon icon-search" href="${pageContext.request.contextPath}${baseURL}/category/${record.categoryId}/${record.goodsId}/preview" target="_preview"></a>	
					</td>	
					<td nowrap>
						<input type="checkbox" name="goods_chk" id="goods_chk" value="${record.goodsId}">
						<input type="hidden" name="goodsId" id="goodsId" value="${record.goodsId}">
						<input type="hidden" name="target_${record.goodsId}" id="target_${record.goodsId}" value="${record.categoryId}">
						<input type="hidden" name="category_${record.goodsId}" id="category_${record.goodsId}" value="${record.categoryId}">
					</td>	
				</tr>
				</c:forEach>			
			</table>
			<input type="hidden" name="target_category" id="target_category" value="">
			<input type="hidden" name="target_channel" id="target_channel" value="">
			</form>
			<%@ include file="../include/page_link.jsp" %>
		</div>


		<div id="btn" style="width:10%;float:right;position: absolute;top:200px;right:50px;">
	    		<dl>
				<dt><div id="message_box">
			 		<label><span id="article_message"></span></label>
					<div class="fbr"></div>
				</div></dt>
			</dl>

			<dl>		  					
			<dt><a href="javascript:void(0);" class="send" id="save_btn">儲存</a></dt>
			<dt><a href="javascript:void(0);" class="send" id="move_btn">搬移</a></dt>
<!--			<dt><a href="javascript:void(0);" class="send" id="share_btn">共享</a></dt>-->
			<dt><a href="javascript:void(0);" class="send" id="reset_btn">重寫</a></dt>
			<c:if test="${not empty category}">
			<dt><a href="javascript:void(0);" class="send" id="new_btn">新增</a></dt>
			<dt><a href="javascript:void(0);" class="send" id="clone_btn">複製</a></dt>
			</c:if>
	 		<%@ include file="/WEB-INF/views/include/copyright.jsp" %>
			</dl>
    <!-- /#btn -->
		</div> 

		</c:if>
	</div>
	</c:if>
	<c:if test="${categoryLimit == 'true'}">
	請點選左側你被授權編輯的類別 ...
	</c:if>
				
</div><!-- /container -->
<script src="/js/classie.js"></script>
<script src="/js/gnmenu.js"></script>
<script>
	new gnMenu( document.getElementById( 'menu' ) );
</script>
<script>
$(document).ready(function(){
	$("#navigation").treeview({
		persist: "location",
		collapsed: true,
		unique: true,		
		persist: "cookie",
		cookieOptions:{			
			path : '/'
		}
	});
		
	$("table tr").unbind('mouseenter mouseleave');
	
	$("#pick_all").click(function() {
		$("input[name='goods_chk']").each(function() {
			if (!$(this).prop("checked")) {
				$(this).prop("checked", true);
			}
		});
	});
	
	$("#cancel_all").click(function() {
		$("input[name='goods_chk']").each(function() {
			if ($(this).prop("checked")) {
				$(this).prop("checked", false);
			}
		});
	});
	
	$("#weight_all_btn").click(function() {
		$("input[type=text]").each(function() {
			if ($(this).attr("name").indexOf("weight_") != -1) {
				$(this).val($("#weightall").val());
			}
		});
	});
	
	$("#status_all_btn").click(function() {
		var selectIndex = $("#statusall").val();
		$("select").each(function() {
			if ($(this).attr("name").indexOf("status_") != -1) {    		
				$(this).val(selectIndex);
			}
		});
	});
     
	$("#new_btn").click(function() {
		window.location.href = "${pageContext.request.contextPath}${baseURL}/goods${categoryURL}/view";
	});
  
	$("#save_btn").click(function() {
		updateData("update", 0, 0);
	});
  
	$("#clone_btn").click(function() {
		updateData("clone", 0, 0);
	});

	$("#reset_btn").click(function() {
		$('#frmQuery').get(0).reset();
	});
  
	$("#move_btn").click(function() {
		var isChecked = false;
		$('input:checkbox:checked[name="goods_chk"]').each(function(i) { isChecked = true; });
		if ( !isChecked) {
			alert("請選擇要搬移的商品 !")
			return;
		}
		
		$.colorbox({
			href:"${pageContext.request.contextPath}${baseURL}/list/${categoryId}/move",
			onOpen:function(){
				$('html').css({
	        			overflow: 'hidden'
	    			});
	 		},
	 		onClosed:function(){
	 			$('html').css({
	        			overflow: 'auto'
	    			});
			}
		});
	});
  
	$("#share_btn").click(function() {
		var isChecked = false;
		$('input:checkbox:checked[name="goods_chk"]').each(function(i) { isChecked = true; });
		if ( !isChecked) {
		 	alert("請選擇要共享的商品 !")
			return;
		}

		$.colorbox({
			href:"${pageContext.request.contextPath}${baseURL}/list/${categoryId}/copy",
			onOpen:function(){
				$('html').css({
					overflow: 'hidden'
				});
			},
			onClosed:function(){
				$('html').css({
					overflow: 'auto'
				});
			}
		});
 	});
});	

function reloadTableList(theUrl) {
	window.location.href = theUrl;
}

function updateData(command, targetChannel, targetCategory) {
	if ( window.confirm( "確定嗎 ?" ) != true ) {
		return;
	}
	if ($.colorbox) $.colorbox.close();
	$("#target_channel").val(targetChannel);
	$("#target_category").val(targetCategory);
	$("#article_message").html("<center>資料更新中 ...<img src='/images/colorbox/images/loading.gif'></center>") 
	var data = $("#frmQuery").serialize();
	$.post("${pageContext.request.contextPath}${baseURL}${categoryURL}/" + command, data, 
		function(msg) {
			if (msg == "OK") {
				$("#article_message").html("更新完成。");
				reloadTableList("${currentURL}?${paramString}&pno=${pageView.currentPage}");
			}
			else {
				$("#article_message").html("更新失敗 !");
				alert(msg);
			}
  		}
	)
	.fail(function(xhr, textStatus, errorThrown) {
		$("#article_message").html("更新失敗 !");
		alert(xhr.responseText);
	});
}
</script>
</body>
</html>
