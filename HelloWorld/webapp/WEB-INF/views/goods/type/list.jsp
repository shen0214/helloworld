<div id="nav_left">
        <div class="d_toggle"></div>
	<ul id="navigation" class="treeview">商品類型管理
	<c:if test="${not empty goodsTypeList}">
	  <c:forEach var="tempGoodsType" items="${goodsTypeList}">
			<li>
					<a href="${pageContext.request.contextPath}${baseURL}/${tempGoodsType.code}">${tempGoodsType.name}</a>
				</li>
		</c:forEach>
	</c:if>	
	<c:if test="${empty goodsTypeList}">
	尚未建置商品類型 !!!
	</c:if>	
	</ul>
</div>
<script type="text/javascript">
$('#nav_left').menu_left({show:true});
$(window).on('resize',function(){
        if($(window).width()<1024)
                $('#nav_left').menu_left({show:false });
        else
                $('#nav_left').menu_left({show:true });
});
</script>
