<div id="nav_left">
        <div class="d_toggle"></div>
	<ul id="navigation" class="treeview">商品標籤管理
	<c:if test="${not empty goodsTagList}">
	  <c:forEach var="tempGoodsTag" items="${goodsTagList}">
			<li>
					<a href="${pageContext.request.contextPath}${baseURL}/${tempGoodsTag.code}">${tempGoodsTag.name}</a>
				</li>
		</c:forEach>
	</c:if>	
	<c:if test="${empty goodsTagList}">
	尚未建置商品連結 !!!
	</c:if>	
	</ul>
</div>
<script type="text/javascript">
$('#nav_left').menu_left({show:true});
$(window).on('resize',function(){
        if($(window).width()<1024)
                $('#nav_left').menu_left({show:false });
        else
                $('#nav_left').menu_left({show:true });
});
</script>
