<%@ include file="/WEB-INF/views/global/include.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page session="false" contentType="text/html;charset=utf-8"%><html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<c:set var="curMenu" value="goods" />
		<c:set var="menuName" value="商品" />
		<c:set var="metro" value="index" />
		<c:set var="metroName" value="商品地區管理" />
		<title>${menuName} | ${webName}</title>
		<%@ include file="/WEB-INF/views/global/css_javascript.jsp"%>
	</head>
	<body>
		<div class="container">
			<%@ include file="/WEB-INF/views/global/menu.jsp"%>
			<div id="content">
				<%@ include file="list.jsp"%>
				<!-- /#navigation -->
				<div id="content_body">

					<form:form method="post" commandName="${formId}"
						action="${pageContext.request.contextPath}${baseURL}/${command}">
						<dl class="form_item">
							<dd><span class="red_star">* </span><span>地區名稱：</span></dd>
							<dd><form:input path="area" maxlength="30"
								cssClass="required inline" /> <form:errors path="area"
								cssClass="error" /></dd></dl>
							<form:hidden path="areaCode" />
					</form:form>

					<div  class="btn_area_horizontal">
						<c:if test="${not empty message}"><div id="message_box_horizontal"><span>${message}</span></div></c:if>
						<a href="javascript:void(0);" class="send" id="save_btn">儲存</a>&nbsp;&nbsp;&nbsp;
						<a href="javascript:void(0);" class="send" id="reset_btn"
							title="按了之後，資料會恢復到未修改前的內容.">重寫</a>&nbsp;&nbsp;&nbsp;
					<c:if test="${command != 'insert'}">
							<a href="javascript:void(0);" class="send" id="delete_btn">刪除</a>&nbsp;&nbsp;&nbsp;
							<a href="javascript:void(0);" class="send" id="new_btn"
								title="要新增下一筆時，按此按鈕.">新增</a>
					</c:if>

					<%@ include file="/WEB-INF/views/include/copyright.jsp"%>
					</div>
					<div class="split_line"></div>
                                        <fieldset>
                                             <legend> 最近修改記錄 </legend>
                                              <div id="table_log"></div>
                                        </fieldset>




				
				</div>
				
				
			</div>
			 
		</div><!-- /container -->
		<script src="/js/classie.js"></script>
		<script src="/js/gnmenu.js"></script>
		<script>
			new gnMenu( document.getElementById( 'menu' ) );
		</script>
	<script>
function init(){
	<c:if test="${command=='insert'}">
	$('#id').val("");
	</c:if>}
init();

$(function () {
	$("#save_btn").click(function() { 
  	$('#${formId}').submit();
  });
  $("#reset_btn").click(function() { 
  	$('#${formId}').get(0).reset();
	init();
  	$('#id').focus();
  });
  
  $("#navigation").treeview({
		persist: "cookie",
		cookieId: "channel_main",
		cookiePath: "/",
		collapsed: true,
		unique: false
	});
  
  <c:if test="${command != 'insert'}">
  $("#delete_btn").click(function() { 
  	if ( window.confirm( "刪除時，系統將無法再復原此筆記錄，確定要執行嗎 ?" )  == true ) {
   		window.location.href = '${pageContext.request.contextPath}${baseURL}/${goodsArea.areaCode}/delete';
  	}
  });
  $("#new_btn").click(function() { 
  	window.location.href = '${pageContext.request.contextPath}${baseURL}/';
  });

  </c:if>
  
  
  $("#${formId}").validate();
	<c:if test="${channelMain.id == 0}">
  $('#id').focus();
  </c:if>
  <c:if test="${channelMain.id != 0}">
  $('#code').focus();
  </c:if>
});
$('#table_log').load('${pageContext.request.contextPath}/log/view/goods_area<c:if test="${command != 'insert'}">/${goodsArea.areaCode}</c:if>');
</script>
		
	</body>
</html>
