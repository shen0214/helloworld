<div id="nav_left">
        <div class="d_toggle"></div>
	<ul id="navigation" class="treeview">商品地區管理
	<c:if test="${not empty goodsAreaList}">
	<c:forEach var="tempGoodsArea" items="${goodsAreaList}">
		<li>
		<a href="${pageContext.request.contextPath}${baseURL}/${tempGoodsArea.code}">${tempGoodsArea.name}</a>
		</li>
	</c:forEach>
	</c:if>	
	<c:if test="${empty goodsAreaList}">
	尚未建置商品地區 !!!
	</c:if>	
	</ul>
</div>
<script type="text/javascript">
$('#nav_left').menu_left({show:true});
$(window).on('resize',function(){
        if($(window).width()<1024)
                $('#nav_left').menu_left({show:false });
        else
                $('#nav_left').menu_left({show:true });
});
</script>
