<c:choose>
	<c:when test="${pageView.totalRecord == 0}">
  		目前沒有記錄可供瀏覽。
 	</c:when>
	<c:when test="${pageView.totalPage == 1}">
 		目前共有 ${pageView.totalRecord} 筆。
	</c:when>
	<c:otherwise>
		<c:set var="paramString" value="order=${pageParam.orderBy}&${orderParam}" />	  
		共 <b>${pageView.totalRecord}</b> 筆，<b>${(pageView.currentPage+1)}</b> / <b>${pageView.totalPage}</b> 頁，到第
 		<select name='pageselect' onChange='reloadTableList(this.options[this.selectedIndex].value)' class="inline" o>
 			<c:forEach var="j" begin="${pageView.pageIndex.startIndex}" end="${pageView.pageIndex.endIndex}">
				<option value='${currentURL}?pno=${j-1}&${paramString}'<c:if test="${(j-1) == pageView.currentPage}"> selected</c:if>>${j}</option>
			</c:forEach>
 		</select> 頁。
		<c:choose>
			<c:when test="${pageView.currentPage > 0}">
  				<a href='javascript:reloadTableList("${currentURL}?pno=0&${paramString}")'><font color=blue>第一頁</font></a> |
  				<a href='javascript:reloadTableList("${currentURL}?pno=${pageView.currentPage - 1}&${paramString}")'><font color=blue>上一頁</font></a> |
 			</c:when>
 			<c:otherwise>
				第一頁 | 上一頁 |
      </c:otherwise>
     </c:choose>
     <c:choose>
			<c:when test="${pageView.currentPage < (pageView.totalPage-1)}">
  			<a href='javascript:reloadTableList("${currentURL}?pno=${pageView.currentPage + 1}&${paramString}")'><font color=blue>下一頁</font></a> |
  			<a href='javascript:reloadTableList("${currentURL}?pno=${pageView.totalPage-1}&${paramString}")'><font color=blue>最末頁</font></a>
     	</c:when>
     	<c:otherwise>
  				下一頁 | 最末頁
 			</c:otherwise>
 		</c:choose>
	</c:otherwise>
</c:choose>
