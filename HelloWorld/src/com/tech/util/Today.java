package com.tech.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Today {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(Today.class);

	public static Locale LIB_LOCALE = Locale.TAIWAN;
	public static String LIB_TIMEZONE = "Asia/Taipei";

	public static Date getDate() {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(LIB_TIMEZONE), LIB_LOCALE);
		return calendar.getTime();
	}

	public static Date addDay(int day) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(LIB_TIMEZONE), LIB_LOCALE);
		calendar.add(Calendar.DATE, day);
		return calendar.getTime();
	}

	public static Date addHours(int hours) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(LIB_TIMEZONE), LIB_LOCALE);
		calendar.add(Calendar.HOUR, hours);
		return calendar.getTime();
	}

	public static Date addMinute(int minute) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(LIB_TIMEZONE), LIB_LOCALE);
		calendar.add(Calendar.MINUTE, minute);
		return calendar.getTime();
	}

	public static Date addDay(Date date, int day) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(LIB_TIMEZONE), LIB_LOCALE);
		calendar.setTime(date);
		calendar.add(Calendar.DATE, day);
		return calendar.getTime();
	}

	public static Date addHours(Date date, int hours) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(LIB_TIMEZONE), LIB_LOCALE);
		calendar.setTime(date);
		calendar.add(Calendar.HOUR, hours);
		return calendar.getTime();
	}

	public static Date addMinute(Date date, int minute) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(LIB_TIMEZONE), LIB_LOCALE);
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, minute);
		return calendar.getTime();
	}

	public static Date getDateWithoutTime() {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(LIB_TIMEZONE), LIB_LOCALE);
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}
}