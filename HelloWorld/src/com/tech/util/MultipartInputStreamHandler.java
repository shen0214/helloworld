package com.tech.util;

import java.io.IOException;
import javax.servlet.ServletInputStream;

class MultipartInputStreamHandler {
	ServletInputStream in;
	int totalExpected;
	int totalRead;
	byte buf[];

	public MultipartInputStreamHandler(ServletInputStream in, int totalExpected) {
		totalRead = 0;
		buf = new byte[8192];
		this.in = in;
		this.totalExpected = totalExpected;
	}

	public String readLine() throws IOException {
		StringBuffer sbuf = new StringBuffer();
		int result;
		// Read data from ServletInputStream While Crlf is Reached.
		do {
			// Call this.readLine
			result = readLine(buf, 0, buf.length);
			if (result != -1) sbuf.append(new String(buf, 0, result, "ISO-8859-1"));
		}
		while (result == buf.length);

		if (sbuf.length() == 0) {
			return null;
		}
		else {
			sbuf.setLength(sbuf.length() - 2);
			return sbuf.toString();
		}
	}

	public int readLine(byte b[], int off, int len) throws IOException {
		// Check Total Read Length
		if (totalRead >= totalExpected) return -1;
		// Reset the Read Length
		if (len > totalExpected - totalRead) len = totalExpected - totalRead;
		int result = in.readLine(b, off, len);
		if (result > 0) totalRead += result;
		return result;
	}
}