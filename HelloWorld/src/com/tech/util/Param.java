package com.tech.util;

import java.text.SimpleDateFormat;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Param {
	private static final Logger logger = LoggerFactory.getLogger(Param.class);


	public Param(HttpServletRequest _request) {
		request = _request;
		max_file_size = 10 * 1024 * 1024;
		is_multipart = isMultiPart();
		codePage = Options.PARAM_CODEPAGE;
	}

	public Param(HttpServletRequest _request, int max_file_size_mb) {
		request = _request;
		if (max_file_size_mb == 0) max_file_size = 10 * 1024 * 1024;
		else max_file_size = max_file_size_mb * 1024 * 1024;
		is_multipart = isMultiPart();
		codePage = Options.PARAM_CODEPAGE;
	}

	public void setRequest(HttpServletRequest _request) {
		request = _request;
		is_multipart = isMultiPart();
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}

	static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

	public boolean is_multipart = false;
	private HttpServletRequest request;
	private MultipartRequest2 mr2;
	private int max_file_size = 10 * 1024 * 1024;

	private String codePage = "BIG5";

	public MultipartRequest2 getMultipartRequest() {
		return mr2;
	}

	public void setMaxFileSize(int max_size) {
		max_file_size = max_size;
	}

	public int getMaxFileSize() {
		return max_file_size;
	}

	public boolean isMultiPart() {
		try {
			if (request.getMethod().toLowerCase().equals("post") != true)	return false;
			if (request.getHeader("content-type").toLowerCase().indexOf("multipart/form-data") == -1) return false;
			mr2 = new MultipartRequest2(request, max_file_size);
			mr2.readRequestAndCheckFileSize();
			return true;
		}
		catch (Exception e) {
			logger.error( e.getMessage() );
			return false;
		}
	}

	public String get(String name) {
		if (is_multipart) {
			return StringUtil.nullToSpace((String) mr2.getParameter(name));
		}
		else {
			if (codePage.equals("BIG5"))
				return CodeConvert.toBig5String(StringUtil.nullToSpace((String) request.getParameter(name)));
			
			if (codePage.equals("UTF-8"))
				return CodeConvert.toUTF8String(StringUtil.nullToSpace((String) request.getParameter(name)));
			
			return StringUtil.nullToSpace((String) request.getParameter(name));
		}
	}

	public String[] getValues(String name) {
		String[] values;
		if (is_multipart) values = mr2.getParameterValues(name);
		else {
			values = request.getParameterValues(name);
			if (values != null && values.length > 0) {
				for (int j = 0; j < values.length; j++) {
					if (codePage.equals("BIG5"))
						values[j] = CodeConvert.toBig5String(StringUtil.nullToSpace(values[j]));
					else if (codePage.equals("UTF-8"))
						values[j] = CodeConvert.toUTF8String(StringUtil.nullToSpace(values[j]));
					else values[j] = StringUtil.nullToSpace(values[j]);
				}
			}
		}
		return values;
	}

	public int getInt(String name) throws Exception {
		if ( get(name).length() == 0 ) return 0;
		try {
			return Integer.parseInt(get(name));
		}
		catch (NumberFormatException e) {
			throw new Exception("'" + name + "' �������ƭ� ! �Э��s��J.");
		}
	}

	public long getLong(String name) throws Exception {
		try {
			return Long.parseLong(get(name));
		}
		catch (NumberFormatException e) {
			throw new Exception("��� '" + name + "' �������ƭ� ! �Э��s��J.");
		}
	}

	public boolean getBoolean(String name) {
		if (get(name).equals("true")) return true;
		return false;
	}

	public byte getByte(String name) throws Exception {
		try {
			return Byte.parseByte(get(name));
		}
		catch (NumberFormatException e) {
			throw new Exception("��� '" + name + "'  �������ƭ� ! �Э��s��J.");
		}
	}

	public double getDouble(String name) throws Exception {
		try {
			return Double.parseDouble(get(name));
		}
		catch (NumberFormatException e) {
			throw new Exception("��� '" + name + "'  �������ƭ� ! �Э��s��J.");
		}
	}

	public String getParameterUrl(String name) throws Exception {
		String url = get(name);
		if (url.length() == 0) return "";
		if (!url.startsWith("http://") && !url.startsWith("https://")
				&& !url.startsWith("ftp://")) {
			throw new Exception("��� '" + name
					+ "' ������ http:// �� https:// �� ftp:// ! �Э��s��J.");
		}
		return url;
	}

	public java.util.Date getDate(String name) throws Exception {
		try {
			return dateFormat.parse(get(name));
		}
		catch (java.text.ParseException e) {
			throw new Exception("������ '" + name + "' �榡���~, �������褸 yyyy/mm/dd ! �Э��s��J.");
		}
	}

	public java.util.Date getDate(String paramYear, String paramMonth,
			String paramDay) throws Exception {
		StringBuffer buffer = new StringBuffer(10);
		buffer.append(getInt(paramYear)).append("/").append(getInt(paramMonth))
				.append("/").append(getInt(paramDay));
		try {
			return dateFormat.parse(buffer.toString());
		}
		catch (java.text.ParseException e) {
			throw new Exception("������ '" + paramYear + "/" + paramMonth + "/" + paramDay
					+ "' �榡���~, �������褸 yyyy/mm/dd ! �Э��s��J.");
		}
	}

	public String getAttribute(HttpSession session, String name) {
		return StringUtil.nullToSpace((String) session.getAttribute(name)).trim();
	}

	public String getAttribute(String name) {
		return StringUtil.nullToSpace((String) request.getAttribute(name)).trim();
	}
	
	public String getAttribute(String name, boolean isGetParam) {
		String retStr = StringUtil.nullToSpace((String) request.getAttribute(name)).trim();
		if ( retStr.length() > 0 || !isGetParam ) return retStr;
		return get(name);
	}

	public String getServer() {
		StringBuffer server = new StringBuffer(128);
		// ���o�ثe�ĥΪ� Protocol, Like http, https, ftp
		String scheme = request.getScheme();
		int port = request.getServerPort();
		if (port < 0) port = 80; 
		server.append(scheme);
		server.append("://");
		server.append(request.getServerName());
		if ((scheme.equals("http") && (port != 80))
				|| (scheme.equals("https") && (port != 443))) {
			server.append(':');
			server.append(port);
		}
		return server.toString();
	}

	public String getCookie(String ckname) throws Exception {
		String cook_value = "";
		Cookie[] cks = request.getCookies();
		if (cks != null) {
			for (int j = 0; j < cks.length; j++) {
				if (cks[j].getName().equals(ckname)) {
					cook_value = cks[j].getValue();
					// cook_value = URLCodeLib.decodeData(cook_value);
					// cook_value = URLDecoder.decode(cook_value,"UTF-8");
					break;
				}
			}
		}
		return cook_value;
	}
	public String getCodePage() {
		return codePage;
	}
	public void setCodePage(String codePage) {
		this.codePage = codePage;
	}
}