package com.tech.util;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;

import com.tech.util.CodeConvert;
import com.tech.util.Options;
import com.tech.util.StringUtil;

public class MultipartRequest2 {
	private HttpServletRequest req;
	private MultipartInputStreamHandler in;
	private int maxPostSize;
	private Hashtable<String, ParameterObj> parameters;

	private String codePage = "BIG5";

	public MultipartRequest2(HttpServletRequest request) {
		init(request);
	}

	private void init(HttpServletRequest request) {
		parameters = new Hashtable<String, ParameterObj>();
		if (request == null) throw new IllegalArgumentException("request cannot be null");
		req = request;
		maxPostSize = 0;
		codePage = Options.MULTIPART_CODEPAGE;
		return;
	}

	public MultipartRequest2(HttpServletRequest request, int max) {
		init(request);
		maxPostSize = max;
	}

	private String extractBoundary(String line) {
		int index = line.lastIndexOf("boundary=");
		if (index == -1) return null;
		String boundary = line.substring(index + 9);
		return ("--" + boundary);
	}

	private String extractContentType(String line) throws IOException {
		if (line.length() == 0) return null;
		String lowLine = line.toLowerCase();
		if (lowLine.startsWith("content-type")) {
			int start = lowLine.indexOf(" ");
			if (start == -1) throw new IOException("Content type corrupt: " + line);
			return lowLine.substring(start + 1);
		}
		throw new IOException("Malformed line after disposition: " + line);
	}

	private String[] extractDispositionInfo(String line) throws IOException {
		if (line == null || line.length() == 0) throw new IOException("Content disposition corrupt.");
		String retval[] = new String[3];
		boolean isFile = false;
		String lowline = line.toLowerCase();

		int start = lowline.indexOf("content-disposition: ");
		int end = lowline.indexOf(";");
		if (start == -1 || end == -1) throw new IOException("Content disposition corrupt: " + line);
		String disposition = lowline.substring(start + 21, end);
		if (!disposition.equals("form-data")) throw new IOException("Invalid content disposition: " + disposition);
		start = lowline.indexOf("name=\"", end);
		end = lowline.indexOf("\"", start + 7);
		if (start == -1 || end == -1) throw new IOException("Content disposition corrupt: " + line);
		String name = line.substring(start + 6, end);
		// Get File Name
		String filename = null;
		start = lowline.indexOf("filename=\"", end + 2);
		end = lowline.indexOf("\"", start + 10);
		if (start != -1 && end != -1) {
			isFile = true;
			filename = line.substring(start + 10, end);
			if (filename.equals("")) filename = null;
		}
		// Set Return vale into String[]
		retval[0] = name;
		retval[1] = filename;
		if (isFile) retval[2] = "true";
		else retval[2] = "false";
		// Return the Disposition Information
		return retval;
	}

	public String getFileContentType(String name) {
		try {
			ParameterObj po = (ParameterObj) parameters.get(name);
			if (po == null) return null;
			return po.fileContentType;
		}
		catch (Exception _ex) {
			return null;
		}
	}

	public InputStream getFileStream(String name) {
		try {
			ParameterObj po = (ParameterObj) parameters.get(name);
			if (po == null) return null;
			if (po.value == null) return null;
			ByteArrayOutputStream baos = (ByteArrayOutputStream) po.value;
			return new ByteArrayInputStream(baos.toByteArray());
		}
		catch (Exception _ex) {
			return null;
		}
	}

	public int getFileStreamSize(String name) {
		try {
			ParameterObj po = (ParameterObj) parameters.get(name);
			if (po == null) return 0;
			if (po.value == null) return 0;
			ByteArrayOutputStream baos = (ByteArrayOutputStream) po.value;
			return baos.size();
		}
		catch (Exception _ex) {
			return 0;
		}
	}

	public String getFilename(String name) {
		try {
			ParameterObj po = (ParameterObj) parameters.get(name);
			if (po == null) return null;

			if (codePage.equals("BIG5")) return CodeConvert.toBig5String(po.filename);

			if (codePage.equals("UTF-8")) return CodeConvert.toUTF8String(po.filename);

			return po.filename;
		}
		catch (Exception _ex) {
			return null;
		}
	}

	public Object getParameter(String name) {
		try {
			ParameterObj po = (ParameterObj) parameters.get(name);
			if (po == null) return null;
			if (po.isFile) return po.value;
			if (po.values.size() == 0) return null;

			if (codePage.equals("BIG5")) return CodeConvert.toBig5String((String) po.values.elementAt(0));

			if (codePage.equals("UTF-8")) return CodeConvert.toUTF8String((String) po.values.elementAt(0));

			return (String) po.values.elementAt(0);
		}
		catch (Exception _ex) {
			return null;
		}
	}

	public String[] getParameterValues(String name) {
		try {
			ParameterObj po = (ParameterObj) parameters.get(name);
			if (po == null) return null;
			if (po.isFile) return null;
			if (po.values.size() == 0) return null;
			String strValues[] = (String[]) po.values.toArray(new String[po.values.size()]);
			if (strValues != null && strValues.length > 0) {
				for (int j = 0; j < strValues.length; j++) {
					if (codePage.equals("BIG5")) strValues[j] = CodeConvert.toBig5String(StringUtil.nullToSpace(strValues[j]));
					else if (codePage.equals("UTF-8")) strValues[j] = CodeConvert.toUTF8String(StringUtil.nullToSpace(strValues[j]));
					else strValues[j] = StringUtil.nullToSpace(strValues[j]);
				}
			}
			return strValues;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Enumeration<String> getParameterNames() {
		return parameters.keys();
	}

	protected ByteArrayOutputStream readFileData(MultipartInputStreamHandler in, String boundary, String fileContentType) throws IOException {
		OutputStream out = null;
		ByteArrayOutputStream baos;

		if (fileContentType.equals("application/x-macbinary")) {
			// mac-binary file type
			baos = new ByteArrayOutputStream(4096);
			out = new MacBinaryDecoderOutputStream(new BufferedOutputStream(baos));
		}
		else {
			baos = new ByteArrayOutputStream(16384);
			out = new BufferedOutputStream(baos);
		}

		byte bbuf[] = new byte[16384];
		boolean rnflag = false;
		int result;
		while ((result = in.readLine(bbuf, 0, bbuf.length)) != -1) {
			if (result > 2 && bbuf[0] == 45 && bbuf[1] == 45) {
				String line = new String(bbuf, 0, result, "ISO-8859-1");
				if (line.startsWith(boundary)) break;
			}

			if (rnflag) {
				out.write(13);
				out.write(10);
				rnflag = false;
			}

			if (result >= 2 && bbuf[result - 2] == 13 && bbuf[result - 1] == 10) {
				out.write(bbuf, 0, result - 2);
				rnflag = true;
			}
			else {
				out.write(bbuf, 0, result);
			}
		}
		out.flush();
		out.close();
		return baos;
	}

	public String readLine() throws IOException {
		return in.readLine();
	}

	protected boolean readNextPart(MultipartInputStreamHandler in, String boundary) throws IOException {
		// Create ParameterObj : define in this package
		ParameterObj po = new ParameterObj();
		// Read one line from ServletInputStream (the part second line)
		String line = in.readLine();
		if (line == null) return true;
		if (line.length() == 0) return true;
		// Get content-disposition Information
		String dispInfo[] = extractDispositionInfo(line);
		// Set parameter value into ParameterObj.
		po.name = dispInfo[0];
		po.filename = dispInfo[1];
		if (dispInfo[2].equals("true")) po.isFile = true;
		else po.isFile = false;
		// Read third line form ServletInputStream
		line = in.readLine();
		if (line == null) return true;
		po.fileContentType = extractContentType(line);
		if (po.fileContentType != null) {
			line = in.readLine();
			if (line == null || line.length() > 0) throw new IOException("Malformed line after content type: " + line);
		}
		if (po.filename == null || po.fileContentType == null) {
			// Read parameter
			String value = readParameter(in, boundary);
			if (value.equals("")) value = null;
			// po.value = value;
			if (parameters.get(po.name) == null) {
				po.addValue(value);
			}
			else {
				po = (ParameterObj) parameters.get(po.name);
				po.addValue(value);
			}
		}
		else {
			ByteArrayOutputStream baos = readFileData(in, boundary, po.fileContentType);
			po.value = baos;
		}
		parameters.put(po.name, po);
		return false;
	}

	protected String readParameter(MultipartInputStreamHandler in, String boundary) throws IOException {
		StringBuffer sbuf = new StringBuffer();
		String line;

		while ((line = in.readLine()) != null) {
			if (line.startsWith(boundary)) break;
			sbuf.append(line + "\r\n");
		}
		if (sbuf.length() == 0) {
			return null;
		}
		else {
			sbuf.setLength(sbuf.length() - 2);
			return sbuf.toString();
		}
	}

	protected boolean readRequest(int maxSize) throws IOException {
		int length = req.getContentLength();
		if (maxSize > 0 && length > maxSize) { throw new IOException("Content length over max size !"); }
		// Get Content Type
		String type = null;
		String type1 = req.getHeader("Content-Type");
		String type2 = req.getContentType();
		if (type1 == null && type2 != null) type = type2;
		else if (type2 == null && type1 != null) type = type1;
		else if (type1 != null && type2 != null) type = type1.length() <= type2.length() ? type2 : type1;
		// Check Content type => multupart/form-data
		if (type == null || !type.toLowerCase().startsWith("multipart/form-data")) throw new IOException("Posted content type isn't multipart/form-data");
		// Get boundary
		String boundary = extractBoundary(type);
		if (boundary == null) throw new IOException("Separation boundary was not specified");
		// Create MultipartInputStreamHandler
		in = new MultipartInputStreamHandler(req.getInputStream(), req.getContentLength());
		// Read first line
		String line = in.readLine();
		// Check Boundary
		if (line == null) { 
			//throw new IOException("Corrupt form data: premature ending");
			return true;
		}
		if (!line.startsWith(boundary)) throw new IOException("Corrupt form data: no leading boundary");
		// Read Next Part of Form Data
		for (boolean done = false; !done; done = readNextPart(in, boundary))
			;
		return true;
	}

	public boolean readRequestAndCheckFileSize() throws IOException {
		return readRequest(maxPostSize);
	}

	/**
	 * @return Returns the codePage.
	 */
	public String getCodePage() {
		return codePage;
	}

	/**
	 * @param codePage The codePage to set.
	 */
	public void setCodePage(String codePage) {
		this.codePage = codePage;
	}
}