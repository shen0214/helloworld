package com.tech.util;

import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CodeConvert {
	private static final Logger logger = LoggerFactory.getLogger(CodeConvert.class);
	private CodeConvert() {	}

	public static boolean isWindows() {
		return (System.getProperty("os.name").indexOf("Windows") != -1);
	}

	public static String toBig5String(boolean bChkOS, String uniStr) {
		if (bChkOS && !isWindows()) return uniStr;
		return toBig5String(uniStr);
	}

	public static String toBig5String(String uniStr) {
		try {
			if (uniStr == null || uniStr.length() == 0) return uniStr;
			return new String(uniStr.getBytes("8859_1"), "Big5");
		} 
		catch (UnsupportedEncodingException e) {
			logger.error( e.getMessage() );
			return uniStr;
		}
	}
	
	public static String toUTF8String(String uniStr) {
		try {
			if (uniStr == null || uniStr.length() == 0) return uniStr;
			return new String(uniStr.getBytes("8859_1"), "UTF8");
		} 
		catch (UnsupportedEncodingException e) {
			logger.error( e.getMessage() );
			return uniStr;
		}
	}
	
	public static String Big5toUTF8(String big5Str) {
		try {
			if (big5Str == null || big5Str.length() == 0) return big5Str;
			return new String(big5Str.getBytes("big5"), "UTF-8");
		} 
		catch (UnsupportedEncodingException e) {
			logger.error( e.getMessage() );
			return big5Str;
		}
	}

	public static String to8859String(boolean bChkOS, String strBig) {
		if (bChkOS && isWindows()) return strBig;
		return to8859String(strBig);
	}

	public static String to8859String(String strBig) {
		try {
			if (strBig == null || strBig.length() == 0) return strBig;
			return new String(strBig.getBytes("Big5"), "8859_1");
		} 
		catch (UnsupportedEncodingException e) {
			logger.error( e.getMessage() );
			return strBig;
		}
	}
}