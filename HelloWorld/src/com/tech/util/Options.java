package com.tech.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Options {
	private static final Logger logger = LoggerFactory.getLogger(Options.class);

	private Options() {
	}

	private static final String OPTION_FILE_NAME = "config/TechLibOptions";
	public static boolean isCompleted = false;
	public static String WEB_ROOT_DIR = "";
	public static String WEB_URL = "";
	public static String STORE_FILE_DIR = "";

	public static String PARAM_CODEPAGE = "NOT_CONVERT";

	public static String MULTIPART_CODEPAGE = "UTF-8";

	public static boolean useDatasource;

	public static String datasourceName;
	public static String driverClassName;
	public static String databaseURL;
	public static String databaseUser;
	public static String databasePassword;

	public static String SMTP = "smtp.host";
	public static int SMTP_PORT = 25;
	public static String DEFAULT_MAIL_FROM = "user@host";
	public static String DEFAULT_MAIL_FROM_NAME = "tech ";
	public static String USERNAME = "";
	public static String PASSWORD = "";
	public static String AUTH = "false";
	public static String DEFAULT_MIMETYPE = "text/html";
	public static String DEFAULT_CHARSET = "UTF-8";

	// Init Options
	static {
		try {
			isCompleted = false;

			ResourceBundle res = ResourceBundle.getBundle(OPTION_FILE_NAME);
			// DB Connection Options
			String strUseDatasource = res.getString("USE_DATASOURCE").trim();
			if (strUseDatasource.equals("true")) {
				useDatasource = true;
				datasourceName = res.getString("DATASOURCE_NAME").trim();
			} else {
				useDatasource = false;
				driverClassName = res.getString("DRIVER_CLASS_NAME").trim();
				databaseURL = res.getString("DATABASE_URL").trim();
				databaseUser = res.getString("DATABASE_USER").trim();
				databasePassword = res.getString("DATABASE_PASSWORD").trim();
			}
			// File Upload Options
			WEB_ROOT_DIR = res.getString("WEB_ROOT_DIR").trim();
			WEB_URL = res.getString("WEB_URL").trim();
			STORE_FILE_DIR = res.getString("STORE_FILE_DIR").trim();
			PARAM_CODEPAGE = res.getString("PARAMETER_CODEPAGE").trim();
			MULTIPART_CODEPAGE = res.getString("MULTIPART_CODEPAGE").trim();

			// mail options
			DEFAULT_MAIL_FROM = res.getString("DEFAULT_MAIL_FROM").trim();
			DEFAULT_MAIL_FROM_NAME = res.getString("DEFAULT_MAIL_FROM_NAME").trim();
			USERNAME = res.getString("USERNAME").trim();
			PASSWORD = res.getString("PASSWORD").trim();
			AUTH = res.getString("AUTH").trim();
			SMTP = res.getString("SMTP").trim();
			DEFAULT_MIMETYPE = res.getString("DEFAULT_MIMETYPE").trim();
			DEFAULT_CHARSET = res.getString("DEFAULT_CHARSET").trim();
			String temp = res.getString("SMTP_PORT").trim();
			if (temp.length() > 0)
				SMTP_PORT = Integer.parseInt(temp);

			isCompleted = true;
		} catch (MissingResourceException e) {
			logger.error(e.getMessage());
		} catch (NumberFormatException e) {
			logger.error(e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}