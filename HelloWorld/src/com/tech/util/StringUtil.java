package com.tech.util;

import java.io.File;
import java.text.NumberFormat;
import java.util.StringTokenizer;

public class StringUtil {
	public static String makeQueryStr(String kw) {
		if (kw.length() == 0) return "";
		String ret = kw;
		if (ret.length() > 0) {
			if (ret.charAt(0) == '?' || ret.charAt(0) == '*') ret = ret.substring(1);
		}
		ret = StringUtil.replaceStr(ret, "'", " ");
		ret = StringUtil.replaceStr(ret, "*", " ");
		ret = StringUtil.replaceStr(ret, "?", " ");
		ret = StringUtil.replaceStr(ret, "[", " ");
		ret = StringUtil.replaceStr(ret, "]", " ");
		if (ret.length() == 1) ret = ret + "*";
		return ret;
	}

	public static String getSubstring(String tag, int len) {
		return getSubstring( tag, len, "...");
	}
	
	public static String getSubstring(String tag, int len, String morestr ) {
		if (isEmpty(tag)) return "";
		if (tag.length() <= len) return tag;
		return tag.substring(0, len) + morestr;
	}

	static String _zero = "0000000000000000000000000000000000000000000000000000000000";

	public static String addZero(int num, int len) {
		return addZero(Integer.toString(num), len);
	}

	public static String addZero(String num, int len) {
		if (num.length() >= len) return num;
		return _zero.substring(0, len - num.length()) + num;
	}

	public static String formatNumber(int num) {
		return NumberFormat.getInstance().format(num);
	}
	
	public static String formatNumber(long num) {
		return NumberFormat.getInstance().format(num);
	}

	public static String formatNumber(String num) {
		if ( num.length() < 10 )
			return NumberFormat.getInstance().format(Integer.parseInt(num));
		return NumberFormat.getInstance().format(Long.parseLong(num));
	}

	public static String nullToSpace(String str1) {
		if (str1 == null) return "";
		else return str1;
	}

	public static String showContent(String cont, String splitStr) {
		if ( isEmpty(cont) ) return "";
		if ( cont.toLowerCase().indexOf("<br>") != -1 ) return cont;
		if ( cont.toLowerCase().indexOf("<p>") != -1 ) return cont;
		return StringUtil.replaceStr(StringUtil.replaceStr(StringUtil.replaceStr(
				cont.trim(), "\n", splitStr + "\n"), "\\" + "r", ""), "\\" + "\"", "\"");
	}

	public static String showContent(String cont) {
		return showContent( cont, "<p>");
	}
	
	public static String spaceToNBSP(String s_field) {
		if (isEmpty(s_field)) return "&nbsp;";
		return s_field;
	}

	public static String isSelected(String str1, String str2) {
		if (str1.equals(str2)) return " selected";
		return "";
	}
	
	public static String isChecked(String str1, String str2) {
		if (str1.equals(str2)) return " checked";
		return "";
	}

	public static String getNotEmpty(String str1, String str2) {
		if (isEmpty(str1)) return str2;
		return str1;
	}

	public static boolean isEmpty(String sobj) {
		if (sobj == null || sobj.length() == 0) return true;
		return false;
	}

	public static String replaceChar(String oldStr, char ch, String replaceStr) {
		if (isEmpty(oldStr)) return oldStr;
		if (isEmpty(replaceStr)) return oldStr;

		char b[] = new char[oldStr.length()];
		oldStr.getChars(0, oldStr.length(), b, 0);

		char b2[] = new char[replaceStr.length()];
		replaceStr.getChars(0, replaceStr.length(), b2, 0);

		char buffer[] = new char[0];

		boolean found = false;
		int offset = 0;
		for (int i = 0; i < b.length; i++) {
			if (b[i] == ch) {
				if (!found) found = true;
				int size = (i - offset);
				char bb[] = new char[buffer.length + size + b2.length];
				System.arraycopy(buffer, 0, bb, 0, buffer.length);
				System.arraycopy(b, offset, bb, buffer.length, size);
				System.arraycopy(b2, 0, bb, buffer.length + size, b2.length);
				buffer = bb;
				offset = i + 1;
			}
		}

		if (!found) return oldStr;

		if (offset < b.length) {
			int size = (b.length - offset);
			char bb[] = new char[buffer.length + size];
			System.arraycopy(buffer, 0, bb, 0, buffer.length);
			System.arraycopy(b, offset, bb, buffer.length, size);
			buffer = bb;
		}
		return new String(buffer);
	}

	public static String replaceStr(String oldStr, String str, String replaceStr) {
		if (isEmpty(oldStr)) return oldStr;
		if (isEmpty(str)) return oldStr;
		if (replaceStr == null) return oldStr;

		int fromIndex = 0, idx, len = str.length();
		StringBuffer sb = new StringBuffer();
		boolean found = false;
		while ((idx = oldStr.indexOf(str, fromIndex)) != -1) {
			if (!found) found = true;
			sb.append(oldStr.substring(fromIndex, idx));
			sb.append(replaceStr);
			fromIndex = idx + len;
		}
		if (!found) return oldStr;

		if (fromIndex < oldStr.length()) {
			sb.append(oldStr.substring(fromIndex));
		}

		return sb.toString();
	}

	public static void makeDirs(String path) {
		File dir = new File(path);
		if (dir.exists()) {
			return;
		}
		int len = path.length();

		for (int next = 1; next < len && (next = path.indexOf('/', next)) != -1; next++) {
			String subPath = path.substring(0, next);
			File subDir = new File(subPath);
			if (!subDir.exists()) {
				subDir.mkdir();
			}
		}
	}
	
	
	public static String[] split(String source, String delim) {
    String[] wordLists;
    if (source == null) {
      wordLists = new String[1];
      wordLists[0] = source;
      return wordLists;
    }
    if (delim == null) {
      delim = ",";
    }
    StringTokenizer st = new StringTokenizer(source, delim);
    int total = st.countTokens();
    wordLists = new String[total];
    for (int i = 0; i < total; i++) {
      wordLists[i] = st.nextToken();
    }
    return wordLists;
  }

  public static String[] split(String source, char delim) {
    return split(source, String.valueOf(delim));
  }

  public static String[] split(String source) {
    return split(source, ",");
  }
  
  public static String subString(String source, int maxlength) {
		String result = "";
		if (source == null) {
			return result;
		}
		else {
			if (source.length() < maxlength)
				return source;
			else
				return source.substring(0,maxlength-1) + "...";
		}
	}
}