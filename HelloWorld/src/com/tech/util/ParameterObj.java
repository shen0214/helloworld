package com.tech.util;

import java.util.Vector;

class ParameterObj {
	public String name;
	public String filename;
	public String fileContentType;
	public Object value;
	public boolean isFile;

	ParameterObj() {
		name = null;
		filename = null;
		fileContentType = null;
		value = null;
		isFile = false;
	}

	Vector<Object> values = new Vector<Object>();

	public void addValue(Object objValue) {
		values.addElement(objValue);
	}
}