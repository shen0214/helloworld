package com.tech.util;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

class MacBinaryDecoderOutputStream extends FilterOutputStream {

	int bytesFiltered;
	int dataForkLength;

	public MacBinaryDecoderOutputStream(OutputStream out) {
		super(out);
		bytesFiltered = 0;
		dataForkLength = 0;
	}

	public void write(int b) throws IOException {
		if (bytesFiltered >= 83 && bytesFiltered <= 86) {
			int leftShift = (86 - bytesFiltered) * 8;
			dataForkLength = dataForkLength | (b & 0xff) << leftShift;
		}
		else {
			if (bytesFiltered < 128 + dataForkLength && bytesFiltered >= 128)
					super.out.write(b);
		}
		bytesFiltered++;
	}

	public void write(byte b[]) throws IOException {
		write(b, 0, b.length);
	}

	public void write(byte b[], int off, int len) throws IOException {
		if (bytesFiltered >= 128 + dataForkLength) bytesFiltered += len;
		else if (bytesFiltered >= 128
				&& bytesFiltered + len <= 128 + dataForkLength) {
			super.out.write(b, off, len);
			bytesFiltered += len;
		}
		else {
			for (int i = 0; i < len; i++)
				write(b[off + i]);
		}
	}
}