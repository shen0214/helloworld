package com.shen.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "category")
public class Category implements Serializable {
	private static final long serialVersionUID = 6407870291348276313L;

	long categoryId;
	// private long channelId;
	private long upperCategoryId;
	private String code;
	public String name;
	int weight;
	int draft;
	private int canDisplay;
	private long channelId;
	private int defaultWeight;
	private int unEditGoods;
	private int imageWidth;
	private int imageHeight;
	private int articleImageWidth;
	int articleImageHeight;
	String metaDesc;
	int top_image_id;
	int topImageWeight;
	int topArticleId;
	String externalLink;
	int externalMore;
	private int subCategoryNumber;
	private int onlyMobile;

	private Channel channel;
	private Set<GoodsMain> goods;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "channel_id", insertable = false, updatable = false)
	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

//	@ManyToMany(fetch = FetchType.EAGER)
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "category_goods", joinColumns = @JoinColumn(name = "category_id"), inverseJoinColumns = @JoinColumn(name = "goods_id"))
	public Set<GoodsMain> getGoods() {
		return goods;
	}

	public void setGoods(Set<GoodsMain> goods) {
		this.goods = goods;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "category_id", unique = true, nullable = false, length = 8)
	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	@Column(name = "upper_category_id", nullable = false, length = 8)
	public long getUpperCategoryId() {
		return upperCategoryId;
	}

	public void setUpperCategoryId(long upperCategoryId) {
		this.upperCategoryId = upperCategoryId;
	}

	@Column(name = "code", nullable = false, length = 20)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "name", nullable = false, length = 50)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "weight", length = 4)
	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	@Column(name = "draft", length = 1)
	public int getDraft() {
		return draft;
	}

	public void setDraft(int draft) {
		this.draft = draft;
	}

	@Column(name = "can_display", length = 1)
	public int getCanDisplay() {
		return canDisplay;
	}

	public void setCanDisplay(int canDisplay) {
		this.canDisplay = canDisplay;
	}

	@Column(name = "channel_id", length = 8)
	public long getChannelId() {
		return channelId;
	}

	public void setChannelId(long channelId) {
		this.channelId = channelId;
	}

	@Column(name = "default_weight", length = 4)
	public int getDefaultWeight() {
		return defaultWeight;
	}

	public void setDefaultWeight(int defaultWeight) {
		this.defaultWeight = defaultWeight;
	}

	@Column(name = "un_edit_goods", length = 10)
	public int getUnEditGoods() {
		return unEditGoods;
	}

	public void setUnEditGoods(int unEditGoods) {
		this.unEditGoods = unEditGoods;
	}

	@Column(name = "image_width", length = 4)
	public int getImageWidth() {
		return imageWidth;
	}

	public void setImageWidth(int imageWidth) {
		this.imageWidth = imageWidth;
	}

	@Column(name = "image_height", length = 4)
	public int getImageHeight() {
		return imageHeight;
	}

	public void setImageHeight(int imageHeight) {
		this.imageHeight = imageHeight;
	}

	@Column(name = "article_image_width", length = 4)
	public int getArticleImageWidth() {
		return articleImageWidth;
	}

	public void setArticleImageWidth(int articleImageWidth) {
		this.articleImageWidth = articleImageWidth;
	}

	@Column(name = "article_image_height", length = 4)
	public int getArticleImageHeight() {
		return articleImageHeight;
	}

	public void setArticleImageHeight(int articleImageHeight) {
		this.articleImageHeight = articleImageHeight;
	}

	@Column(name = "meta_desc", length = 300)
	public String getMetaDesc() {
		return metaDesc;
	}

	public void setMetaDesc(String metaDesc) {
		this.metaDesc = metaDesc;
	}

	@Column(name = "top_image_id", length = 20)
	public int getTop_image_id() {
		return top_image_id;
	}

	public void setTop_image_id(int top_image_id) {
		this.top_image_id = top_image_id;
	}

	@Column(name = "top_image_weight", length = 8)
	public int getTopImageWeight() {
		return topImageWeight;
	}

	public void setTopImageWeight(int topImageWeight) {
		this.topImageWeight = topImageWeight;
	}

	@Column(name = "top_article_id", length = 20)
	public int getTopArticleId() {
		return topArticleId;
	}

	public void setTopArticleId(int topArticleId) {
		this.topArticleId = topArticleId;
	}

	@Column(name = "external_link", length = 200)
	public String getExternalLink() {
		return externalLink;
	}

	public void setExternalLink(String externalLink) {
		this.externalLink = externalLink;
	}

	@Column(name = "external_more", length = 1)
	public int getExternalMore() {
		return externalMore;
	}

	public void setExternalMore(int externalMore) {
		this.externalMore = externalMore;
	}

	@Column(name = "sub_category_number", length = 4)
	public int getSubCategoryNumber() {
		return subCategoryNumber;
	}

	public void setSubCategoryNumber(int subCategoryNumber) {
		this.subCategoryNumber = subCategoryNumber;
	}

	@Column(name = "only_mobile", length = 1)
	public int getOnlyMobile() {
		return onlyMobile;
	}

	public void setOnlyMobile(int onlyMobile) {
		this.onlyMobile = onlyMobile;
	}
}
