package com.shen.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "goods_main")
public class GoodsMain implements Serializable {
	private static final long serialVersionUID = 8408599195862620852L;

	long goodsId;
	long channelId;
	long upperCategoryId;
	long goodsType;
	long goodsTag;
	long areaCode;
	int price; // 費用
	String onPayUrl; // 線上付款
	String headline; // 標題
	String subHeadline; // 副標題
	String summary; // 摘要
	String sourceName;
	String organizer;
	String coOrganizer;
	String sponsor;
	Date startTime;
	Date endTime;
	Date closeTime;
	int pageview; /* remove */
	int fbShare; /* remove */
	Date insertTime;

	GoodsInfo goodsInfo;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "goods_id", referencedColumnName = "goods_id")
	public GoodsInfo getGoodsInfo() {
		return goodsInfo;
	}

	public void setGoodsInfo(GoodsInfo goodsInfo) {
		this.goodsInfo = goodsInfo;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "goods_id", nullable = false, length = 8)
	public long getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(long goodsId) {
		this.goodsId = goodsId;
	}

	@Column(name = "channel_id", nullable = false, length = 8)
	public long getChannelId() {
		return channelId;
	}

	public void setChannelId(long channelId) {
		this.channelId = channelId;
	}

	@Column(name = "upper_category_id", length = 8)
	public long getUpperCategoryId() {
		return upperCategoryId;
	}

	public void setUpperCategoryId(long upperCategoryId) {
		this.upperCategoryId = upperCategoryId;
	}

	@Column(name = "goods_type", nullable = false, length = 20)
	public long getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(long goodsType) {
		this.goodsType = goodsType;
	}

	@Column(name = "goods_tag", nullable = false, length = 20)
	public long getGoodsTag() {
		return goodsTag;
	}

	public void setGoodsTag(long goodsTag) {
		this.goodsTag = goodsTag;
	}

	@Column(name = "area_code", length = 20)
	public long getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(long areaCode) {
		this.areaCode = areaCode;
	}

	@Column(name = "price", length = 8)
	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Column(name = "on_pay_url", length = 255)
	public String getOnPayUrl() {
		return onPayUrl;
	}

	public void setOnPayUrl(String onPayUrl) {
		this.onPayUrl = onPayUrl;
	}

	@Column(name = "headline", nullable = false, length = 500)
	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	@Column(name = "sub_headline", length = 500)
	public String getSubHeadline() {
		return subHeadline;
	}

	public void setSubHeadline(String subHeadline) {
		this.subHeadline = subHeadline;
	}

	@Column(name = "summary", length = 500)
	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	@Column(name = "source_name", length = 500)
	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	@Column(name = "organizer", length = 500)
	public String getOrganizer() {
		return organizer;
	}

	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}

	@Column(name = "co_organizer", length = 500)
	public String getCoOrganizer() {
		return coOrganizer;
	}

	public void setCoOrganizer(String coOrganizer) {
		this.coOrganizer = coOrganizer;
	}

	@Column(name = "sponsor", length = 500)
	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	@Column(name = "start_time")
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	@Column(name = "end_time")
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@Column(name = "close_time")
	public Date getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}

	@Column(name = "pageview", nullable = false, length = 10)
	public int getPageview() {
		return pageview;
	}

	public void setPageview(int pageview) {
		this.pageview = pageview;
	}

	@Column(name = "fb_share", nullable = false, length = 100)
	public int getFbShare() {
		return fbShare;
	}

	public void setFbShare(int fbShare) {
		this.fbShare = fbShare;
	}

	@Column(name = "insert_time")
	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

}
