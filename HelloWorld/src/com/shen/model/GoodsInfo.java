package com.shen.model;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "goods_info")
public class GoodsInfo {

	long channelId;
	long goodsId;
	String reporter;
	String headline;
	String sourceName;
	long creator;
	Date modifiedTime;
	long modifiedBy;
	long providerId;
	int pictureNumber;
	int videoNumber;
	String eventGPS;
	String senderGPS;
	long country;
	String countryName;
	String countryFlag;
	String placeName;
	String rid;
	long topImageId;
	int topImageWeight;
	long topVideoId;
	int topVideoWeight;
	Date insertTime;
	int areaCode;

	/*@ManyToOne
    @JoinColumn(name = "channel_id",insertable = false, updatable = false)
	Channel channel;
	public Channel getChannel() {
		return channel;
	}
	
	public void setChannel(Channel channel) {
		this.channel = channel;
	}
	
	@OneToOne
	@JoinColumn(name = "goods_id", insertable = false, updatable = false)
	GoodsMain goodMain;
	public GoodsMain getGoodMain() {
		return goodMain;
	}
	
	public void setGoodMain(GoodsMain goodMain) {
		this.goodMain = goodMain;
	}
	*/


	@Id
	@Column(name = "goods_id", nullable = false, length = 8)
	public long getGoodsId() {
		return goodsId;
	}
	
	public void setGoodsId(long goodsId) {
		this.goodsId = goodsId;
	}
	
	@Column(name = "channel_id", nullable = false, length = 8)
	public long getChannelId() {
		return channelId;
	}

	public void setChannelId(long channelId) {
		this.channelId = channelId;
	}

	@Column(name = "reporter", length = 200)
	public String getReporter() {
		return reporter;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	@Column(name = "headline", nullable = false, length = 500)
	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	@Column(name = "source_name", length = 50)
	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	@Column(name = "creator", length = 8)
	public long getCreator() {
		return creator;
	}

	public void setCreator(long creator) {
		this.creator = creator;
	}

	@Column(name = "modified_time")
	public Date getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(Date modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	@Column(name = "modified_by", length = 8)
	public long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "provider_id", length = 8)
	public long getProviderId() {
		return providerId;
	}

	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}

	@Column(name = "pic_num", length = 2)
	public int getPictureNumber() {
		return pictureNumber;
	}

	public void setPictureNumber(int pictureNumber) {
		this.pictureNumber = pictureNumber;
	}

	@Column(name = "video_num", length = 2)
	public int getVideoNumber() {
		return videoNumber;
	}

	public void setVideoNumber(int videoNumber) {
		this.videoNumber = videoNumber;
	}

	@Column(name = "event_gps", length = 30)
	public String getEventGPS() {
		return eventGPS;
	}

	public void setEventGPS(String eventGPS) {
		this.eventGPS = eventGPS;
	}

	@Column(name = "sender_gps", length = 30)
	public String getSenderGPS() {
		return senderGPS;
	}

	public void setSenderGPS(String senderGPS) {
		this.senderGPS = senderGPS;
	}

	@Column(name = "country", length = 8)
	public long getCountry() {
		return country;
	}

	public void setCountry(long country) {
		this.country = country;
	}

	@Column(name = "country_name", length = 20)
	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	@Column(name = "country_flag", length = 100)
	public String getCountryFlag() {
		return countryFlag;
	}

	public void setCountryFlag(String countryFlag) {
		this.countryFlag = countryFlag;
	}

	@Column(name = "place_name", length = 30)
	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	@Column(name = "rid", length = 128)
	public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	@Column(name = "top_image_id", length = 20)
	public long getTopImageId() {
		return topImageId;
	}

	public void setTopImageId(long topImageId) {
		this.topImageId = topImageId;
	}

	@Column(name = "top_image_weight", length = 4)
	public int getTopImageWeight() {
		return topImageWeight;
	}

	public void setTopImageWeight(int topImageWeight) {
		this.topImageWeight = topImageWeight;
	}

	@Column(name = "top_video_id", length = 20)
	public long getTopVideoId() {
		return topVideoId;
	}

	public void setTopVideoId(long topVideoId) {
		this.topVideoId = topVideoId;
	}

	@Column(name = "top_video_weight", length = 4)
	public int getTopVideoWeight() {
		return topVideoWeight;
	}

	public void setTopVideoWeight(int topVideoWeight) {
		this.topVideoWeight = topVideoWeight;
	}

	@Column(name = "insert_time", length = 4)
	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	@Column(name = "area_code", length = 20)
	public int getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(int areaCode) {
		this.areaCode = areaCode;
	}
	
	long goodsType;
	long goodsTag;
	int price;				// 費用
	String onPayUrl;			// 線上付款
	Date endTime;			// 訖(結束)時間
	Date startTime;			// 起(開始)時間
	Date closeTime;			// 報名截止時間
	String subHeadline;		// 副標題	
	String summary;			// 摘要
	String organizer;
	String coOrganizer;
	String sponsor;


	@Transient
	public long getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(long goodsType) {
		this.goodsType = goodsType;
	}

	@Transient
	public long getGoodsTag() {
		return goodsTag;
	}

	public void setGoodsTag(long goodsTag) {
		this.goodsTag = goodsTag;
	}

	@Transient
	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Transient
	public String getOnPayUrl() {
		return onPayUrl;
	}

	public void setOnPayUrl(String onPayUrl) {
		this.onPayUrl = onPayUrl;
	}

	@Transient
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@Transient
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	@Transient
	public Date getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(Date closeTime) {
		this.closeTime = closeTime;
	}

	@Transient
	public String getSubHeadline() {
		return subHeadline;
	}

	public void setSubHeadline(String subHeadline) {
		this.subHeadline = subHeadline;
	}

	@Transient
	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	@Transient
	public String getOrganizer() {
		return organizer;
	}

	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}

	@Transient
	public String getCoOrganizer() {
		return coOrganizer;
	}

	public void setCoOrganizer(String coOrganizer) {
		this.coOrganizer = coOrganizer;
	}

	@Transient
	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	
}
