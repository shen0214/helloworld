package com.shen.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "category_goods")
public class GoodsCategory implements Serializable {
	private static final long serialVersionUID = 2375186384239183780L;
	long channelId;
	long categoryId;
	long goodsId;
	int status;
	int weight;

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof GoodsCategory))
			return false;
		GoodsCategory paramObj = (GoodsCategory) object;
		return new EqualsBuilder().appendSuper(super.equals(object)).append(this.categoryId, paramObj.categoryId)
				.append(this.goodsId, paramObj.goodsId).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).appendSuper(super.hashCode()).append(this.categoryId).append(this.goodsId)
				.toHashCode();
	}

	@Column(name = "channel_id", nullable = false, length = 8)
	public long getChannelId() {
		return channelId;
	}

	public void setChannelId(long channelId) {
		this.channelId = channelId;
	}

	@Column(name = "category_id", nullable = false, length = 8)
	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	@Id
	@Column(name = "goods_id", nullable = false, length = 8)
	public long getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(long goodsId) {
		this.goodsId = goodsId;
	}

	@Column(name = "status", length = 1)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "weight", length = 4)
	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
}
