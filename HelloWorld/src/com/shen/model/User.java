package com.shen.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "user")
public class User implements Serializable {
	private static final long serialVersionUID = -6413475973664618153L;
	private long userId;
	private String email;
	private String name;
	private long deptId;
	private int categoryLimit;
	private int enabled;
	private Date passwordChangeTime;
	private int myGoodsStatus;
	private int myDurationStatus;
	private long myDefaultChannel;
	private String myDefaultOrder;

	private String password;
	private String passwordConfirm;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id", unique = true, nullable = false, length = 8)
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "name", unique = true, nullable = false, length = 8)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "dept_id", nullable = false, length = 4)
	public long getDeptId() {
		return deptId;
	}

	public void setDeptId(long deptId) {
		this.deptId = deptId;
	}

	@Column(name = "category_limit", length = 1)
	public int getCategoryLimit() {
		return categoryLimit;
	}

	public void setCategoryLimit(int categoryLimit) {
		this.categoryLimit = categoryLimit;
	}

	@Column(name = "enabled", nullable = false, length = 1)
	public int getEnabled() {
		return enabled;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}

	@Column(name = "pwd_change_time")
	public Date getPasswordChangeTime() {
		return passwordChangeTime;
	}

	public void setPasswordChangeTime(Date passwordChangeTime) {
		this.passwordChangeTime = passwordChangeTime;
	}

	@Column(name = "my_goods_status", length = 1)
	public int getMyGoodsStatus() {
		return myGoodsStatus;
	}

	public void setMyGoodsStatus(int myGoodsStatus) {
		this.myGoodsStatus = myGoodsStatus;
	}

	@Column(name = "my_duration_status", length = 1)
	public int getMyDurationStatus() {
		return myDurationStatus;
	}

	public void setMyDurationStatus(int myDurationStatus) {
		this.myDurationStatus = myDurationStatus;
	}

	@Column(name = "my_default_channel", length = 8)
	public long getMyDefaultChannel() {
		return myDefaultChannel;
	}

	public void setMyDefaultChannel(long myDefaultChannel) {
		this.myDefaultChannel = myDefaultChannel;
	}

	@Column(name = "my_default_order", length = 20)
	public String getMyDefaultOrder() {
		return myDefaultOrder;
	}

	public void setMyDefaultOrder(String myDefaultOrder) {
		this.myDefaultOrder = myDefaultOrder;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Transient
	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	private Set<Role> roles;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	
//	private Set<Category> category;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "channel_user", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "channel_id"))
	
	

	private Set<Channel> channels;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "channel_user", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "channel_id"))
	public Set<Channel> getChannels() {
		return channels;
	}

	public void setChannels(Set<Channel> channels) {
		this.channels = channels;
	}

	private Dept dept;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "dept_id", insertable = false, updatable = false)
	public Dept getDept() {
		return dept;
	}

	public void setDept(Dept dept) {
		this.dept = dept;
	}

}
