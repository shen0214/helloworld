package com.shen.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "channel")
public class Channel implements Serializable {
	private static final long serialVersionUID = 8641858220349922870L;

	private long channelId;
	private String code;
	private String name;
	private int weight;
	private String reminder;
	private int unEditGoods;
	private int imageChoose;
	private String previewURL;
	private String goodsURL;
	private int toEPaper;
	private int rssShow;
	private int isAlbum;
	
	private Set<User> users;
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "channels") // roles 要和 Muser 的 getRoles 和 setRoles 中的 Roles 一致
	public Set<User> getUsers() {
		return users;
	}
	
	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
	private Set<Category> category;


	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	@JoinColumn(name = "channel_id", insertable = false, nullable = false, updatable = false)
	public Set<Category> getCategory() {
		return category;
	}

	public void setCategory(Set<Category> category) {
		this.category = category;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "channel_id", unique = true, nullable = false, length = 8)
	public long getChannelId() {
		return channelId;
	}

	public void setChannelId(long channelId) {
		this.channelId = channelId;
	}

	@Column(name = "code", nullable = false, length = 20)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "name", nullable = false, length = 30)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getReminder() {
		return reminder;
	}

	public void setReminder(String reminder) {
		this.reminder = reminder;
	}

	@Column(name = "un_edit_goods", length = 10)
	public int getUnEditGoods() {
		return unEditGoods;
	}

	public void setUnEditGoods(int unEditGoods) {
		this.unEditGoods = unEditGoods;
	}

	@Column(name = "image_choose", length = 1)
	public int getImageChoose() {
		return imageChoose;
	}

	public void setImageChoose(int imageChoose) {
		this.imageChoose = imageChoose;
	}

	@Column(name = "preview_url", length = 200)
	public String getPreviewURL() {
		return previewURL;
	}

	public void setPreviewURL(String previewURL) {
		this.previewURL = previewURL;
	}

	@Column(name = "goods_url", length = 200)
	public String getGoodsURL() {
		return goodsURL;
	}

	public void setGoodsURL(String goodsURL) {
		this.goodsURL = goodsURL;
	}

	@Column(name = "to_epaper", length = 1)
	public int getToEPaper() {
		return toEPaper;
	}

	public void setToEPaper(int toEPaper) {
		this.toEPaper = toEPaper;
	}

	@Column(name = "rss_show", length = 1)
	public int getRssShow() {
		return rssShow;
	}

	public void setRssShow(int rssShow) {
		this.rssShow = rssShow;
	}

	@Column(name = "is_album", length = 1)
	public int getIsAlbum() {
		return isAlbum;
	}

	public void setIsAlbum(int isAlbum) {
		this.isAlbum = isAlbum;
	}

	int authorId;

	int userId;

	int adId;

	@Transient
	public int getAuthorId() {
		return authorId;
	}

	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}

	@Transient
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Transient
	public int getAdId() {
		return adId;
	}

	public void setAdId(int adId) {
		this.adId = adId;
	}

}
