package com.shen.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shen.model.GoodsCategory;
import com.shen.repository.GoodsCategoryRepository;

@Service
public class GoodsCategoryServiceImple extends GenericServiceImpl<GoodsCategory, GoodsCategory> {

	@Autowired
	GoodsCategoryRepository goodsCategoryRepository;
	
	public GoodsCategory findById(long goodsId, long categoryId) {
		return  goodsCategoryRepository.findById(goodsId, categoryId);
	}

}
