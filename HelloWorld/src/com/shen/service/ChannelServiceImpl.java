package com.shen.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shen.model.Channel;
import com.shen.repository.ChannelRepository;

@Service
public class ChannelServiceImpl extends GenericServiceImpl<Channel, Long>{
	
	@Autowired
	ChannelRepository channelRepository;
	
	public Channel findById(long id) {
		return channelRepository.findById(id).isPresent()?channelRepository.findById(id).get():null;
	}

}
