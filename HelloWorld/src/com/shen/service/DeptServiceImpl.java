package com.shen.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.shen.model.Dept;
import com.shen.reference.CodeName;
import com.shen.repository.DeptRepository;

@Service
public class DeptServiceImpl {
	@Autowired
	DeptRepository deptRepository;

	public List<CodeName> getDeptList() {
		List<CodeName> list = new ArrayList<CodeName>();
		Sort sort= new Sort(Sort.Direction.DESC,"deptId");
		List<Dept> depts = deptRepository.findAll(sort);
		if ( depts == null ) return list;
		for( Dept dept : depts) {
			list.add( new CodeName(Long.toString(dept.getDeptId()), dept.getName()));
		}
		return list;
	}
	
	
	public List<Dept> findAll(PageRequest pageable){
		List<Dept> deptList =  deptRepository.findAllForPageable(pageable);
		return deptList; 
	}
}
