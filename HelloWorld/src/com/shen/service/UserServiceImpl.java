package com.shen.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.shen.model.User;
import com.shen.repository.UserRepository;

@Service
public class UserServiceImpl extends GenericServiceImpl<User, Long> {
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	@Autowired
	UserRepository userRepository;


    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public void save(User user) {
    	String passwd = user.getPassword();
    	if (passwd==null || passwd.equals("******************"))  {
    		
    	}
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

    public User findByUserId(long userId) {
		return userRepository.findByUserId(userId);
	}
    
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}
	
	public void delete(String email) {
		User user = userRepository.findByEmail(email);
		if (user!=null ) userRepository.delete(user);
	}

	public List<User> findAll(){
		List<User> users = userRepository.findAll();
		return users; 
	}
}
