package com.shen.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shen.model.Category;
import com.shen.repository.CategoryRepository;

@Service
public class CategoryServiceImpl extends GenericServiceImpl<Category, Long> {
	@Autowired
	CategoryRepository categoryRepository;

	public Category findById(long id) {
		return categoryRepository.findById(id).isPresent() ? categoryRepository.findById(id).get() : null;
	}
}