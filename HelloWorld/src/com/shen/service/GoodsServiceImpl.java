package com.shen.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shen.model.GoodsMain;
import com.shen.repository.GoodsRepository;

@Service
public class GoodsServiceImpl extends GenericServiceImpl<GoodsMain, Long> {
	
	@Autowired
	GoodsRepository goodsRepository;
	
	public GoodsMain findById(long id) {
		return goodsRepository.findById(id).isPresent() ? goodsRepository.findById(id).get(): null;
	}

}
