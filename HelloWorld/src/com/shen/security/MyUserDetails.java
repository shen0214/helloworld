package com.shen.security;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.shen.model.Role;
import com.shen.model.User;
import com.tech.util.Today;
@Entity
public class MyUserDetails implements UserDetails {
	private static final long serialVersionUID = -2586281694268499892L;

	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(MyUserDetails.class);

	MessageSource messageSource;

	User user;
	Set<Role> roles;

	public MyUserDetails() {
		user = new User();
		roles = new HashSet<Role>();
	}

	boolean accountNonExpired = true; /* 帳號沒過期：true 過期：false */
	boolean accountNonLocked = true; /* 帳號是不被鎖定 */

	public Set<Role> getRoles() {
		return roles;
	}

	public void addRole(Role role) {
		this.roles.add(role);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		for (Role role : user.getRoles()) {
			grantedAuthorities.add(new SimpleGrantedAuthority(role.getRoleName()));
		}
		return grantedAuthorities;
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		return user.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	/*
	 * 密碼有效期 : 未過期或未開戶 true 過期 false
	 */
	@Override
	public boolean isCredentialsNonExpired() {
		if (user.getPasswordChangeTime() == null)
			return true;
		if (user.getPasswordChangeTime().compareTo(Today.addDay(-30)) < 0)
			return true;
		return false;
	}

	/*
	 * 帳號被禁用 false
	 */
	@Override
	public boolean isEnabled() {
		return (user.getEnabled() == 1);
	}

}
