package com.shen.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.shen.util.MessageUtils;
import com.tech.util.Param;
import com.tech.util.StringUtil;

public class ExtUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	private static final Logger logger = LoggerFactory.getLogger(ExtUsernamePasswordAuthenticationFilter.class);

	@Value("${web.functionHandler}")
	private String functionHandler;

	@Value("${web.keyCodeHandler}")
	private String keyCodeHandler;
	
	@Value("${captcha.key}")
	private String captchaKey;

	@Autowired
	MessageSource messageSource;

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		logger.debug(String.format("==============  attemptAuthentication(HttpServletReques  ==============="));

		Param param = new Param(request);
		String keyCode = param.get(keyCodeHandler);
		String goFunction = param.get(functionHandler);
		HttpSession session = request.getSession(true);

		final String authKey = param.getAttribute(session, captchaKey);
		if (!keyCode.equals(authKey)) {
			logger.debug(String.format("==============  attemptAuthentication(HttpServletReques  ==============="));
			throw new LoginAuthenticationException(MessageUtils.get(messageSource, "captcha.not.match"));
		}
		if (StringUtil.isEmpty(goFunction))
			logger.debug(String.format("==============  attemptAuthentication(HttpServletReques  ==============="));
			goFunction = "/";
		session.setAttribute(functionHandler, goFunction);

		return super.attemptAuthentication(request, response);

	}

}