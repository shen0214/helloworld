package com.shen.security;

import org.springframework.security.core.AuthenticationException;

public class LoginAuthenticationException extends AuthenticationException {
	private static final long serialVersionUID = 2837727238396662103L;

	public LoginAuthenticationException(final String message) {
		super(message);
	}

	public LoginAuthenticationException(final String message, final Throwable cause) {
		super(message, cause);
	}
}