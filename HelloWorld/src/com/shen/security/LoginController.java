package com.shen.security;

import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.shen.model.User;
import com.shen.service.UserServiceImpl;
import com.shen.util.KeyImage;
import com.shen.validator.UserValidator1;

@Controller
public class LoginController {
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private UserServiceImpl userServiceImpl;

	@Autowired
	private SecurityService securityService;

	@Autowired
	private UserValidator1 userValidator1;

	/* 驗證碼的 key */
	@Value("${captcha.key}")
	private String captchaKey;

	/* 驗證碼的圖寬 */
	@Value("${captcha.picWidth}")
	private int captchaPicWidth;

	/* 驗證碼的圖高 */
	@Value("${captcha.picHeight}")
	private int captchaPicHeight;

	/* 驗證碼字的大小 */
	@Value("${captcha.characterSize}")
	private int captchaCharacterSize;

	/* 驗證碼的字長度 */
	@Value("${captcha.characterLength}")
	private int captchaCharacterLength;

	/* 驗證碼的圖背景色 */
	@Value("${captcha.bgColor}")
	private int captchaBgColor;

	/* 取得驗證碼 */
	@RequestMapping(value = "/keyimg", method = RequestMethod.GET)
	public void KeyImag(HttpServletRequest request, HttpServletResponse response) {
		KeyImage keyImage = new KeyImage();
		/* 取得驗證碼的圖 */
		BufferedImage image = keyImage.getImage(captchaPicWidth, captchaPicHeight, captchaBgColor, captchaCharacterSize,
				captchaCharacterLength);
		/* 驗證碼的數字 */
		String randString = keyImage.getKey();
		HttpSession session = request.getSession(true);
		session.setAttribute(captchaKey, randString);
		try {
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			ImageIO.write(image, "JPEG", response.getOutputStream());
			response.flushBuffer();
		} catch (Throwable e) {
			logger.error(e.getMessage());
		}
	}

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String registration(Model model) {
		logger.debug(String.format("====================  registration  ======================"));
		model.addAttribute("userForm", new User());

		return "registration";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
		logger.debug(String.format("====================  registration  ======================"));
		userValidator1.validate(userForm, bindingResult);
		if (bindingResult.hasErrors()) {
			return "";
		}

		userServiceImpl.save(userForm);

		securityService.autologin(userForm.getName(), userForm.getPasswordConfirm());

		return "redirect:/welcome";
	}

	@RequestMapping(value = "/login")
	public String login(Model model, String error, String logout) {
		return "login";
	}

	@RequestMapping(value = { "/", "/welcome" }, method = RequestMethod.GET)
	public String welcome(Model model) {
		logger.debug(String.format("=====================  welcome  ======================"));
		return "welcome";
	}
}
