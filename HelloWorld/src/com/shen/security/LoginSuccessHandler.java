package com.shen.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import com.tech.util.StringUtil;

public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
	private static final Logger logger = LoggerFactory.getLogger(LoginSuccessHandler.class);
	
	@Value("${web.functionHandler}")
	private String functionHandler;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
		logger.debug(String.format( "==========================  onAuthenticationSuccess  ===========================") );
		/* 驗證成功 在正式登入時,在這加記 登入資訊的 login log. */
		
		/* 
		 * 登入成功後要導入的首頁
		 * 如果要取得 user 的預設頁面，可加入以下的 code 
		 *  MyUserDetails myUserDetals = (MyUserDetails) authentication.getPrincipal();
		*/
		
		String goFunction = (String)request.getSession().getAttribute(functionHandler);
		if ( StringUtil.isEmpty(goFunction)!= true ) {
			goFunction = "/security/user/insert" + goFunction;
			getRedirectStrategy().sendRedirect(request, response, goFunction);
		} else {
			super.onAuthenticationSuccess(request, response, authentication);
		}
	}
}
