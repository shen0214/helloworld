package com.shen.security;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shen.model.User;
import com.shen.repository.UserRepository;
import com.shen.util.MessageUtils;
public class UserDetailsServiceImpl implements UserDetailsService {
	private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	MessageSource messageSource;
	
	
	public UserDetailsServiceImpl() {}  

	@Transactional
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
		logger.debug(String.format( "================== loadUserByUsername  ==================="));

		MyUserDetails myUserDetail = new MyUserDetails();
		User user = userRepository.findByEmail(username);
		try {
			Validate.notNull(user, MessageUtils.get(messageSource, "login.fail.account.not.exist"));  // user 是null 抛出 Exception
			if (user.getEnabled() != 1)				//　權限是否起動
				throw new LoginAuthenticationException(MessageUtils.get(messageSource, "login.fail.not.enabled"));
			
			Validate.notEmpty(user.getRoles(), MessageUtils.get(messageSource, "login.fail.role.not.set"));  // user.roles 為空值時，抛出 Exception

			myUserDetail.setUser(user);
		} catch (Exception e) {
			throw new LoginAuthenticationException(e.getMessage());
		}
		
		return myUserDetail;
	}
}
