package com.shen.util;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;

public class MessageUtils {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(MessageUtils.class);

	static final Locale locale = Locale.TAIWAN;

	public static String get(MessageSource messageSource, String messageCode) {
		return messageSource.getMessage(messageCode, null, locale);
	}
}