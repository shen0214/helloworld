package com.shen.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.security.SecureRandom;

public class KeyImage {
	private SecureRandom random = new SecureRandom();
	private String randKey;

	private Color getRandColor(int fc, int bc) {
		if (fc > 255)
			fc = 255;
		if (bc > 255)
			bc = 255;
		int r = fc + random.nextInt(bc - fc);
		int g = fc + random.nextInt(bc - fc);
		int b = fc + random.nextInt(bc - fc);
		return new Color(r, g, b);
	}

	public BufferedImage getImage(int picWidth, int picHeight, int bgColor, int characterSize, int characterLength) {
		int characterLen = (int) Math.pow(10, characterLength);
		this.randKey = Integer.toString(characterLen + random.nextInt(characterLen)).substring(1);

		BufferedImage image = new BufferedImage(picWidth, picHeight, BufferedImage.TYPE_INT_RGB);
		Graphics graphics = image.getGraphics();
		graphics.setColor(new Color(bgColor));
		graphics.fillRect(0, 0, picWidth, picHeight);

		for (int j = 0; j < randKey.length(); j++) {
			graphics.setColor(getRandColor(20, 130));
			graphics.setFont(new Font("Times New Roman", Font.ITALIC, 30 + random.nextInt(characterSize)));
			graphics.drawString(String.valueOf(randKey.charAt(j)), 6 + 12 * j, picHeight - 5);
		}

		graphics.dispose();
		return image;
	}

	public String getKey() {
		return randKey;
	}
}
