package com.shen;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.shen.security.MyUserDetails;
import com.shen.security.UserDetailsServiceImpl;

public class BaseController {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
	public static final String VIEWS_BASE_PATH = "/";
	public static final String ERROR_FORM = VIEWS_BASE_PATH + "/errmsg";
	public static final String COMMAND_INSERT = "insert";
	public static final String COMMAND_UPDATE = "update";
	
	public static final Locale locale = Locale.TAIWAN;

	@Autowired
	public MessageSource messageSource;

	public void putCommonDataToView(ModelAndView mv) {
		MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		mv.addObject("currentUser", myUserDetails.getUser());
	}

	public String getMessage(String messageCode) {
		return messageSource.getMessage(messageCode, null, locale);
	}

}
