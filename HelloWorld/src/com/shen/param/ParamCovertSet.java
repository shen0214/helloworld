package com.shen.param;

import java.util.HashSet;
import java.util.Set;

import com.shen.model.Channel;
import com.shen.model.Role;
import com.tech.util.Param;

public class ParamCovertSet {

	private ParamCovertSet() {}

	public static Set<Role> getRoles(Param param) {
		String[] roleIds = param.getValues("roleId");
		if(roleIds==null || roleIds.length==0) return null;
		
		Set<Role> roles = new HashSet<Role>();
		for (String roleId : roleIds) {
			Role role = new Role();
			role.setRoleId(Long.valueOf(roleId));
			roles.add(role);
		}
		return roles;
	}
	
	public static Set<Channel> getChannels(Param param) {
		Set<Channel> channels = new HashSet<Channel>();
		Channel channel = new Channel();
		channel.setChannelId(1L);
		channels.add(channel);
		return channels;
	}
	
}
