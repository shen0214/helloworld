package com.shen.param;

import java.util.HashMap;
import java.util.LinkedHashMap;

import com.shen.pagelist.PageParam;
import com.tech.util.Param;
import com.tech.util.StringUtil;


public class UserPageParam extends PageParam {
	static LinkedHashMap<String, String> orderMap;
	static HashMap<String, String> queryFields;

	static {
		orderMap = new LinkedHashMap<String, String>();
		orderMap.put("name", "姓名");
		orderMap.put("email", "email");
		orderMap.put("dept", "部門");
		
		queryFields = new HashMap<String, String>();
		queryFields.put("name", "姓名");
	}

	final static String defaultOrderBy = "name";

	public UserPageParam() {
		super();
		setMaxResult(15); // 每頁 15 筆.
	}

	public HashMap<String, String> getOrderMap() {
		return orderMap;
	}

	@Override
	public void setParam(Param param) throws Exception {
		super.setParam(param);
		if (StringUtil.isEmpty(getOrderBy())) setOrderBy(defaultOrderBy); 
		setOrderByTitle(queryOrderByTitle());
		if (StringUtil.isEmpty(getOrderByTitle())) {
			setOrderBy(defaultOrderBy);
			setOrderByTitle(queryOrderByTitle());
		}
	}

	private String queryOrderByTitle() {
		return StringUtil.nullToSpace(orderMap.get(getOrderBy()));
	}
	
	public HashMap<String, String> getQueryFields() {
		return queryFields;
	}
}
