package com.shen.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.shen.model.User;

public class UserValidator implements Validator {
	 
  public boolean supports(Class<?> clazz) {  
      return clazz.equals(User.class);  
  }  
  
  public void validate(Object target, Errors errors) { 
  	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userId", "required", "required");
  	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "required", "required");
  	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "required", "required");
  	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required", "required");
  }
}
