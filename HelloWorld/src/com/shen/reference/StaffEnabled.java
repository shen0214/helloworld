package com.shen.reference;

import java.util.ArrayList;
import java.util.List;

public class StaffEnabled {
	
	static List<CodeName> listItem;
	
	static {
		listItem = new ArrayList<CodeName>();
		listItem.add(new CodeName("1", "已啟用"));
		listItem.add(new CodeName("0", "未啟用"));
	}
	
	public static List<CodeName> getItems() {
		return listItem;
	}
}
