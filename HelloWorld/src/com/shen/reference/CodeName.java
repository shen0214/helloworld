package com.shen.reference;

public class CodeName {
	String code;
	String name;
	
	public CodeName(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}