package com.shen.pagelist;

import java.net.URLEncoder;
import java.util.HashMap;

import com.tech.util.Param;
import com.tech.util.StringUtil;

public abstract class PageParam {
	private int currentPage;
	private String orderBy;
	private String orderByTitle;
	private String col;
	private String kw;
	private String kwCode;
	private int maxResult;

	public PageParam() {
		currentPage = 0;
		orderBy = "";
		orderByTitle = "";
		col = "";
		kw = "";
		kwCode = "";
		maxResult = 20;
	}
	
	public abstract HashMap<String, String> getOrderMap();
	
	public abstract HashMap<String, String> getQueryFields();

	public void setParam(Param param) throws Exception {
		currentPage = param.getInt("pno");
		orderBy = param.get("order");
		col = param.get("col");
		kw = param.get("kw");
		kwCode = URLEncoder.encode(kw, "UTF-8");
	}
	
	public boolean isHaveKeyword() {
		if (StringUtil.isEmpty(getCol())) return false;
		if (StringUtil.isEmpty(getKw())) return false;
		return true;
	}

	public int getFirstRowNumber() {
		return (currentPage * maxResult);
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getCol() {
		return col;
	}

	public void setCol(String col) {
		this.col = col;
	}

	public String getKw() {
		return kw;
	}

	public void setKw(String kw) {
		this.kw = kw;
	}

	public int getMaxResult() {
		return maxResult;
	}

	public void setMaxResult(int maxResult) {
		this.maxResult = maxResult;
	}

	public String getOrderByTitle() {
		return orderByTitle;
	}

	public void setOrderByTitle(String orderByTitle) {
		this.orderByTitle = orderByTitle;
	}

	public String getKwCode() {
		return kwCode;
	}

	public void setKwCode(String kwCode) {
		this.kwCode = kwCode;
	}
}
