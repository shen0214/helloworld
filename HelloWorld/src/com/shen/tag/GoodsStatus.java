package com.shen.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

public class GoodsStatus implements Tag {
	private String name = null; 	/* 以 name 輸出 */
	private String onclick = null;	/* 以 onclick 輸出 */
	private String info = null; 	/* 不輸出,說明,註解用 */
	private String id = null; 		/* 以 id 輸出,可供 css or javascript用 */
	private String clazz = null; 	/* 以 class 輸出,可供 css or javascript用 */
	private Integer selected = null;/* 以 selected輸出 */
	private PageContext pageContext;
	private Tag parent;

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public void setId(String paramString) {
		this.id = paramString;
	}

	public String getId() {
		return this.id;
	}

	public void setName(String paramString) {
		this.name = paramString;
	}

	public String getName() {
		return this.name;
	}

	public void setOnclick(String paramString) {
		this.onclick = paramString;
	}

	public String getOnclick() {
		return this.onclick;
	}

	public void setSelected(int paramInt) {
		this.selected = new Integer(paramInt);
	}

	public int getSeleted() {
		return this.selected.intValue();
	}

	public int doEndTag() throws JspException {
		int j;
		if (this.selected == null) {
			j = 0;
		} else {
			j = this.selected.intValue();
		}

		StringBuffer localStringBuffer = new StringBuffer("");
		localStringBuffer.append("<select name=\"" + this.name + "\"");
		if (this.id != null) {
			localStringBuffer.append(" id=\"" + this.id + "\"");
		}
		if (this.clazz != null) {
			localStringBuffer.append(" class=\"" + this.clazz + "\"");
		}
		if (this.onclick != null) {
			localStringBuffer.append(" onclick=\"" + this.onclick + "\"");
		}
		localStringBuffer.append(" >\n");

		switch (j) {
		case 0:
			localStringBuffer.append("<option value=\"0\" selected=\"selected\">未上架</option>\n");
			localStringBuffer.append("<option value=\"1\">下架</option>\n");
			localStringBuffer.append("<option value=\"3\">上架</option>\n");
			break;
		case 1:
			localStringBuffer.append("<option value=\"0\">未上架</option>\n");
			localStringBuffer.append("<option value=\"1\" selected=\"selected\">下架</option>\n");
			localStringBuffer.append("<option value=\"3\">上架</option>\n");
			break;
		case 3:
			localStringBuffer.append("<option value=\"0\">未上架</option>\n");
			localStringBuffer.append("<option value=\"1\">下架</option>\n");
			localStringBuffer.append("<option value=\"3\"  selected=\"selected\">上架</option>\n");
			break;
		default:
			localStringBuffer.append("<option>" + j + "超出選項範圍</option>\n");
		}
		localStringBuffer.append("</select>");
		try {
			this.pageContext.getOut().write(localStringBuffer.toString());
		} catch (IOException localIOException) {
			throw new JspException("IO Error: " + localIOException.getMessage());
		}
		dropData();
		return 6;
	}

	public int doStartTag() throws JspException {
		return 0;
	}

	public Tag getParent() {
		return this.parent;
	}

	public void release() {
		dropData();

	}

	private void dropData() {
		this.name = null;
		this.info = null;
		this.clazz = null;
		this.onclick = null;
		this.id = null;
		this.selected = null;
	}

	public void setPageContext(PageContext arg0) {
		this.pageContext = arg0;
	}

	public void setParent(Tag arg0) {
		this.parent = arg0;
	}

}
