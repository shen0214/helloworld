package com.shen.tag;

import java.util.Set;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import com.shen.model.Dept;
public class DeptTag implements Tag {
	
	
	private String name = null; 	/* 以 name 輸出 */
	private String onclick = null;	/* 以 onclick 輸出 */
	private String info = null; 	/* 不輸出,說明,註解用 */
	private String id = null; 		/* 以 id 輸出,可供 css or javascript用 */
	private String clss = null; 	/* 以 class 輸出,可供 css or javascript用 */
	private Integer selected = null;/* 以 selected輸出 */
	private PageContext pageContext;
	private Tag parent;
	Set<Dept> depts= null;
	
	
	
	private void dropData() {
		this.name = null;
		this.info = null;
		this.clss = null;
		this.onclick = null;
		this.id = null;
		this.selected = null;
	}
	
	
	
	@Override
	public int doEndTag() throws JspException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int doStartTag() throws JspException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Tag getParent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void release() {
		dropData();
	}

	@Override
	public void setPageContext(PageContext arg0) {
		this.pageContext = arg0;
		
	}

	@Override
	public void setParent(Tag arg0) {
		this.parent = arg0;
		
	}

}
