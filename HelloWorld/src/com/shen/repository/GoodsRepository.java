package com.shen.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shen.model.GoodsMain;

public interface GoodsRepository extends JpaRepository<GoodsMain, Long> {

}
