package com.shen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.shen.model.GoodsCategory;

@Repository
public interface GoodsCategoryRepository extends JpaRepository<GoodsCategory,GoodsCategory> {
	
	@Query(value="select G from GoodsCategory G where G.goodsId = :goodsId and G.categoryId = :categoryId")
	GoodsCategory findById(@Param("goodsId") long goodsId, @Param("categoryId") long categoryId);
	
}
