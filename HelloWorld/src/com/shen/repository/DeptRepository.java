package com.shen.repository;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.shen.model.Dept;

public interface DeptRepository extends JpaRepository<Dept, Long>{
	
	
//	@Query(value="select * from Dept",nativeQuery = true)
	 @Query(value="select d from Dept d order by ?#{#pageable}")
	 List<Dept> findAllForPageable(PageRequest pageable);

}
