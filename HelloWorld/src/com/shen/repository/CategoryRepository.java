package com.shen.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shen.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

}
