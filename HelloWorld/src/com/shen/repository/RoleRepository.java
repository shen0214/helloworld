package com.shen.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shen.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
}
