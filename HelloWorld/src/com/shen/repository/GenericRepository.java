package com.shen.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface GenericRepository<K, T extends Serializable> {

	public void insert(T  t);
	
	public void insert(List<T> t);
	
	public void merge(T t);

	public void update(T t);

	public void simpleUpdate(Map<Object, Object> delta);

	public void delete(Class<T>  t);

	public T load(Long id);

	public void refresh(T t);

	public long count();

	public List<T> findByCondition(Map<K, T> condition);

	public long countByCondition(Map<K, T> condition);
	
	public List<T> findAll(Class<T> t);
	
	//public long countRow(Class<T> t, PageParam pageParam) ;
	

}
