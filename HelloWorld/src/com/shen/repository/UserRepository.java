package com.shen.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.shen.model.User;
 
public interface UserRepository extends JpaRepository<User, Long> {
	User findByName(String name);

	User findByEmail(String email);
	
	
	User findByUserId(long userId) ;
	
//	List<User> findAll();

	@Query(value="select u from User u order by ?#{#pageable}",	countQuery = "select count(u.userId) from User u ")
	 Page<User> findAllForPageable(Pageable pageable);
}
