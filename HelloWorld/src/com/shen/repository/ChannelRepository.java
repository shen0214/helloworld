
package com.shen.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shen.model.Channel;

public interface ChannelRepository extends JpaRepository<Channel, Long> {
	
}
