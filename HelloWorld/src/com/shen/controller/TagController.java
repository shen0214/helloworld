package com.shen.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TagController {
	
	@RequestMapping(value = { "/test" }, method = RequestMethod.GET)
	public ModelAndView insertForm(ModelAndView mv) {
		mv = new ModelAndView("test");
		Map<Integer,String>  merchant = new HashMap<Integer, String> ();
		merchant.put(1, "MCC");
		merchant.put(2, "中文字");
		mv.addObject("merchants", merchant);
		return mv;
	}

}
