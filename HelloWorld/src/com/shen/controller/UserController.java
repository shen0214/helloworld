package com.shen.controller;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.shen.BaseController;
import com.shen.model.Channel;
import com.shen.model.Role;
import com.shen.model.User;
import com.shen.param.ParamCovertSet;
import com.shen.reference.StaffEnabled;
import com.shen.security.LoginController;
import com.shen.service.DeptServiceImpl;
import com.shen.service.UserServiceImpl;
import com.shen.validator.UserValidator;
import com.tech.util.Param;

@Controller
public class UserController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	static final String VIEW = "/security/user";
	static final String BASE_URL = "/security/user";
	static final String MODEL_NAME = "user";

	@Autowired
	UserServiceImpl userServiceImpl;

	@Autowired
	DeptServiceImpl deptServiceImpl;

	@InitBinder(MODEL_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(new UserValidator());
	}

	/* 申請新的工作人員 */
	@RequestMapping(value = { BASE_URL, BASE_URL + "/*", BASE_URL + "/form" }, method = RequestMethod.GET)
	public ModelAndView insertForm(ModelAndView mv) {
		User user = new User();
		mv = new ModelAndView(BASE_URL);
		mv.addObject("command", COMMAND_INSERT);
		putCommonDataToView(user, mv);
		return mv;
	}

	/* 新增 工作人員 */
	@RequestMapping(value = BASE_URL + "/insert", method = RequestMethod.POST)
	public ModelAndView insert(@Validated @ModelAttribute("user") User user, BindingResult result,
			HttpServletRequest request, ModelAndView mv) throws Exception {
		Param param = new Param(request);
		Set<Role> roles = ParamCovertSet.getRoles(param);
		Set<Channel> channels = ParamCovertSet.getChannels(param); // 目前頻道固定寫1 ,沒做用

		user.setRoles(roles);
		user.setChannels(channels);

		mv = new ModelAndView(BASE_URL);
		mv.addObject(MODEL_NAME, user);
		mv.addObject("command", COMMAND_UPDATE);
		userServiceImpl.save(user);
		user.setPassword("******************");
		putCommonDataToView(user, mv);
		return mv;
	}

	/* 取得工作人員 */
	@Transactional
	@RequestMapping(value = BASE_URL + "/{userId}/form", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable long userId, ModelAndView mv) throws Exception {
		User user = userServiceImpl.findByUserId(userId);
		user.setPassword("******************");
		for (Role role : user.getRoles())
			; // 因為 Use r對應 Role 是 lazy 會無法關聯所以要在方法上注解為Transaction 將取出的資料再回設一次

		// update user set user.password = ( select user1.password from user1 where
		// user1.user_id=4) where user.user_id=4

		// logger.debug( role.getRoleName() + "===============");

		mv = new ModelAndView(VIEW);
		if (user == null) {
			user = new User();
			mv.addObject("command", COMMAND_INSERT);
			mv.addObject("message", getMessage("not.exist"));
		} else {
			/*
			 * if (!authorization.isIT()) { if (editObj.getDept() !=
			 * authorization.getUser().getDept()) { throw new Exception("你只能修改你所在部門的使用者.");
			 * } }
			 */
			mv.addObject("command", COMMAND_UPDATE);
			mv.addObject("message", getMessage("action.load"));
		}
		putCommonDataToView(user, mv);
		return mv;
	}

	/* 刪除工作人員 */
	@RequestMapping(value = BASE_URL + "/update", method = RequestMethod.POST)
	public ModelAndView update(@Validated @ModelAttribute("user") User user, BindingResult result,
			HttpServletRequest request, ModelAndView mv) throws Exception {
		mv.addObject(MODEL_NAME, user);

		mv = new ModelAndView(BASE_URL);

		userServiceImpl.save(user);
		mv.addObject("command", COMMAND_INSERT);
		putCommonDataToView(user, mv);
		return mv;
	}

	/* 刪除工作人員 */
	@RequestMapping(value = BASE_URL + "/delete", method = RequestMethod.POST)

	public ModelAndView delete(@Validated @ModelAttribute("user") User user, BindingResult result,
			HttpServletRequest request, ModelAndView mv) throws Exception {
		mv.addObject(MODEL_NAME, user);

		mv = new ModelAndView(BASE_URL);
		userServiceImpl.save(user);
		mv.addObject("command", COMMAND_INSERT);
		user = new User();
		putCommonDataToView(user, mv);
		return mv;
	}

	public void putCommonDataToView(User user, ModelAndView mv) {
		super.putCommonDataToView(mv);
		mv.addObject("userDepts", deptServiceImpl.getDeptList());
		mv.addObject("baseURL", BASE_URL);
		mv.addObject("formId", MODEL_NAME);
		mv.addObject(MODEL_NAME, user);
		mv.addObject("goodsStatus", null);
		mv.addObject("roleNameList", null);
		mv.addObject("enabledItems", StaffEnabled.getItems());
	}

	@RequestMapping(value = BASE_URL + "/{clickToPage}/pagelist")
	public ModelAndView showUserPageList(@PathVariable String clickToPage, HttpServletRequest request,
			ModelAndView mv) {
		Sort sort = new Sort(Sort.Direction.DESC, "deptId"); // PageRequest
		// pageRequest = PageRequest.of(1, 10, sort); Pageable pageable =
		// PageRequest.of(0, 3, sort);
		List<User> depts = userServiceImpl.findAll();
		mv = new ModelAndView(VIEWS_BASE_PATH + "/security/user_pagelist");
		mv.addObject("users", depts);

		return mv;

	}

}
