package com.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;

public class TestFrame {
	String testFolder = "/Users/shen/testFolder";
	//String[] urlAndName ={"https://www.w3schools.com/howto/tryit.asp?font=ZCOOL%20KuaiLe", "zcoolKuaiLe"};
	//String[] urlAndName={"https://www.w3schools.com/howto/tryit.asp?font=Indie%20Flower", "indieFlower"};
	String[] urlAndName = {"https://www.w3schools.com/howto/tryit.asp?font=Ropa%20Sans", "ropaSans"};
	//String[] urlAndName ={"https://zenozeng.github.io/fonts.css/", "zeno"};
	String scale = "0.8";
	String[] args = {urlAndName[0], "-T", "-D", testFolder, "-o", urlAndName[1],  "-s", scale};
	
	
	Set<String> h = new HashSet<>(Arrays.asList("a", "b"));
	Set<String> set = Stream.of("a", "b").collect(Collectors.toSet());
	Set<String> StringSet = Stream.of("a", "b").collect(Collectors.toCollection(HashSet::new));
	
	
	Map<String, String> myMap = new HashMap<String, String>(){{
        put("a", "b");
        put("c", "d");
    }};
	    
	
	@SuppressWarnings("serial")
	ArrayList<String> cmd = new ArrayList<String>(){{
	    add("/usr/local/bin/webkit2png");
	    addAll(Arrays.asList(args));
	}};

	@Before
	public void before() {
		cmd.stream().forEach(a -> {
			System.out.println(a);
		});
		
	}

	@Test
	public void main(){
		Process p;
		try {
			p = Runtime.getRuntime().exec(cmd.stream().toArray(String[]::new));
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		 } catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
