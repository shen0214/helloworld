package com.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.IntStream;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

public class TestRandom {
	private static final Logger logger = LoggerFactory.getLogger(TestRandom.class);
	int ticketSeq = 1001;		// 彩票的初始 seq
	int lotteryCount = 1000;	// 產生的組數 
	
	// 彩金配比
	@SuppressWarnings("serial")
	Map<Integer, Integer> lottery = new HashMap<Integer, Integer>() {{
		put(100000, 0);	put(50000, 0);	put(10000, 1);	put(5000, 2);	put(1000, 4);
		put(500, 10);	put(100, 20);	put(50, 30);	put(20, 50);	put(10, 100);
	}};
	
	int luckMultiple = 10;		// 幸運倍率
	int luckLotteryRatio = 2;	// 幸運倍率
	// 會出現的幸運倍率的彩金種類
	@SuppressWarnings("serial")
	List<Integer> multipleLottery = new ArrayList<Integer>(){{
		add(1000);	add(500);	add(100);	add(50);	add(10);
	}};

	List<Integer> lotteryKind = new ArrayList<Integer>(lottery.keySet());	// 彩金種類
	Collection<Integer> lotteryRatio =lottery.values(); 					// 彩金占比
	int ratio = lotteryRatio.stream().mapToInt(Integer::intValue).sum();	// 中奬組數
	
	int startBallNum = 101; 		// 彩球開始數字
	int endBallNum = 200; 		// 彩球結束數字(不含這個數字)

	// 初始待選彩球固定范圍
	List<Integer> balls = IntStream.range(startBallNum, endBallNum).collect(ArrayList<Integer>::new, List<Integer>::add, null);
	// 彩票集
	final List<HashMap<Integer, Integer>> tickets = new ArrayList<HashMap<Integer, Integer>>() ;
	// 0：托  1：胆  2：other 
	final List<ArrayList<HashMap<Integer, Integer>>> tickets1 = new ArrayList<ArrayList<HashMap<Integer, Integer>>>() ;

	int redBallBoxesNum = 4; 	// 紅球 球箱數 胆
	int blueBallBoxesNum = 25;	// 藍球 球箱數 托
	
	// 初始球箱(彩票上會出現 redBallBoxesNum + blueBallBoxesNum 個數字就會產生多少的彩箱)
	@SuppressWarnings("serial")
	final List<HashMap<Integer, Integer>> ballBoxes = new ArrayList<HashMap<Integer, Integer>>() {{
			for (int i = 0; i < (redBallBoxesNum + blueBallBoxesNum); i++)	add(new HashMap<Integer, Integer>());
	}};
	

	// 紅球數 胆
	HashMap<Integer, Integer> redBall = new HashMap<Integer, Integer>();
	// 藍球數 托
	HashMap<Integer, Integer> blueBall = new HashMap<Integer, Integer>();
	
	// 標示符號
	final String STAR_MARKS = String.format(" %s ", StringUtils.repeat('*',20));
	final String PLUS_MARKS = String.format(" %s ", StringUtils.repeat('+',20));
	final String MINUS_MARKS = String.format(" %s ", StringUtils.repeat('+',20));
	final String EQUAL_MARKS = String.format(" %s ", StringUtils.repeat('=',20));
	

	@Before
	public void before() {
		int lotterySum = lottery.keySet().stream().mapToInt(key -> lottery.get(key) * key).sum();
		
		System.out.format("%s 總彩金金額 ： %d \n", EQUAL_MARKS, lotterySum);
		System.out.format("%s 中奬機率 ： %d / %d \n", EQUAL_MARKS, ratio, lotteryCount);
		System.out.format("%s 彩金種類 ： %s \n", EQUAL_MARKS, lotteryKind.toString());
		System.out.format("%s 幸運倍率 ： %d \n", EQUAL_MARKS, luckMultiple);
		System.out.format("%s 幸運中奬率 ： %d / %d  \n", EQUAL_MARKS, luckLotteryRatio, lotteryCount);
	}
	
	/** 
	 * 將待選彩球和隨機選出一個彩金種類(HashMap<Integer, Integer>)，依序放入球箱
	 * 從每個球箱隨機選出一個彩球, 組成彩票上的數字
	 * @param ticket  彩票
	 */ 
	private void setTicket(HashMap<Integer, Integer> ticket) {
		// 清空全部球箱
		ballBoxes.forEach(ballBox -> {
			ballBox.clear();
		});
		
		Collections.shuffle(balls); // 待選彩球隨機排列
		
		// 將待選彩球和隨機選出一個彩金種類(HashMap<Integer, Integer>)，依序放入球箱
		IntStream.range(0, endBallNum - startBallNum - 1).forEach(idx -> {
			int values = lotteryKind.get( Math.abs((new Random()).nextInt() % lotteryKind.size()));
			ballBoxes.get(idx % 29).put(balls.get(idx), values);
		});
		
		List<Integer> ticketNum = new ArrayList<Integer>();
		// 初始彩票,從每個球箱隨機選出一個彩球
		ballBoxes.forEach(ballBox -> {
			Integer idx = Math.abs((new Random()).nextInt() % ballBox.size());
			Integer key = new ArrayList<Integer>(ballBox.keySet()).get(idx);
			ticketNum.add(key);
		});
		
		// 產生隨機彩金集和彩箱數目同
		@SuppressWarnings("serial")
		List<Integer> ticketVal = new ArrayList<Integer>() {{
			// 因java 隨機數分佈很不平均，反覆把彩金種類全放入到彩金集
			for (int i=0 ; i < ballBoxes.size()%lotteryKind.size() ; i++)
				addAll(lotteryKind);
			// 彩金集剩下的空位再彩金集補足
			for(int i = size(); i<= ballBoxes.size(); i++) {
				Integer idx = Math.abs((new Random()).nextInt() % lotteryKind.size());
				Integer value = lotteryKind.get(idx);
				add(value);
			}
		}};
		
		Collections.shuffle(ticketNum);		// 隨機排列
		Collections.shuffle(ticketVal);		// 彩金集隨機排列
		
		// 將彩球和彩金配對放入彩票集
		IntStream.range(0, 29).forEach(key-> {
			ticket.put(ticketNum.get(key), ticketVal.get(key));
		});
		ticketNum.clear();
		ticketVal.clear();
	
	}

	public int count ;
	@Test
	public void pick() {
		for(int i=ticketSeq; i<(ticketSeq + lotteryCount); i++) {
			//System.out.println( i + StringUtils.repeat(PLUS_MARKS.replace(" ",""),4));
			HashMap<Integer, Integer> ticket = new  HashMap<Integer, Integer>();		// 彩票
			setTicket(ticket);
			tickets.add(ticket);
			/*count = 0;
			ticket.forEach((key,value)->{
				System.out.format("[%d : %d]	", key, value);
				if (count%5 == 4)  System.out.println();
				++count;
			});
			System.out.println();*/
		}
		List<Integer> winnings = new ArrayList<Integer>();
		randomArray(ticketSeq, ticketSeq + lotteryCount, ratio, winnings);	// 取得隨機得奬的 seq
		
		
		Gson gson =new Gson();
		@SuppressWarnings("unused")
		String str = gson.toJson(winnings);
		System.out.println(tickets);
		
		/*winnings.forEach( x -> {
			System.out.println(x);
		});
		
		tickets.forEach(ticket->{
			count =0;
			ticket.forEach((key,value)->{
				System.out.format("[%d : %d]	", key, value);
				if (count%5 == 4)  System.out.println();
				++count;
			});
			System.out.println();
			System.out.println(StringUtils.repeat(PLUS_MARKS.replace(" ",""),4));
		});
		System.out.println(tickets.size());
		/*List<int[]> xx = Arrays.asList(IntStream.range(1001,2001).toArray());
		Collections.shuffle(xx);
		
		
		Arrays.asList(xx);
		*/
	}
	
	/** 
	 * 隨機指定范圍內N個不重複的數 
	 * 在初始化的無重複待選數組中隨機產生一個數放入結果集中， 
	 * 將待選數組被隨機選的到數，用待選數組(len-1)下標對應的數替換 
	 * 然後len-2裡隨機產生下一個隨機數，如此類推 
	 * @param max  指定範圍最大值
	 * @param min  指定範圍最小值
	 * @param num  隨機數個數
	 * @param List<Integer> 隨機數结果集 
	 * https://www.jianshu.com/p/79a7179f69d1
	 */  
	public static void randomArray(int min, int max, int num, List<Integer> result) {
		int len = max - min + 1;
		// 初始化給固定范圍的的待選數組
		int[] source = IntStream.range(min, max + 1).toArray();

		Random rd = new Random();
		int index = 0;
		for (int i = 0; i < num; i++) {
			// 待選數組0到(len-2)隨機一個下標
			index = Math.abs(rd.nextInt() % len--);
			// 將隨機到的數放入到結果集
			result.add(source[index]);
			// 將待選數組中被隨機到的數，用待選數組(len-1)下標對應的數替換
			source[index] = source[len];
		}
	}  
}
