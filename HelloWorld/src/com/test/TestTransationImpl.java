package com.test;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.shen.repository.GenericRepository;

public abstract class TestTransationImpl<T, K extends Serializable> implements GenericRepository<T, K> {
	private static final Logger logger = LoggerFactory.getLogger(TestTransationImpl.class);

	private static EntityManagerFactory factory;

	public static EntityManagerFactory getEntityManagerFactory() {
		return factory;
	}

	public static EntityManager getEntityManager() {
		return factory.createEntityManager();
	}

	public static EntityTransaction getTransaction() {
		EntityManager entityManager = getEntityManager();
		EntityTransaction tx = entityManager.getTransaction();
		return tx;
	}

	public static void shutdown() {
		if (factory != null) {
			factory.close();
		}
	}

	@Override
	public void insert(T  t) {
		EntityManager entityManager = getEntityManager();
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		try {
			entityManager.persist(t);
			tx.commit();
		} catch (RuntimeException e) {
			tx.rollback();
			logger.debug(String.format(e.getMessage()));
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void insert(List<T> t) {
		EntityManager entityManager = getEntityManager();
		EntityTransaction tx = entityManager.getTransaction();
		tx.begin();
		try {

			for (T obj : t) {
				entityManager.persist(obj);
			}

			tx.commit();
		} catch (RuntimeException e) {
			tx.rollback();
			logger.error(String.format(e.getMessage()));
		} finally {
			entityManager.close();
		}
	}

	@Override
	public void merge(T  t) {
	    if (t != null) {
	    	EntityManager entityManager = getEntityManager();
			EntityTransaction tx = entityManager.getTransaction();
	        try {
	            tx.begin();
	            t = entityManager.merge(t);
	            tx.commit();
	        } catch (Exception e) {
	        	tx.rollback();
	        	logger.error(String.format(e.getMessage()));
	        } finally {
	        	entityManager.close();
	        }
	    }

	}

	@Override
	public void delete(Class<T>  t) {
		
		if (t!=null) {
			EntityManager entityManager = getEntityManager();
	        List<T> objs = findAll(t);
	        if (objs != null) {
	            EntityTransaction tx = entityManager.getTransaction();
	            try {
	                tx.begin();
	                entityManager.remove(t);
	                tx.commit();
	            } catch (Exception e) {
	                tx.rollback();
	                logger.error(String.format(e.getMessage()));
	            } finally {
	            	entityManager.close();
	            }
	        }
	    }

	}

	public T load(Long id) {
		return null;
	}

	@Override
	public void refresh(T t) {

	}

	@Override
	public Long count() {
		return null;
	}

	
	@Override
	public Long countByCondition(Map<T, K> condition) {
		return null;
	}
	
	@Override
	public List<T> findAll(Class<T>  t) {
		EntityManager entityManager = getEntityManager();
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> query = (CriteriaQuery<T>) criteriaBuilder.createQuery(t);
		TypedQuery<T> typedQuery = entityManager.createQuery(query);
		List<T> list = typedQuery.getResultList();
		entityManager.close();

		return list;
	}

}
