package com.test;

import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.shen.model.Category;
import com.shen.model.Channel;
import com.shen.model.GoodsMain;
import com.shen.model.Role;
import com.shen.model.User;
import com.shen.service.CategoryServiceImpl;
import com.shen.service.UserServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@PropertySource(value = { "classpath*:application.properties", "classpath*:messages.message.properties" })
@ContextConfiguration(locations = { "file:testResource/appconfig-root.xml" })
@Transactional
//@ContextConfiguration(locations={"file:webapp/WEB-INF/appconfig-root.xml"})
public class Utest {

	@Autowired
	private UserServiceImpl userServiceImpl;
//	
//	@Autowired
//	private ChannelServiceImpl channelServiceImpl;
//	
	@Autowired
	private CategoryServiceImpl categoryServiceImpl;
//	
//	@Autowired
//	private GoodsServiceImpl goodsServiceImpl;
//	
//	@Autowired
//	private GoodsCategoryServiceImple goodsCategoryServiceImple;
//	

	@Before
	public void setup() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("After");
		;
	}

	private User user = new User();
	private Set<Role> roles = new HashSet<Role>();
	private Category category;
	private Set<Channel> channels = new HashSet<Channel>();
	private Set<GoodsMain> goods = new HashSet<GoodsMain>();
	private Set<Category> categorys = new HashSet<Category>();

	private void newChannels() {
		Channel channel1 = new Channel();
		Channel channel2 = new Channel();
		channel1.setChannelId(1);
		channel2.setChannelId(2);
		channels.add(channel1);
		channels.add(channel2);
	}

	private void newRoles() {
		Role role1 = new Role();
		Role role2 = new Role();
		Role role3 = new Role();
		Role role4 = new Role();
		role1.setRoleId(1L);
		role2.setRoleId(2L);
		role3.setRoleId(3L);
		role4.setRoleId(4L);
		roles.add(role1);
		roles.add(role2);
		roles.add(role3);
		roles.add(role4);
	}

	private void getUser(String email) {
		System.out.println("==============set roles====================");
		user = userServiceImpl.findByEmail(email);
		roles = user.getRoles();
		channels = user.getChannels();
		setUser();
	}

	private void setUser() {
		System.out.println("==============set user====================");
		user.setCategoryLimit(0);
		user.setDeptId(1);
		user.setEmail("chihchiho");
		user.setEnabled(1);
		user.setMyDefaultChannel(1);
		user.setMyDurationStatus(0);
		user.setMyGoodsStatus(0);
		user.setName("何智琦");
		user.setPassword("shen1234");
		user.setRoles(roles);
		user.setChannels(channels);
	}

	//@Test
	public void findUser() {
		System.out.println("==============find user====================");
		getUser("chihchiho");
		/*newChannels();
		Set<Channel> channels = user.getChannels();
		if (!channels.isEmpty())
			for (Channel channel : channels) {
				System.out.println(channel.getName() + "=============channel====================");
				categorys = channel.getCategory();
				for(Category category: categorys)
					System.out.println(category.getName() + "=============category====================");
			}
		*/
		// userServiceImpl.save(user);
	}
	
	
	@Test
	public void updateUser() {
		System.out.println("==============find user====================");
		newRoles();

		user = userServiceImpl.findByEmail("shen123");
		//getUser("shen123");
		user.setPassword("shen123");
		user.setName("沈麗娟");
		user.setEnabled(1);
		user.setRoles(roles);
		user.setDeptId(1);
		userServiceImpl.save(user);
		
	}

	//@Test
	public void addUser() {
		System.out.println("==============add user====================");
		user = new User();
		newRoles();
		newChannels();
		setUser();
		userServiceImpl.save(user);
	}

	// @Test
	public void deleteUser() {
		System.out.println("==============delete user====================");
		getUser("chihchiho");
		if (user != null)
			userServiceImpl.delete(user.getEmail());
		;
	}

	/*
	 * @Test public void testUser() { User user =
	 * userServiceImpl.findByEmail("shenee"); System.out.println(user.getName());
	 * 
	 * }
	 * 
	 * @Test public void testChannel() { Channel channel =
	 * channelServiceImpl.findById(1); List<Category> categorys =
	 * channel.getCategory();
	 * System.out.println(categorys.isEmpty()?0:categorys.size());
	 * System.out.println(channel.getName()); }
	 */

	/*
	 * @Test public void testCategory() { Category category =
	 * categoryServiceImpl.findById(7619); goods = category.getGoods(); if
	 * (!goods.isEmpty()) for (GoodsMain gds : goods) {
	 * System.out.println(gds.getHeadline()); }
	 * System.out.println(category.getChannel().getName()); }
	 */
	/*
	 * @Test public void testGoods() { GoodsMain goods =
	 * goodsServiceImpl.findById(430017); GoodsInfo goodsInfo =
	 * goods.getGoodsInfo(); System.out.println(goods.getSummary());
	 * System.out.println(goodsInfo.getSourceName());
	 * 
	 * }
	 */

	/*
	 * @Test public void testGoodsCategory(){ GoodsCategory
	 * goodsCategory=goodsCategoryServiceImple.findById(430017, 7609);
	 * System.out.println(goodsCategory.getChannelId());
	 * 
	 * }
	 */
}
